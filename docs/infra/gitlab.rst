Gitlab
======

Gitlab running in docker on a Ubuntu 18.10 VM


https://docs.gitlab.com/omnibus/docker/


.. code-block:: console

  cdraper@build:~$ uname -r
  4.18.0-17-generic
  cdraper@build:~$ cat /etc/*release
  DISTRIB_ID=Ubuntu
  DISTRIB_RELEASE=18.10
  DISTRIB_CODENAME=cosmic
  DISTRIB_DESCRIPTION="Ubuntu 18.10"
  NAME="Ubuntu"
  VERSION="18.10 (Cosmic Cuttlefish)"
  ID=ubuntu
  ID_LIKE=debian
  PRETTY_NAME="Ubuntu 18.10"
  VERSION_ID="18.10"
  HOME_URL="https://www.ubuntu.com/"
  SUPPORT_URL="https://help.ubuntu.com/"
  BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
  PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
  VERSION_CODENAME=cosmic
  UBUNTU_CODENAME=cosmic



.. code-block:: console

   # sudo docker run --detach --privileged \
    --hostname scansible.sprt.<REDACTED>.com \
    --publish 443:443 --publish 80:80 --publish 22:22 --publish 4567:4567 \
    --name gitlab \
    --restart always \
    --volume /srv/gitlab/config:/etc/gitlab \
    --volume /srv/gitlab/logs:/var/log/gitlab \
    --volume /srv/gitlab/data:/var/opt/gitlab \
    gitlab/gitlab-ce:latest


.. code-block:: console

  # cdraper@build:~$ openssl req \
  >   -newkey rsa:4096 -nodes -sha256 -keyout domain.key \
  >   -x509 -days 365 -out domain.crt
