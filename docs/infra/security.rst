Security
========

lynis
~~~~~~

https://www.server-world.info/en/note?os=CentOS_7&p=lynis


tripwire
~~~~~~~~~
https://www.server-world.info/en/note?os=CentOS_7&p=tripwire


aide
~~~~~~
https://www.server-world.info/en/note?os=CentOS_7&p=aide

rkhunter
~~~~~~~~~
https://www.server-world.info/en/note?os=CentOS_7&p=rkhunter

Traffic Mirroring for IDS
~~~~~~~~~~~~~~~~~~~~~~~~~


iptables
--------

To mirror traffic to an IDS using iptables:

.. code-block:: console

    # iptables -t mangle -A PREROUTING -j TEE --gateway [_ipaddress_]
    # iptables -t mangle -A POSTROUTING -j TEE --gateway [_ipaddress_]

To disable the mirroring:

.. code-block:: console

  # iptables -F -t mangle

.. important::

    TEE will only send the cloned packet to the next hop. If there are other routers in the path then you will need to repeat the TEE configuration to relay traffic to the target IDS where Snort is running.

.. important::

    Source and destination must be on the same subnet.

.. important::

    TEE modifies the destination MAC address, you will lose the original data.

TZSP
----

https://code.google.com/archive/p/port-mirroring/

Wikipedia_

.. _Wikipedia: https://en.wikipedia.org/wiki/TZSP


ERSPAN
------

ERSPAN encapsulates SPAN traffic using GRE so that we can mirror traffic and send it to a specified IP address.

.. note:: GRE = Generic Routing Encapsulation

An ERSPAN packet looks like this:

+----------------------------------------------------------+
| MAC_HEADER | IP_HEADER | GRE_HEADER | L2_Mirrored_Packet |
+----------------------------------------------------------+

