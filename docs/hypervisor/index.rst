Hypervisor
==========

qemu
----

http://events17.linuxfoundation.org/sites/events/files/slides/linux-con-eu-virt-server-2015-up.pdf

logcabin
--------

.. code-block:: console

  # logcabin -c 192.168.160.11,192.168.160.12,192.168.160.13 rm /REDACTED/vsd/15cb4f8d-5bb9-4daf-8291-6434541b80e0
  # logcabin -c 192.168.160.11,192.168.160.12,192.168.160.13 rm /REDACTED/vsd/be80bf38-0d8c-47ca-a16d-4d0917a9d0ba
  # logcabin -c 192.168.160.11,192.168.160.12,192.168.160.13 rm /REDACTED/vsd/19f23d3e-87c2-482b-be00-7acb89776664


TODO
----

[root@REDACTED-192-168-10-200 ~ 14:44:21 ]# sc vm import uri smb://10.10.10.210/vmexport/JACMDFIGS002/test/template-export/ definition-file template-export.xml

VM 'smb://10.10.10.210/vmexport/JACMDFIGS002/test/template-export/' import started successfully




https://www.linux-kvm.org/page/Documents


https://planet.virt-tools.org/


https://www.linux-kvm.org/page/Packed_virtqueue_performance


https://www.youtube.com/watch?v=E_2LjeX3BHU


https://www.linux-kvm.org/images/c/c6/Performance-Optimization-on-Huawei-Public-and-Private-Cloud-Lei-Gong-Jinsong-Liu-Huawei.pdf


https://www.google.com/search?q=SPDK+vhost&oq=SPDK+vhost&aqs=chrome..69i57&sourceid=chrome&ie=UTF-8


https://www.linux-kvm.org/page/KVM_Forum_2018


https://unix.stackexchange.com/questions/426652/connect-to-running-qemu-instance-with-qemu-monitor
https://en.wikibooks.org/wiki/QEMU/Monitor

https://qemu.weilnetz.de/doc/qemu-doc.html


https://lists.gnu.org/archive/html/qemu-devel/2017-11/msg05065.html

https://www.google.com/search?q=qemu+performance+monitor&oq=monitor+qemu+performance+&aqs=chrome.1.69i57j0.5943j0j7&sourceid=chrome&ie=UTF-8
