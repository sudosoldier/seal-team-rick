Tunnel in when you can't establish a tunnel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This method is pretty handy to troubleshoot remote connection issues.

Webex to the customer

If on a Windows workstation: Download and install the Fedora Image Writer

https://getfedora.org/en/workstation/download/


Follow the wizard prompts to download and write Fedora Workstation to the USB drive. You may check the box to write the image after it downloads.

Insert the USB and enable it in the BIOS by pressing DEL during boot. Go to the BIOS save settings menu and at the bottom there should be an option for "boot override". Under this menu you should see the USB drive.


After the liveusb boots, select "Try Fedora". Have the customer set the root password and start the ssh service. We will also create a tunnel so that we can login remotely.

.. code-block:: console

  $ sudo su
  # passwd
  <enter some password>
  # systemctl start sshd


You can now ssh into it from the customer's workstation, maybe over a WebEx.


If this is not possible, you may try opening a temporary jumphost:

.. code-block:: console
  
  # ssh -R support:22:localhost:22 serveo.net

Now you can login remotely from your workstation:

.. code-block:: console

  $ ssh -J serveo.net root@support

I would suggest changing the password to something more secure than what was set by the customer earlier, then you may begin your troubleshooting.

.. code-block:: console

  # passwd
  <some secure randomly generated password>
