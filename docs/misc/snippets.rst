Snippets
========

Get IP addresses of VMs on node
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

  # virsh -q list | awk '{print $2}' | xargs -t -I{} virsh qemu-agent-command {} '{"execute":"guest-network-get-interfaces"}' | python -mjson.tool | grep -B1 ipv4 | grep -v 127.0.0.1 | grep -v "ip-address-" | cut -d ":" -f 2 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

Duplicate Address Detection
~~~~~~~~~~~~~~~~~~~~~~~~~~~
To run the script for the first time, you first need to make the script executable.


.. note:: These were written with the intent to run on a cronjob and send email alerts

.. code-block:: bash

  #!/bin/bash

  lan_list=$(cat /opt/REDACTED/var/nodes | awk '{ print $3 }')

  for lanIP in $lan_list
  do
    backplane=$(cat /opt/REDACTED/var/nodes | grep $lanIP | awk '{ print $2}')
    ssh $backplane arping -D -c 1 -I lan $lanIP
    result=$(echo $?)
    if(( $result != 0 )); then
      $(echo "IP conflict detected at" $(date) >> conflictlog.txt)
    fi
  done

Email alerting is handled using the following:

.. code-block:: bash

  #!/bin/bash

  if [ -s conflictlog.txt ]; then
    $(echo -e "IP conflict detail: \n $(cat conflictlog.txt)" | mailx -s "IP conflict detected" rtsering@REDACTED.com)
    $(rm conflictlog.txt)
  else
    echo "file does not exist"
  fi

Retiering
~~~~~~~~~~

.. code-block:: console

  # sc vsd show | tail -n +3 | gawk '
  function tobi(s) {
  if (match(s, /([0-9.]+)TB/, m)) {
      return strtonum(m[1] * 1024 * 1024 * 1024 * 1024);
  }
  if (match(s, /([0-9.]+)GB/, m)) {
      return strtonum(m[1] * 1024 * 1024 * 1024);
  }
  if (match(s, /([0-9.]+)MB/, m)) {
      return strtonum(m[1] * 1024 * 1024);
  }
  if (match(s, /([0-9.]+)KB/, m)) {
      return strtonum(m[1] * 1024);
  }
  return strtonum(m[1]);
  }
  tobi($4) > tobi($5) { printf("%d %s \n", tobi($4) - tobi($5),$0); }
  ' | sort -n > /tmp/VSDsNeedingRecoveredNow


VM Changerate
~~~~~~~~~~~~~
Note the time-stamp. Edit it to whatever time you want, then run the script. You can ONLY run this in the style "from X time to now". No "run from time A to time B".
Someone smarter than me could probably re-write it to do include such logic.

.. code-block:: bash

   #!/bin/bash

   sc vm show display snaps | gawk '
   /VM.GUID/ { if (d > 0) print vm,d; vm=$NF; d = 0; }
   /^           Timestamp/ { match($0, / : (.*)$/, m); ts=m[1]; }
   /^           Block Diff/ && ts >= "2018-03-14 14:00:00"  && $NF < 100000 { d += $NF }
   END { if (d > 0) print vm,d; }
   '


Upload image over network
~~~~~~~~~~~~~~~~~~~~~~~~~~

https://askubuntu.com/questions/215505/how-do-you-monitor-the-progress-of-dd

.. code-block:: console

 $ dd if=/run/media/cdraper/backup/images/images/cumulus-linux-3.5.0-vx-amd64.bin | sc 9124 "REDACTEDcp -ov 63472870-02b8-4cd6-9883-79b1945e3562"
 317576+1 records in
 317576+1 records out
 162599382 bytes (163 MB, 155 MiB) copied, 30.2442 s, 5.4 MB/s

To display a progress bar

.. code-block:: console

	$ pv /run/media/cdraper/backup/images/images/cumulus-linux-3.5.0-vx-amd64.bin | sc 9124 "REDACTEDcp -ov 63472870-02b8-4cd6-9883-79b1945e3562"
  155MiB 0:01:06 [2.33MiB/s] [=============================================================================================================================================>] 100%

Download image over network
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. warning::

	I hope your ``/tmp`` directory is bigger than the size of this VSD

.. code-block:: console

 $ sc 9124 "REDACTEDcp -iv 63472870-02b8-4cd6-9883-79b1945e3562" | dd of=/tmp/backup.img


Import/Export
~~~~~~~~~~~~~

.. code-block:: console

  ##Direct import:
  # /usr/bin/qemu-img convert smb://WORKGROUP;REDACTED:REDACTED@10.3.50.21/REDACTED/NewNetsite-clone/c3051308-5bb7-4362-af17-ca9082163ced.qcow2 -O REDACTED REDACTED:REDACTED/bafe7972-4079-41d2-a39d-e1e7654c16d1 -p

  ##Direct export:
  # /usr/bin/qemu-img convert -f REDACTED REDACTED:REDACTED/c3051308-5bb7-4362-af17-ca9082163ced -O qcow2 smb://WORKGROUP;REDACTED:REDACTED@10.3.50.21/REDACTED/NewNetsite-clone//c3051308-5bb7-4362-af17-ca9082163ced.qcow2 -p


the -f `-f REDACTED REDACTED:REDACTED/` is the name of the SDS block engine
