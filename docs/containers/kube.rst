Kubernetes
~~~~~~~~~~~

Kubespray
----------

.. code-block:: console

  #  git clone https://github.com/kubernetes-incubator/kubespray


https://github.com/kubernetes-incubator/kubespray/blob/master/docs/getting-started.md


Post-Install
-------------

https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/

Login to docker

.. code-block:: console

  # docker login


.. code-block:: console

  $ kubectl create secret docker-registry regcred --docker-server=https://index.docker.io/v1/ --docker-username=<REDACTED> --docker-password='<REDACTED>' --docker-email=<REDACTED>

  $ kubectl get secret regcred --output="jsonpath={.data.\.dockerconfigjson}" | base64 --decode

Create a pod with the private docker hub account defined as follows:

.. literalinclude:: rtfd-deployment.yaml
   :language: yaml


https://github.com/kubernetes-incubator/kubespray/blob/master/docs/calico.md
