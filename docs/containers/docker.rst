Docker
======

Images
~~~~~~

mcr.microsoft.com/mssql/server:2017-latest
docker.elastic.co/elasticsearch/elasticsearch:6.4.3
docker.elastic.co/kibana/kibana:6.4.3
docker.elastic.co/logstash/logstash:6.4.3
docker.elastic.co/beats/filebeat:6.4.3
docker.elastic.co/beats/metricbeat:6.4.3
docker.elastic.co/beats/packetbeat:6.4.3
docker.elastic.co/beats/heartbeat:6.4.3
docker.elastic.co/beats/auditbeat:6.4.3
docker.elastic.co/apm/apm-server:6.4.3


Filebeat
--------

.. literalinclude:: config/filebeat.yml
   :language: yaml

.. code-block:: console

  $ docker run \
  --mount type=bind,source="$(pwd)"/filebeat.yml,target=/usr/share/filebeat/filebeat.yml \
  docker.elastic.co/beats/filebeat:6.4.3

Metricbeat
----------

.. literalinclude:: config/metricbeat.yml

.. code-block:: console

  $ docker run \
  --mount type=bind,source="$(pwd)"/metricbeat.yml,target=/usr/share/metricbeat/metricbeat.yml \
  docker.elastic.co/beats/metricbeat:6.4.3


ELK Stack
~~~~~~~~~
https://www.docker.elastic.co/


.. code-block:: console

  docker export "$(docker create --name metricbeat docker.elastic.co/beats/metricbeat)" -o metricbeat.tar


Tools
~~~~~
https://www.projectatomic.io/blog/2018/02/reintroduction-podman/
