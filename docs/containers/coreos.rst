CoreOS
======

Write the system configuration file in YAML:
.. code-block:: yaml

  passwd:
    users:
      - name: core
        ssh_authorized_keys:
          - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6u1IN/6aTLxLPMKftb62Ane+DC43oLUP3ngJEWve9V3wQssjwwnxHXyYQvypWraKFQt90R94dD82oG3RYVZSNlDQQj4cvpEdfpw6P8Xnt+WCqcWAZuU01ITOvsTodyt7rsPUF/Xir9+X9YcvnPH0CvLeD9YIqwUCLRNv+gXSOQ0aKHu1IoydDfPHq24YhTg9Bcfpck+dC7Co+eQNf4YGYp890syFQ7dNkOEu8qLl/RnnLaZMTGFKwrZo/P/2Inb7R7whgYOBvWSNcYXFjU5BbyvIsUc2n6Q+geK7MWRQJ5dI/TTEfioVf/V0se5kL6qgTSNNoRNxU1JQdVIfetHqb


Run the tool to create the JSON file that will actually be used during setup:

.. code-block:: console

  $ i-forget-the-name-of-this-tool

Results will look like:

.. code-block:: json

  {"ignition":{"config":{},"security":{"tls":{}},"timeouts":{},"version":"2.2.0"},"networkd":{},"passwd":{"users":[{"name":"core","sshAuthorizedKeys":["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6u1IN/6aTLxLPMKftb62Ane+DC43oLUP3ngJEWve9V3wQssjwwnxHXyYQvypWraKFQt90R94dD82oG3RYVZSNlDQQj4cvpEdfpw6P8Xnt+WCqcWAZuU01ITOvsTodyt7rsPUF/Xir9+X9YcvnPH0CvLeD9YIqwUCLRNv+gXSOQ0aKHu1IoydDfPHq24YhTg9Bcfpck+dC7Co+eQNf4YGYp890syFQ7dNkOEu8qLl/RnnLaZMTGFKwrZo/P/2Inb7R7whgYOBvWSNcYXFjU5BbyvIsUc2n6Q+geK7MWRQJ5dI/TTEfioVf/V0se5kL6qgTSNNoRNxU1JQdVIfetHqb"]}]},"storage":{},"systemd":{}}

Copy the init.json to CoreOS and install:

.. code-block:: console

  $ scp...
  $ sudo coreos-install -d /dev/vda -i init.json
