Lenovo
======

XClarity/IMM2 setup
~~~~~~~~~~~~~~~~~~~
If you need to set passwords or usernames:

https://www.thomas-krenn.com/en/wiki/Configuring_IPMI_under_Linux_using_ipmitool

.. code-block:: console

    # ipmitool lan print 1
    # ipmitool lan set 1 ipsrc static
    # ipmitool lan set 1 ipaddr 192.168.1.211
    Setting LAN IP Address to 192.168.1.211
    # ipmitool lan set 1 netmask 255.255.255.0
    Setting LAN Subnet Mask to 255.255.255.0
    # ipmitool lan set 1 defgw ipaddr 192.168.1.254
    Setting LAN Default Gateway IP to 192.168.1.254
    # ipmitool lan set 1 defgw macaddr 00:0e:0c:aa:8e:13
    Setting LAN Default Gateway MAC to 00:0e:0c:aa:8e:13
    # ipmitool lan set 1 arp respond on
    Enabling BMC-generated ARP responses
    # ipmitool lan set 1 auth ADMIN MD5
    # ipmitool lan set 1 access on
    # ipmitool lan print 1



.. code-block:: console

  [root@sr2500 ~]# ipmitool user set name 2 admin
  [root@sr2500 ~]# ipmitool user set password 2
  Password for user 2:
  Password for user 2:
  [root@sr2500 ~]# ipmitool channel setaccess 1 2 link=on ipmi=on callin=on privilege=4
  [root@sr2500 ~]# ipmitool user enable 2
  [root@sr2500 ~]#

  [root@sr2500 ~]# ipmitool user set name 3 monitor
  [root@sr2500 ~]# ipmitool user set password 3
  Password for user 3:
  Password for user 3:
  [root@sr2500 ~]# ipmitool channel setaccess 1 3 link=on ipmi=on callin=on privilege=2
  [root@sr2500 ~]# ipmitool user enable 3
  [root@sr2500 ~]# ipmitool channel getaccess 1 3
  Maximum User IDs     : 15
  Enabled User IDs     : 2

  User ID              : 3
  User Name            : monitor
  Fixed Name           : No
  Access Available     : call-in / callback
  Link Authentication  : enabled
  IPMI Messaging       : enabled
  Privilege Level      : USER
  [root@sr2500 ~]#

  [root@sr2500 ~]# ipmitool lan print 1
  Set in Progress         : Set Complete
  Auth Type Support       : NONE MD5 PASSWORD
  Auth Type Enable        : Callback :
                          : User     : MD5
                          : Operator :
                          : Admin    : MD5
                          : OEM      :
  IP Address Source       : Static Address
  IP Address              : 192.168.1.211
  Subnet Mask             : 255.255.255.0
  MAC Address             : 00:0e:0c:ea:92:a2
  SNMP Community String   :
  IP Header               : TTL=0x40 Flags=0x40 Precedence=0x00 TOS=0x10
  BMC ARP Control         : ARP Responses Enabled, Gratuitous ARP Disabled
  Gratituous ARP Intrvl   : 2.0 seconds
  Default Gateway IP      : 192.168.1.254
  Default Gateway MAC     : 00:0e:0c:aa:8e:13
  Backup Gateway IP       : 0.0.0.0
  Backup Gateway MAC      : 00:00:00:00:00:00
  RMCP+ Cipher Suites     : 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14
  Cipher Suite Priv Max   : XXXXXXXXXXXXXXX
                          :     X=Cipher Suite Unused
                          :     c=CALLBACK
                          :     u=USER
                          :     o=OPERATOR
                          :     a=ADMIN
                          :     O=OEM


You can access the BMC with SSH, although the CLI is very limited.
Reference: http://sysmgt.lenovofiles.com/help/index.jsp?topic=%2Fcom.lenovo.systems.management.xcc.doc%2Fnn1ia_r_ffdccommand.html

.. note:: I did not have success with this method, and had to spin up a VM on the cluster to access the BMC via a web browser instead.

.. code-block:: console

  # ssh USERID@192.168.1.211 # password is PASSW0RD by default
  system> ffdc generate
  system> ffdc copy -t 1 -ip 192.168.70.230 -u User2 -pw Passw0rd -f /tmp/
  system> ffdc status

Dynamic System Analysis / OneCLI
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are probably some useful commands in OneCLI, but all I've messed with is the inventory command to dump data for Lenovo cases since they require it.
I am not going to docuement it here, instead I have automated it via Ansible.


Links
~~~~~

https://systemx.lenovofiles.com/help/index.jsp
