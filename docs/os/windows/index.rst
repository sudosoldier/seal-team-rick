Windows
========

https://www.linux-kvm.org/page/WindowsGuestDrivers/viostor/documentation

https://pve.proxmox.com/wiki/Windows_guests_-_build_ISOs_including_VirtIO_drivers

https://www.linux-kvm.org/page/Internals_of_NDIS_driver_for_VirtIO_based_network_adapter


Package Management
~~~~~~~~~~~~~~~~~~
``chocolatey`` is similar to ``apt`` for ubuntu

Install it here: https://chocolatey.org/docs/installation

Powershell over SSH
~~~~~~~~~~~~~~~~~~~

https://docs.microsoft.com/en-us/powershell/scripting/core-powershell/ssh-remoting-in-powershell-core?view=powershell-6

SQL 2017 on Linux
~~~~~~~~~~~~~~~~~
https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-2017
https://www.microsoft.com/en-us/sql-server/sql-server-2017


Debugging
~~~~~~~~~

https://docs.microsoft.com/en-us/visualstudio/debugger/using-dump-files?view=vs-2017 ****


Setup
-----
https://docs.microsoft.com/en-us/windows-hardware/drivers/devtest/wdftester--wdf-driver-testing-toolset

https://docs.microsoft.com/en-us/windows-hardware/drivers/download-the-wdk


https://github.com/virtio-win/kvm-guest-drivers-windows/wiki/Building-the-drivers

symbol path: ``c:\symbols\local_symbols;SRV*c:\symbols\websymbols*http://msdl.microsoft.com/download/symbols``


Driver Verifier
---------------

https://answers.microsoft.com/en-us/windows/forum/windows_10-update/driver-verifier-tracking-down-a-mis-behaving/f5cb4faf-556b-4b6d-95b3-c48669e4c983

Links
-----
https://bugzilla.redhat.com/show_bug.cgi?id=771390#c31
https://github.com/virtio-win/kvm-guest-drivers-windows
https://fedorapeople.org/groups/virt/virtio-win/CHANGELOG
https://www.linux-kvm.org/page/WindowsGuestDrivers/GuestDebugging#Setting_symbol_paths
https://www.linux-kvm.org/page/WindowsGuestDrivers
https://www.linux-kvm.org/page/WindowsGuestDrivers/UpdatedGuestDebugging#Set_Symbols_path
https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/index
https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/crash-dump-files
https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/extracting-information-from-a-dump-file


Sysinfo
~~~~~~~
https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/systeminfo

Sysinternals
~~~~~~~~~~~~~
https://docs.microsoft.com/en-us/sysinternals/downloads/procmon


SMB Share Troubleshooting
~~~~~~~~~~~~~~~~~~~~~~~~~
.. code-block:: powershell

  PS> Get-NetOffloadGlobalSetting
  PS> Get-NetAdapterRSS
  PS> Get-SMBServerNetworkInterface
  PS> Get-SMBClientNetworkInterface
  PS> Get-SMBServerConfiguration
  PS> Get-SMBClientConfiguration


https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/systeminfo

Networking
~~~~~~~~~~~

.. important:: Run powershell as Administrator


Double-take unable to reach Management console because the port was in use:

.. code-block:: powershell

  PS> netstat -b -n -a | Select-String -Pattern "6326"

Solution was to set the dynamic port range to set from 6327 to 7327 as follows:

.. code-block:: powershell

  PS> netsh int ipv4 show dynamicportrange tcp
  PS> netsh int ipv4 set dynamicport tcp start=6327 num=1000



Boot Options
~~~~~~~~~~~~


cmd
---

.. code-block:: console

  # force the default boot mode to be safe mode with networking
  > bcdedit /set {default} safeboot network
  # force display of boot menu
  > bcdedit /set {bootmgr} displaybootmenu yes
  # force bootmenu timeout to be 10 seconds
  > bcdedit /set {bootmgr} timeout 10

.. note::
  you will need to set the default boot options back to normal in the OS using ``msconfig`` if you set the default to be safe mode

msconfig
--------

- run msconfig
- select boot tab
- check or uncheck the box for safe boot
- check the box `make all boot settings permanent`
- click apply then yes

.. image:: msconfig_boot.png


bcdedit
-------
https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/bcdedit-command-line-options

.. code-block:: console

  # bcdedit /set {default} safeboot network   <==== cmd line (MS cmd) to make VM boot into safe mode with networking
  # bcdedit /set {bootmgr} displaybootmenu yes <====cmd line (MS cmd) to make a VM display the boot menu
  # bcdedit /set {bootmgr} timeout 10   <===== cmd line (MS cmd) to make bootmenu time out 10 seconds

dism
----
https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/dism---deployment-image-servicing-and-management-technical-reference-for-windows


Troubleshooting
~~~~~~~~~~~~~~~

Boot
----
Go to Troubleshooting > Advanced > Console

Determine which drive letter is assigned to the REDACTED Tools ISO:

.. code-block:: console

  # diskpart
  # list vol
  # exit

Load the drivers from the ISO:

.. code-block:: console

  # drvload e:\drivers\stor\2k16\amd64\viostor.inf

System Integrity
----------------

.. code-block:: console

  # sfc /scannow

Memory Dump
-----------
https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/automatic-memory-dump


Exchange
--------

https://blogs.technet.microsoft.com/exchange/2016/05/31/checklist-for-troubleshooting-outlook-connectivity-in-exchange-2013-and-2016-on-premises/

Stuck getting devices ready
---------------------------

- boot to live cd > advanced > cmd prompt
- drvload e:\\drivers\\stor\\2k12\\amd64\\viostor.inf
- bcdedit
- exit and continue boot and go to device mgr
- uninstall generic upnp monitor
- set to normal boot
- reboot

BCD error on boot
-----------------

bootrec /FixMbr
bootrec /FixBoot
bootrec /ScanOs
bootrec /RebuildBcd

Can't import or export - fails partway through
----------------------------------------------

https://superuser.com/questions/1197853/windows-10-network-share-external-drive-to-android


Netsh
-----

https://serverfault.com/questions/102736/persistent-static-arp-entries-on-windows-is-it-possible
https://docs.microsoft.com/en-us/windows-server/networking/technologies/netsh/netsh-contexts

tcp
---
https://gallery.technet.microsoft.com/NTttcp-Version-528-Now-f8b12769


diskspd
-------
https://gallery.technet.microsoft.com/DiskSpd-a-robust-storage-6cd2f223



Ensure Terminal Services are using TCP
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Windows Server 2012 R2 by default has UDP as the default for RDP sessions. As reported by another one of our customers running Terminal Services:

Here's the fix:

Computer Config > Windows Settings > Administrative Templates > Windows Components > Remote Desktop Services > Remote Desktop Session Host > Connections > Select RDP transport protocol = Use only TCP

You can also set this on the client side by specifying:

Computer Config > Windows Settings > Admin Templates > Windows Components > Remote Desktop Services > Remote Desktop Connection Client > Turn off UDP on Client = Enabled


clock
~~~~~

https://docs.microsoft.com/en-us/sysinternals/downloads/clockres


boot time debugging locally
~~~~~~~~~~~~~~~~~~~~~~~~~~~



sysinternals
~~~~~~~~~~~~

https://docs.microsoft.com/en-us/sysinternals/

break these down into how tos
