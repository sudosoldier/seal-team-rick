.. code-block:: powershell

  > $PSVersionTable
  > get-host


Ansible setup: https://docs.ansible.com/ansible/2.7/user_guide/windows_setup.html#setting-up-a-windows-host

.. code-block:: powershell

  > winrm quickconfig
  > winrm quickconfig -transport:https


https://docs.microsoft.com/en-us/powershell/scripting/core-powershell/ssh-remoting-in-powershell-core?view=powershell-6
