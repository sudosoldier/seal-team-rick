Shells
======

zsh_

fish_

bash_


.. _fish: https://wiki.archlinux.org/index.php/Fish
.. _zsh: https://wiki.archlinux.org/index.php/zsh
.. _bash: https://wiki.archlinux.org/index.php/bash


Bash
~~~~

Locked terminal on accident
---------------------------

S is so near to A, and we all use ``Ctrl-A`` all the time. So, it is easy to hit ``Ctrl-S`` by accident.

``Ctrl-S`` pauses flow-control (XOFF), the terminal will accept inputs but will not display output.

To start flow-control, simply give ``Ctrl-Q`` (XON).


https://serverfault.com/questions/55880/moving-an-already-running-process-to-screen/284795#284795



zsh
~~~
Here are the contents of my ``~/.zshrc``

.. code-block:: bash

  source /etc/profile

  export ZSH=/home/cdraper/.oh-my-zsh
  source $ZSH/oh-my-zsh.sh

  ZSH_THEME="powerlevel9k/powerlevel9k"
  POWERLEVEL9K_MODE='nerdfont-complete'


  plugins=(kubectl celery cp colored-man-pages gcloud-zsh-completion docker-compose docker-machine dnf fabric fedora nmap rsync screen ssh-agent tmux sudo git adb docker git-extras vagrant pip sudo gem firewalld rake bundler)


  alias bpython2="python2 -m bpython"
  alias namebench="namebench.py"
  alias sc="ssh cluster -p "
  alias docker-remove-images="docker rmi $(docker images -q)"
  alias docker-remove-containers="docker rm $(docker ps -a -q)"
  alias gitgrep="grep -I -rnw '/home/cdraper/Git' -ie "
  alias g="googler -n 7 -c us -l en"
  alias sshmount="sshfs root@10.205.109.124:/ /home/cdraper/TestCluster -C"

  function thrift-session() { python2 -m bpython -i /home/cdraper/Git/REDACTED-thrift/REDACTED-api/client.py --host $1 --username $2 --password $3 ;} # 1 IP 2 USERNAME 3 PASS

  function proxyconnect() { ssh -D 57600 -p $1 cluster ;} # not working
  function ssh-dl() { scp -C -P $1 cluster:$2 $3 ;} # where 1 is the case 2 is remote file and 3 is local file
  function ssh-dldir() { scp -r -C -P $1 cluster:$2 $3 ;}
  function ssh-upload() { scp -C -P $1 $2 cluster:$3 ;} # where 1 is case 2 is local file and 3 is remote file

  if [ $commands[kubectl] ]; then
  fi

  if ! pgrep -u "$USER" ssh-agent > /dev/null; then
  fi
  if [[ "$SSH_AGENT_PID" == "" ]]; then
  fi

  autoload -Uz compinit compdef promptinit
  compinit
  promptinit
  zstyle ':completion:*' menu select
  setopt COMPLETE_ALIASES

  source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  zstyle ':completion:*' rehash true
