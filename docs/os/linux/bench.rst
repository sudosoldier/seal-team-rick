Benchmarking
~~~~~~~~~~~~

https://github.com/akopytov/sysbench#usage
https://github.com/axboe/fio


Tool map
--------

.. image:: img/perf_tools.png

Bonnie++
--------

  272  echo -e "start=`date`" > /tmp/`hostname`.log && bonnie++ -d /opt/splunk/var/lib/splunk/defaultdb/db/ -s 80000 -u root:root -fq -n 25\npath="/opt/splunk/var/lib/splunk/defaultdb/db/" >> /tmp/`hostname`.log && echo -e "finish=`date`\n--- EOF ---" >> /tmp/`hostname`.log
