Storage
~~~~~~~~

Change device naming
--------------------
To change a device name during boot you will need to write a `udev` rule. You will need to match attributes of the block device in order to identify it.

In this scenario, we will be adding an additonal name to `/dev/vdb` as `/dev/sdb`. This was done for a case in which a linux VM appliance was unable to boot, it was looking for a hardcoded `/dev/sdb` to load a database from, which is suboptimal as that would force us to use IDE drivers which are much slower.

The appliance was a Watchguard Dimension Server. https://www.watchguard.com/wgrd-products/watchguard-dimension

After booting to a linux live CD, examine the current device attributes to be used for identification:

.. code-block:: console

  # udevadm info -a -p $(find /sys/block -name vdb) | less

In this case I chose to use the KERNEL, SUBSYSTEM, and ATTR{size} fields.

With the VM's partition containing `fstab` mounted on `mnt`, create the rules file as follows:

.. code-block:: console

  # echo 'SUBSYSTEM=="block", KERNEL=="vdb", ATTR{size}=="524288000", SYMLINK+="sdb"' > /mnt/etc/udev/rules.d/10-local.rules

We have matched on the first three parameters (hence the == operators), and we are adding `sdb` to the SYMLINKs. Note that we are not using `=` to set SYMLINK as that would overwrite any other SYMLINK that may be set by other rules, etc.

SAS
---

.. code-block:: console

  # badblocks -sv /dev/sda
	# smartctl -x /dev/sda
  # systool -a -v -c scsi_host

hdparm
-------

.. code-block:: console

  # hdparm /dev/sda
  # hdparm -i /dev/sda
  # hdparm -I /dev/sda

.. code-block:: console

  # hdparm -Tt /dev/sda

  /dev/sda:
   Timing cached reads:   13138 MB in  1.99 seconds = 6594.57 MB/sec
   Timing buffered disk reads: 430 MB in  3.26 seconds = 131.84 MB/sec


Bootloader
-----------

.. code-block:: console

  # file -sb /dev/sda


Hardware Raid
--------------
https://www.maths.cam.ac.uk/computing/docs/public/megacli_raid_lsi.html
https://wikitech.wikimedia.org/wiki/MegaCli


Check to see if the node is using the MegaRAID controller:

.. code-block:: console

  # lspci | grep -i mega


.. code-block:: console

  # /opt/MegaRAID/MegaCli/MegaCli64 -AdpAllInfo -aAll
  # /opt/MegaRAID/MegaCli/MegaCli64 -PDList -aALL
  # /opt/MegaRAID/MegaCli/MegaCli64 -AdpPR -Info -aALL
  # /opt/MegaRAID/MegaCli/MegaCli64 -AdpGetProp PatrolReadRate -aALL
  # /opt/MegaRAID/MegaCli/MegaCli64 CfgDsply -aALL
  # cd /tmp; /opt/MegaRAID/MegaCli/MegaCli64 -AdpEventLog -GetEvents -f events.log -aALL && tail -n 30 events.log
  # /opt/MegaRAID/MegaCli/MegaCli64 -EncInfo -aALL
  # /opt/MegaRAID/MegaCli/MegaCli64 -AdpBbuCmd -aALL
  # /opt/MegaRAID/MegaCli/MegaCli64 -ShowSummary -aALL


LVM
---

Expand
______

Ubuntu:

.. code-block:: console

  $ sudo lvextend -l +100%FREE /dev/ubuntu-vg/ubuntu-lv && sudo resize2fs /dev/dm-0


RHEL:

.. code-block:: console

  # fdisk /dev/sda

  Command (m for help): p

    Device Boot      Start         End      Blocks   Id  System
  /dev/sda1   *        2048     2099199     1048576   83  Linux
  /dev/sda2         2099200    33554431    15727616   8e  Linux LVM


  Command (m for help): d
  Partition number (1,2, default 2): 2
  Partition 2 is deleted

  Command (m for help): n
  Select (default p): p
  Partition number (2-4, default 2):
  First sector (2099200-125829119, default 2099200): 2099200
  Last sector, +sectors or +size{K,M,G} (2099200-125829119, default 125829119): 125829119

  Command (m for help): w

  # partprobe -s
  # pvresize /dev/sda2
  # lvextend -l +100%FREE /dev/rhel/root
  # xfs_growfs /

Mount 
_____

This example will use a dd created img but will work with minor adjustments from a live cd as well.

To mount a dd created img:

.. code-block:: console
  
  $ ssh <SERVER> "sudo dd if=/dev/VolGroup/lv_root | gzip -1 -" | dd status=progress of=<SERVER>.img.gz
  $ gunzip <SERVER>.img.gz
  $ sudo losetup -f -P <FILENAME>.img
  $ sudo lvmdiskscan
    /dev/loop2p1                                          [     500.00 MiB]
    /dev/loop0                                            [      <2.22 MiB]
    /dev/mapper/luks-e9e2dea6-72ce-4d4c-87d1-e560ed013e96 [     236.46 GiB]
    /dev/loop2p2                                          [      14.51 GiB] LVM physical volume
    /dev/loop1                                            [      88.48 MiB]
    /dev/sda1                                             [       1.00 GiB]
    /dev/sda2                                             [       1.00 GiB]
    /dev/sda3                                             [    <236.47 GiB]
    0 disks
    7 partitions
    0 LVM physical volume whole disks
    1 LVM physical volume




Software Raid (md)
------------------

To check if one of the raid devices has fallen out of sync:

.. code-block:: console

	# mdadm --examine /dev/sd[a-z]2 | egrep "Event|/dev/sd"


To wipe the raid metadata and re-add the raid device to trigger a resync:

.. important:: be very careful when performing the above command, make sure you are running it on the partition and not the disk, you can obliterate the entire drive's data if you typo.

.. code-block:: console

	# mdadm --zero-superblock /dev/sda2
	# mdadm --add /dev/md1 /dev/sda2


Tiering
-------

If SSDs are high on usage you have three options:

**-The Gentle Way:**
	Adjust the tiering.limiter from 1 to 2 per node, see how it fares.

**-The Agressive Way:**
	Force a retier recover.

**-The Nuclear Option:**
	Set priority factor < 0 > ...


Performance
------------


iostat
______

.. code-block:: console

  iostat -x 2 5

iotop
_____

.. code-block:: console

	iotop -o


top
___
Run top then press `F` and then `u` to sort by page faults.


.. code-block:: console

	top

sar
___

.. code-block:: console

	# sar 1 7

vmstat
______

.. code-block:: console

	# vmstat 1

systemd-cgtop
______________

.. code-block:: console

  # systemd-cgtop -i
