Compute
~~~~~~~

mpstat
-------

mpstat provides processor statistics, the following are reported:

%usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle

.. code-block:: bash

  mpstat -P ALL


systemd-cgtop
--------------

Show top control groups by their resource usage.


To sort by CPU usage:

.. code-block:: console

  # systemd-cgtop -c

To sort by IO usage:

.. code-block:: console

  # systemd-cgtop -i


proc
----
.. code-block:: console

  # watch grep \"cpu MHz\" /proc/cpuinfo

clock settings for KVM guests
-----------------------------

https://www.suse.com/documentation/opensuse121/book_kvm/data/sec_kvm_managing_clock.html

https://access.redhat.com/solutions/18627

.. code-block:: console

  # cat /sys/devices/system/clocksource/clocksource0/current_clocksource
  # cat /sys/devices/system/clocksource/\*/available_clocksource

  ### Test a temporary (cleared on boot) change of the clocksource:
  # cat echo "acpi_pm" > /sys/devices/system/clocksource/clocksource0/current_clocksource

  # to make permanent, add clocksource=acpi_pm to your kernel commandline stanza in the bootloader


notsc

nokvmclock


buff/cache
-----------

https://www.tecmint.com/clear-ram-memory-cache-buffer-and-swap-space-on-linux/


.. code-block:: console

  ## only clear PageCache
  # sync; echo 1 > /proc/sys/vm/drop_caches
  ## clear dentires and inodes
  # sync; echo 2 > /proc/sys/vm/drop_caches
  ## clear PageCache, dentires, and inodes
  # sync; echo 3> /proc/sys/vm/drop_caches


turbostat
---------

omg so good

.. code-block:: console
  # turbostat

    turbostat version 19.03.20 - Len Brown <lenb@kernel.org>
    CPUID(0): GenuineIntel 0x16 CPUID levels; 0x80000008 xlevels; family:model:stepping 0x6:8e:a (6:142:10)
    CPUID(1): SSE3 MONITOR SMX EIST TM2 TSC MSR ACPI-TM HT TM
    CPUID(6): APERF, TURBO, DTS, PTM, HWP, HWPnotify, HWPwindow, HWPepp, No-HWPpkg, EPB
    cpu4: MSR_IA32_MISC_ENABLE: 0x00850089 (TCC EIST MWAIT PREFETCH TURBO)
    CPUID(7): SGX
    cpu4: MSR_IA32_FEATURE_CONTROL: 0x0000ff07 (Locked )
    CPUID(0x15): eax_crystal: 2 ebx_tsc: 176 ecx_crystal_hz: 0
    TSC: 2112 MHz (24000000 Hz * 176 / 2 / 1000000)
    CPUID(0x16): base_mhz: 2100 max_mhz: 4200 bus_mhz: 100
    cpu4: MSR_MISC_PWR_MGMT: 0x00401cc0 (ENable-EIST_Coordination DISable-EPB DISable-OOB)
    RAPL: 17476 sec. Joule Counter Range, at 15 Watts
    cpu4: MSR_PLATFORM_INFO: 0x804043df1011500
    4 * 100.0 = 400.0 MHz max efficiency frequency
    21 * 100.0 = 2100.0 MHz base frequency
    cpu4: MSR_IA32_POWER_CTL: 0x0024005d (C1E auto-promotion: DISabled)
    cpu4: MSR_TURBO_RATIO_LIMIT: 0x27272a2a
    39 * 100.0 = 3900.0 MHz max turbo 4 active cores
    39 * 100.0 = 3900.0 MHz max turbo 3 active cores
    42 * 100.0 = 4200.0 MHz max turbo 2 active cores
    42 * 100.0 = 4200.0 MHz max turbo 1 active cores
    cpu4: MSR_CONFIG_TDP_NOMINAL: 0x00000013 (base_ratio=19)
    cpu4: MSR_CONFIG_TDP_LEVEL_1: 0x00080050 (PKG_MIN_PWR_LVL1=0 PKG_MAX_PWR_LVL1=0 LVL1_RATIO=8 PKG_TDP_LVL1=80)
    cpu4: MSR_CONFIG_TDP_LEVEL_2: 0x001500c8 (PKG_MIN_PWR_LVL2=0 PKG_MAX_PWR_LVL2=0 LVL2_RATIO=21 PKG_TDP_LVL2=200)
    cpu4: MSR_CONFIG_TDP_CONTROL: 0x00000000 ( lock=0)
    cpu4: MSR_TURBO_ACTIVATION_RATIO: 0x00000017 (MAX_NON_TURBO_RATIO=23 lock=0)
    cpu4: MSR_PKG_CST_CONFIG_CONTROL: 0x1e008008 (UNdemote-C3, UNdemote-C1, demote-C3, demote-C1, locked, pkg-cstate-limit=8 (unlimited))
    cpu4: POLL: CPUIDLE CORE POLL IDLE
    cpu4: C1: MWAIT 0x00
    cpu4: C1E: MWAIT 0x01
    cpu4: C3: MWAIT 0x10
    cpu4: C6: MWAIT 0x20
    cpu4: C7s: MWAIT 0x33
    cpu4: C8: MWAIT 0x40
    cpu4: C9: MWAIT 0x50
    cpu4: C10: MWAIT 0x60
    cpu4: cpufreq driver: intel_pstate
    cpu4: cpufreq governor: performance
    cpufreq intel_pstate no_turbo: 0
    cpu4: MSR_MISC_FEATURE_CONTROL: 0x00000000 (L2-Prefetch L2-Prefetch-pair L1-Prefetch L1-IP-Prefetch)
    cpu0: MSR_PM_ENABLE: 0x00000001 (HWP)
    cpu0: MSR_HWP_CAPABILITIES: 0x0108132a (high 42 guar 19 eff 8 low 1)
    cpu0: MSR_HWP_REQUEST: 0x00002a2a (min 42 max 42 des 0 epp 0x0 window 0x0 pkg 0x0)
    cpu0: MSR_HWP_INTERRUPT: 0x00000000 (Dis_Guaranteed_Perf_Change, Dis_Excursion_Min)
    cpu0: MSR_HWP_STATUS: 0x00000004 (No-Guaranteed_Perf_Change, No-Excursion_Min)
    cpu0: MSR_IA32_ENERGY_PERF_BIAS: 0x00000006 (balanced)
    cpu0: MSR_RAPL_POWER_UNIT: 0x000a0e03 (0.125000 Watts, 0.000061 Joules, 0.000977 sec.)
    cpu0: MSR_PKG_POWER_INFO: 0x00000078 (15 W TDP, RAPL 0 - 0 W, 0.000000 sec.)
    cpu0: MSR_PKG_POWER_LIMIT: 0x42816000dd80b0 (UNlocked)
    cpu0: PKG Limit #1: ENabled (22.000000 Watts, 28.000000 sec, clamp ENabled)
    cpu0: PKG Limit #2: ENabled (44.000000 Watts, 0.002441* sec, clamp DISabled)
    cpu0: MSR_DRAM_POWER_LIMIT: 0x5400de00000000 (UNlocked)
    cpu0: DRAM Limit: DISabled (0.000000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_PP0_POLICY: 0
    cpu0: MSR_PP0_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: Cores Limit: DISabled (0.000000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_PP1_POLICY: 0
    cpu0: MSR_PP1_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: GFX Limit: DISabled (0.000000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_IA32_TEMPERATURE_TARGET: 0x02640000 (100 C)
    cpu0: MSR_IA32_PACKAGE_THERM_STATUS: 0x88160802 (78 C)
    cpu0: MSR_IA32_PACKAGE_THERM_INTERRUPT: 0x00000003 (100 C, 100 C)
    cpu4: MSR_PKGC3_IRTL: 0x0000884e (valid, 79872 ns)
    cpu4: MSR_PKGC6_IRTL: 0x00008876 (valid, 120832 ns)
    cpu4: MSR_PKGC7_IRTL: 0x00008894 (valid, 151552 ns)
    cpu4: MSR_PKGC8_IRTL: 0x000088fa (valid, 256000 ns)
    cpu4: MSR_PKGC9_IRTL: 0x0000894c (valid, 339968 ns)
    cpu4: MSR_PKGC10_IRTL: 0x00008bf2 (valid, 1034240 ns)
    ^CCore  CPU     Avg_MHz Busy%   Bzy_MHz TSC_MHz IRQ     SMI     POLL    C1      C1E     C3      C6      C7s     C8      C9      C10     POLL%   C1%     C1E%    C3%     C6%     C7s%    C8%     C9%     C10%    CPU%c1  CPU%c3  CPU%c6  CPU%c7  CoreTmp PkgTmp  GFX%rc6 GFXMHz  Totl%C0 Any%C0  GFX%C0  CPUGFX% Pkg%pc2 Pkg%pc3     Pkg%pc6 Pkg%pc7 Pkg%pc8 Pkg%pc9 Pk%pc10 PkgWatt CorWatt GFXWatt RAMWatt PKG_%   RAM_%
    -       -       507     14.40   3527    2110    3772    0       37      404     875     399     1634    1       2789    73      918     0.00    0.59    2.39    0.94    9.07    0.00    38.39   2.26    30.48   20.43   0.95    10.64   53.58   76      76      0.00    1150    98.15   51.76   100.52  51.76   0.00    0.000.00    0.00    0.00    0.00    0.00    17.00   9.71    5.53    1.65    0.00    0.00
    0       0       478     13.50   3546    2108    662     0       0       32      91      84      366     0       459     1       2       0.00    0.98    2.19    1.29    19.39   0.00    60.47   0.30    0.56    25.78   1.49    18.51   40.73   75      76      0.00    1150    98.23   51.81   100.61  51.81   0.00    0.000.00    0.00    0.00    0.00    0.00    17.00   9.71    5.53    1.65    0.00    0.00
    0       4       536     14.98   3587    2109    995     0       3       65      173     78      321     1       466     34      151     0.00    0.57    3.67    1.49    13.16   0.00    39.67   2.55    22.51   24.33
    1       1       461     13.25   3481    2109    369     0       0       197     154     27      142     0       331     19      217     0.00    2.25    1.97    0.63    5.93    0.00    26.36   9.53    38.80   16.54   1.20    9.60    59.42   71
    1       5       363     10.40   3490    2110    360     0       0       35      106     46      181     0       288     1       111     0.00    0.23    1.74    1.29    8.25    0.00    32.31   0.80    43.89   19.40
    2       2       657     18.68   3522    2110    284     0       11      13      65      26      113     0       203     6       144     0.01    0.07    2.24    0.42    4.84    0.00    20.62   1.40    49.56   15.99   0.51    6.22    58.60   76
    2       6       521     14.85   3510    2111    352     0       0       15      88      25      142     0       308     3       125     0.00    0.05    1.55    0.82    6.18    0.00    39.46   0.81    34.76   19.83
    3       3       516     14.70   3509    2111    362     0       14      29      146     66      186     0       335     2       114     0.00    0.25    4.46    0.67    6.52    0.00    36.71   0.57    34.61   20.84   0.61    8.27    55.58   74
    3       7       528     14.85   3555    2112    388     0       9       18      52      47      183     0       399     7       54      0.01    0.31    1.29    0.91    8.25    0.00    51.55   2.11    19.15   20.70