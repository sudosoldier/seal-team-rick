Debugging
=========
https://www.kernel.org/doc/html/v4.15/dev-tools/index.html

http://www.brendangregg.com/linuxperf.html

http://www.brendangregg.com/ktap.html


pstack
~~~~~~
Generates a stack trace of the running process

.. code-block:: console

  # pstack <pid>
  # pstack $(pidof REDACTED)

gdb
~~~
Generate a core dump of the process

.. code-block:: console

  # gcore [-o filename] <pid>
  # gcore -o /tmp/qemu12345.core 12345


strace
~~~~~~

.. warning:: Using strace causes significant overhead and will slow down the process considerably.

https://nanxiao.gitbooks.io/strace-little-book/content/

.. code-block:: console

  # strace -yy -f -p <pid>


kvm
~~~

https://www.linux-kvm.org/page/Perf_events


KVM debug tracing

.. code-block:: console

  # echo 1 > /sys/kernel/debug/tracing/events/kvm/enable
  # cat /sys/kernel/debug/tracing/trace_pipe
  # echo 0 > /sys/kernel/debug/tracing/events/kvm/enable

Perf events recording

.. code-block:: console

  # perf kvm --host --guest [kvm options] record -a -o /tmp/perf.data.kvm
  # perf kvm --host --guest report -i /tmp/perf.data.kvm


trace-cmd
~~~~~~~~~

https://www.linux-kvm.org/page/Tracing

ftrace
~~~~~~


Raw copypasta
~~~~~~~~~~~~~
Need to parse this stuff

https://www.redhat.com/archives/libvirt-users/2018-January/msg00036.html **

.. code-block:: console

  ```[root@REDACTED-10-10-41-20 ~ 17:52:29 ]# mount -t tmpfs coretmp /mnt/```

  ```[root@REDACTED-10-10-41-20 ~ 17:52:29 ]# mount -t tmpfs coretmp /mnt/```


  ```-rw-r--r-- 1 root root 22G Oct  1 17:56 /mnt/REDACTED.northDevon.core.8761```


  [root@REDACTED-10-10-41-20 ~ 18:08:20 ]# ls -lha /mnt/REDACTED.northDevon.core.8761*
  -rw-r--r-- 1 root root  22G Oct  1 17:56 /mnt/REDACTED.northDevon.core.8761
  -rw-r--r-- 1 root root 475M Oct  1 17:56 /mnt/REDACTED.northDevon.core.8761.xz

  [root@REDACTED-10-10-41-20 ~ 18:08:23 ]# sha256sum /mnt/REDACTED.northDevon.core.8761
  3bfe6735bb457b4f34cafa2e3ee3b0056eb8e6bc0c5a4291f099a47f2b29af8f  /mnt/REDACTED.northDevon.core.8761

  [root@REDACTED-10-10-41-10 ~ 19:49:15 ]# sc vsdreplication show uuid 89084906-5cf6-464e-ac5c-a93920f8d5ac display detail
  ========================================================== ROW 1 ==========================================================
  List   : UUID                                  Enabled  Conn      Source    Target    Status
           ====================================  =======  ========  ========  ========  =============================
           7ed0ad2a-20b0-4b2c-b209-6e80ddca4a0c  N        58da74d4  89084906  83e17f2e  TRANSFER_BLOCKS 0->1 1/141519
  ========================================================== ROW 2 ==========================================================
  List   : ID    WorkType         VSD       From  To  CurrentBlock  BlockDiff  BlocksSent  Progress  Started
           ====  ===============  ========  ====  ==  ============  =========  ==========  ========  =======================
           1055  CREATE_SNAPSHOT            0     1   0             0          0           0         -
           1054  TRANSFER_BLOCKS  89084906  0     1   0             141519     1           0         2018-04-03 14:11:08.000
  ========================================================== ROW 3 ==========================================================
  List   : ID    WorkType         From  To   BlockDiff  Started                  Finished
           ====  ===============  ====  ===  =========  =======================  =======================
           1053  CREATE_TARGET    0     0    0          2018-04-03 14:10:38.000  2018-04-03 14:10:38.000
           1052  CREATE_SNAPSHOT  525   526  -          2018-04-03 13:47:42.000  2018-04-03 13:47:44.000
           1051  TRANSFER_BLOCKS  525   526  602        2018-04-03 13:46:37.000  2018-04-03 13:47:42.000
           1050  CREATE_SNAPSHOT  524   525  -          2018-04-03 13:32:08.000  2018-04-03 13:32:09.000
           1049  TRANSFER_BLOCKS  524   525  571        2018-04-03 13:31:05.000  2018-04-03 13:32:08.000
           1048  CREATE_SNAPSHOT  523   524  -          2018-04-03 13:19:16.000  2018-04-03 13:19:18.000



  5308160,(boost::shared_ptr<RSDRefcountSectorFacade>::element_type)
  10108168,(std::_Vector_base<unsigned int
  279890320,(std::_Rb_tree_node<RSDAddress>)
  972656000,(std::_Vector_base<std::array<unsigned int
  total: 1274171548


  ```[root@REDACTED-10-10-41-20 ~ 17:31:26 ]# ps -C REDACTED -O rsz,trs,drs
    PID   RSZ  TRS   DRS S TTY          TIME COMMAND
   8761 18898016 11072 22803827 S ? 42-17:48:58 /opt/REDACTED/sbin/REDACTED -c /opt/REDACTED/etc/REDACTED.conf
  [root@REDACTED-10-10-41-20 ~ 17:31:40 ]#```

  well, that is a big increase



  @keaton I am setting up to look at some more things. It is probably less disruptive than the core but has the potential to be more disruptive (crash REDACTED). I wanted to let you know while I am trying it out on a testbed



  [root@REDACTED-10-10-41-20 ~ 17:33:11 ]# cat >print_info.gdb
  set $newfd = open("/tmp/memory_profile", 02101)
  set $oldfd = dup(2)
  call dup2($newfd, 2)
  call close($newfd)
  call malloc_stats()

  call dup2($oldfd, 2)
  call close($oldfd)

  [root@REDACTED-10-10-41-20 ~ 17:55:43 ]# ps -C REDACTED -O rsz,trs,drs
    PID   RSZ  TRS   DRS S TTY          TIME COMMAND
   8761 18780308 11072 22673663 S ? 42-18:00:07 /opt/REDACTED/sbin/REDACTED -c /opt/REDACTED/etc/REDACTED.conf

   [root@REDACTED-10-10-41-20 ~ 17:56:02 ]# gdb /opt/REDACTED/sbin/REDACTED 8761 --batch -x print_info.gdb


   [root@REDACTED-10-10-41-20 ~ 17:57:25 ]# cat /tmp/memory_profile
  ------------------------------------------------
  MALLOC:    18672650544 (17807.6 MiB) Bytes in use by application
  MALLOC: +            0 (    0.0 MiB) Bytes in page heap freelist
  MALLOC: +    424772240 (  405.1 MiB) Bytes in central cache freelist
  MALLOC: +      1880064 (    1.8 MiB) Bytes in transfer cache freelist
  MALLOC: +     19907648 (   19.0 MiB) Bytes in thread cache freelists
  MALLOC: +     59453600 (   56.7 MiB) Bytes in malloc metadata
  MALLOC:   ------------
  MALLOC: =  19178664096 (18290.2 MiB) Actual memory used (physical + swap)
  MALLOC: +   2551545856 ( 2433.3 MiB) Bytes released to OS (aka unmapped)
  MALLOC:   ------------
  MALLOC: =  21730209952 (20723.5 MiB) Virtual address space used
  MALLOC:
  MALLOC:         720959              Spans in use
  MALLOC:            135              Thread heaps in use
  MALLOC:           8192              Tcmalloc page size
  ------------------------------------------------
  Call ReleaseFreeMemory() to release freelist memory to the OS (via madvise()).
  Bytes released to the OS take up virtual address space but no physical memory.
