Networking
~~~~~~~~~~
https://community.mellanox.com/docs/DOC-1481
https://community.mellanox.com/docs/DOC-2086#jive_content_id_RDMA_Drivers

Wonderful diagrams
------------------

!!!!!

http://linux-ip.net/pages/diagrams.html

** tcpwrappers

icmp redirects and bad firewalls are have nasty gotchas and nobody will believe you especially if they are a developer
----------------------------------------------------------------------------------------------------------------------

https://docs.netgate.com/pfsense/en/latest/firewall/troubleshooting-blocked-log-entries-due-to-asymmetric-routing.html



queue latency
-------------
https://bravenewgeek.com/benchmarking-message-queue-latency/


uperf
-----
http://uperf.org/

bcc
---
Performance troubleshooting tools based on the Berkley Packet Filter

https://github.com/iovisor/bcc
https://github.com/iovisor/bpftrace
https://github.com/iovisor/bcc/blob/master/docs/tutorial.md


wireshark
---------
https://networkengineering.stackexchange.com/questions/2012/why-do-i-see-a-rst-ack-packet-instead-of-a-rst-packet


netfilter
---------

.. code-block:: console

  # cat /proc/net/nf_conntrack

netlink
-------

.. code-block:: console

  # nl-tctree-list
  # cat /proc/net/netlink | column -t

tc
---
Traffic Control is the Quality of Service (QoS) mechanism in linux.

http://linux-ip.net/articles/Traffic-Control-HOWTO/

https://bravenewgeek.com/benchmarking-message-queue-latency/


https://elixir.bootlin.com/linux/v3.10/ident/dev_requeue_skb
https://github.com/Mellanox/mlxsw/wiki/Queues-Management
http://tldp.org/en/Traffic-Control-HOWTO/ar01s07.html
https://linux.die.net/man/8/tc-prio
https://elixir.bootlin.com/linux/latest/source/Documentation/networking/scaling.txt

.. code-block:: console

    $ tc -s qdisc ls dev enp57s0u1u1
    or
    $ tc -s qdisc show dev enp57s0u1u1

    qdisc fq_codel 0: root refcnt 2 limit 10240p flows 1024 quantum 1514 target 5.0ms interval 100.0ms memory_limit 32Mb ecn
    Sent 53323701 bytes 132394 pkt (dropped 0, overlimits 0 requeues 55)
    backlog 0b 0p requeues 55
    maxpacket 1125 drop_overlimit 0 new_flow_count 347 ecn_mark 0
    new_flows_len 0 old_flows_len 0


    tc-basic(8),  tc-bfifo(8),  tc-bpf(8),  tc-cbq(8),  tc-cgroup(8),  tc-choke(8), tc-
    codel(8),  tc-drr(8),  tc-ematch(8),  tc-flow(8),   tc-flower(8),   tc-fq(8),   tc-
    fq_codel(8),   tc-fw(8),   tc-hfsc(7),  tc-hfsc(8),  tc-htb(8),  tc-mqprio(8),  tc-
    pfifo(8),  tc-pfifo_fast(8),  tc-red(8),  tc-route(8),  tc-sfb(8),  tc-sfq(8),  tc-
    stab(8), tc-tbf(8), tc-tcindex(8), tc-u32(8),


https://unix.stackexchange.com/questions/96804/tc-show-output-explanation
http://qos.ittc.ku.edu/howto/node11.html
https://www.tldp.org/HOWTO/Adv-Routing-HOWTO/lartc.qdisc.classful.html
http://wiki.openwrt.org/doc/howto/packet.scheduler/packet.scheduler#configuration
http://wiki.openwrt.org/doc/howto/packet.scheduler/packet.scheduler
http://wiki.linuxwall.info/doku.php/en%3aressources%3adossiers%3anetworking%3atraffic_control
https://www.funtoo.org/Traffic_Control

.. code-block:: console

  [192.168.13.124 (edaed01a) ~ 12:21:52]# tc -p -s -d  qdisc show
  qdisc noqueue 0: dev lo root refcnt 2
   Sent 0 bytes 0 pkt (dropped 0, overlimits 0 requeues 0)
   backlog 0b 0p requeues 0
  qdisc htb 2: dev lan0 root refcnt 9 r2q 10 default 1 direct_packets_stat 82775199 ver 3.17 direct_qlen 1000
   Sent 303883379504 bytes 300867962 pkt (dropped 0, overlimits 356834441 requeues 50246)
   backlog 0b 0p requeues 50246
  qdisc mq 0: dev backplane0 root
   Sent 46609729595890 bytes 530996963 pkt (dropped 0, overlimits 0 requeues 23541855)
   backlog 0b 0p requeues 23541855
  qdisc pfifo_fast 0: dev backplane0 parent :8 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
   Sent 12489651106107 bytes 1375073910 pkt (dropped 0, overlimits 0 requeues 5204930)
   backlog 0b 0p requeues 5204930
  qdisc pfifo_fast 0: dev backplane0 parent :7 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
   Sent 13638163747329 bytes 2986448756 pkt (dropped 0, overlimits 0 requeues 6728757)
   backlog 0b 0p requeues 6728757
  qdisc pfifo_fast 0: dev backplane0 parent :6 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
   Sent 20323372040750 bytes 3820452592 pkt (dropped 0, overlimits 0 requeues 11342304)
   backlog 0b 0p requeues 11342304
  qdisc pfifo_fast 0: dev backplane0 parent :5 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
   Sent 34151423650 bytes 286440199 pkt (dropped 0, overlimits 0 requeues 830)
   backlog 0b 0p requeues 830
  qdisc pfifo_fast 0: dev backplane0 parent :4 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
   Sent 44266157756 bytes 272710645 pkt (dropped 0, overlimits 0 requeues 17599)
   backlog 0b 0p requeues 17599
  qdisc pfifo_fast 0: dev backplane0 parent :3 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
   Sent 6697098552 bytes 29496369 pkt (dropped 0, overlimits 0 requeues 48532)
   backlog 0b 0p requeues 48532
  qdisc pfifo_fast 0: dev backplane0 parent :2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
   Sent 41519193121 bytes 171748054 pkt (dropped 0, overlimits 0 requeues 195806)
   backlog 0b 0p requeues 195806
  qdisc pfifo_fast 0: dev backplane0 parent :1 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
   Sent 31908828625 bytes 178561030 pkt (dropped 0, overlimits 0 requeues 3097)
   backlog 0b 0p requeues 3097


ethtool
-------

To blink the LEDs and identify a NIC:

.. code-block:: console

  # ethtool --identify backplane0

To check physical (NIC) statistics:

.. code-block:: console

  # ethtool -S backplane0

.. code-block:: console

  msglvl N
  msglvl type on|off ...
        Sets  the  driver  message type flags by name or number. type names the type of message to enable or disable; N specifies the new flags numerically. The de‐
        fined type names and numbers are:

        drv         0x0001  General driver status
        probe       0x0002  Hardware probing
        link        0x0004  Link state
        timer       0x0008  Periodic status check
        ifdown      0x0010  Interface being brought down
        ifup        0x0020  Interface being brought up
        rx_err      0x0040  Receive error
        tx_err      0x0080  Transmit error
        tx_queued   0x0100  Transmit queueing
        intr        0x0200  Interrupt handling
        tx_done     0x0400  Transmit completion
        rx_status   0x0800  Receive completion
        pktdata     0x1000  Packet contents
        hw          0x2000  Hardware status
        wol         0x4000  Wake-on-LAN status

        The precise meanings of these type flags differ between drivers.


iptables
--------

.. code-block:: console

  # iptables-save > iptables.save && iptables -F
  # iptables-restore < iptables.save
  # iptables -L -v -n





ip
---

.. code-block:: console

  # ip -s -s link show backplane0
  # ip -s -s link show backplaneBond


ss
---

http://man7.org/linux/man-pages/man8/ss.8.html


.. code-block:: console

  # ss -temoin

nmap
----

.. code-block:: console

  # nmap -sn -oA find-the-dell-switches -e lan -n -T5 10.10.1.0/24
  # nmap -sP -sn -host-timeout 10s -max-hostgroup 10 -min-hostgroup 5 -T4 --max-retries 2 -oA nmapsweep -n --traceroute 10.10.0.0/16
  # nmap -sn x.x.x.x/24  # ping sweep scan
  # nmap -p 139,445 x.x.x.x # check open ports for SMB file share

bridge
------

.. code-block:: console
  # bridge -d -s link show

nstat
-----

.. code-block:: console

  # nstat

  #kernel
  IpInReceives                    75710259           0.0
  IpInDelivers                    75102617           0.0
  IpOutRequests                   75060752           0.0
  IpOutNoRoutes                   48                 0.0
  IcmpInMsgs                      196743             0.0
  IcmpInErrors                    1599               0.0
  IcmpInEchos                     101881             0.0
  IcmpInEchoReps                  93263              0.0
  IcmpOutMsgs                     195180             0.0
  IcmpOutDestUnreachs             36                 0.0
  IcmpOutEchoReps                 101881             0.0
  IcmpMsgInType0                  93263              0.0
  IcmpMsgInType8                  101881             0.0
  IcmpMsgInType9                  1599               0.0
  IcmpMsgOutType0                 101881             0.0
  IcmpMsgOutType3                 36                 0.0
  IcmpMsgOutType69                93263              0.0
  TcpActiveOpens                  824028             0.0
  TcpPassiveOpens                 823996             0.0
  TcpAttemptFails                 79                 0.0
  TcpEstabResets                  538604             0.0
  TcpInSegs                       74808818           0.0
  TcpOutSegs                      74810755           0.0
  TcpRetransSegs                  1140               0.0
  TcpInErrs                       49                 0.0
  TcpOutRsts                      539225             0.0
  UdpInDatagrams                  109988             0.0
  UdpNoPorts                      4                  0.0
  UdpOutDatagrams                 63803              0.0
  Ip6InReceives                   360081             0.0
  Ip6InDelivers                   34                 0.0
  Ip6OutRequests                  41                 0.0
  Ip6InMcastPkts                  360081             0.0
  Ip6OutMcastPkts                 61                 0.0
  Ip6InOctets                     50196119           0.0
  Ip6OutOctets                    2752               0.0
  Ip6InMcastOctets                50196119           0.0
  Ip6OutMcastOctets               4272               0.0
  Ip6InNoECTPkts                  360081             0.0
  Icmp6InMsgs                     34                 0.0
  Icmp6OutMsgs                    41                 0.0
  Icmp6InNeighborAdvertisements   34                 0.0
  Icmp6OutRouterSolicits          15                 0.0
  Icmp6OutNeighborSolicits        5                  0.0
  Icmp6OutNeighborAdvertisements  1                  0.0
  Icmp6OutMLDv2Reports            20                 0.0
  Icmp6InType136                  34                 0.0
  Icmp6OutType133                 15                 0.0
  Icmp6OutType135                 5                  0.0
  Icmp6OutType136                 1                  0.0
  Icmp6OutType143                 20                 0.0
  TcpExtSyncookiesFailed          4                  0.0
  TcpExtTW                        285059             0.0
  TcpExtDelayedACKs               2596687            0.0
  TcpExtDelayedACKLocked          203                0.0
  TcpExtDelayedACKLost            229                0.0
  TcpExtTCPPrequeued              1815726            0.0
  TcpExtTCPDirectCopyFromBacklog  8590523            0.0
  TcpExtTCPDirectCopyFromPrequeue 712888051          0.0
  TcpExtTCPHPHits                 28775072           0.0
  TcpExtTCPHPHitsToUser           1832036            0.0
  TcpExtTCPPureAcks               5576987            0.0
  TcpExtTCPHPAcks                 22779712           0.0
  TcpExtTCPSackRecovery           28                 0.0
  TcpExtTCPDSACKUndo              22                 0.0
  TcpExtTCPLossUndo               6                  0.0
  TcpExtTCPLostRetransmit         59                 0.0
  TcpExtTCPFastRetrans            336                0.0
  TcpExtTCPForwardRetrans         42                 0.0
  TcpExtTCPTimeouts               157                0.0
  TcpExtTCPLossProbes             445                0.0
  TcpExtTCPLossProbeRecovery      129                0.0
  TcpExtTCPDSACKOldSent           229                0.0
  TcpExtTCPDSACKRecv              500                0.0
  TcpExtTCPDSACKOfoRecv           1                  0.0
  TcpExtTCPAbortOnData            538786             0.0
  TcpExtTCPAbortOnClose           44                 0.0
  TcpExtTCPAbortOnTimeout         3                  0.0
  TcpExtTCPDSACKIgnoredNoUndo     176                0.0
  TcpExtTCPSackShifted            29                 0.0
  TcpExtTCPSackMerged             59                 0.0
  TcpExtTCPSackShiftFallback      125                0.0
  TcpExtTCPDeferAcceptDrop        539574             0.0
  TcpExtTCPRcvCoalesce            477502             0.0
  TcpExtTCPOFOQueue               1071               0.0
  TcpExtTCPChallengeACK           49                 0.0
  TcpExtTCPSYNChallenge           49                 0.0
  TcpExtBusyPollRxPackets         7242               0.0
  TcpExtTCPAutoCorking            302                0.0
  TcpExtTCPSynRetrans             304                0.0
  TcpExtTCPOrigDataSent           51909898           0.0
  TcpExtTCPHystartTrainDetect     28                 0.0
  TcpExtTCPHystartTrainCwnd       659                0.0
  TcpExtTCPHystartDelayDetect     3                  0.0
  TcpExtTCPHystartDelayCwnd       627                0.0
  IpExtInMcastPkts                1599               0.0
  IpExtInBcastPkts                129530             0.0
  IpExtOutBcastPkts               12976              0.0
  IpExtInOctets                   17939332041        0.0
  IpExtOutOctets                  17874043371        0.0
  IpExtInMcastOctets              57564              0.0
  IpExtInBcastOctets              15479896           0.0
  IpExtOutBcastOctets             1245696            0.0
  IpExtInNoECTPkts                75710259           0.0


nping
-----

tcpdump
-------
https://unix.stackexchange.com/questions/144794/why-would-the-kernel-drop-packets
https://wiki.wireshark.org/DisplayFilters

Check for STP:

.. code-block:: console

  # tcpdump -v -i any -c 10 stp

Check for LLDP:

.. code-block:: console

  # tcpdump -v -i any ether proto 0x88cc


iperf
------

iperf -s &
iperf -c <IP> -r -P 5


traceroute
----------

iperf3
-------
https://iperf.fr/iperf-doc.php
https://iperf.fr/iperf-download.php

sockperf
--------


Troubleshooting
~~~~~~~~~~~~~~~
https://support.cumulusnetworks.com/hc/en-us/articles/205544578#cause_of_the_errors
https://docs.cumulusnetworks.com/display/DOCS/Network+Troubleshooting

https://blogs.dropbox.com/tech/2017/09/optimizing-web-servers-for-high-throughput-and-low-latency/

TCP
---
https://en.wikipedia.org/wiki/Out-of-order_delivery


https://blog.stackpath.com/glossary/cwnd-and-rwnd/

.. code-block:: console

  # printf '\t\t' ; cat /proc/net/dev | column -t


ICMP Redirects
--------------

RCVD (22.8873s) ICMP [10.205.2.1 > 10.205.2.150 Network redirect (type=5/code=0) addr=10.205.2.254] IP [ttl=255 id=16228 iplen=56 ]

ntop
------
https://www.ntop.org/

Useful links/TODO
~~~~~~~~~~~~~~~~~
https://anarc.at/blog/2017-04-29-netdev-rise-linux-networking-hw/
https://www.linuxjournal.com/article/8381
http://www.haifux.org/lectures/172/netLec.pdf
http://vger.kernel.org/lpc-networking.html
https://www.netdevconf.org/0.1/sessions/24.html
https://www.google.com/search?q=linux+netdev+tools&source=lnt&tbs=qdr:y&sa=X&ved=0ahUKEwjZzOHCi9neAhXhj4MKHYVMD7YQpwUIJA&biw=2955&bih=1590


udevadm
-------

.. code-block:: console

  # udevadm info /sys/class/net/lan0


dem good links
--------------

https://github.com/brendangregg/perf-tools/blob/master/net/tcpretrans

Tips
----

Check reachable hosts in the local network by pinging the broadcast address then displaying the ARP/neighbor table.

https://wiki.linuxfoundation.org/networking/neighboring_subsystem
http://man7.org/linux/man-pages/man8/ip-neighbour.8.html

.. code-block:: console

  [172.16.109.124 (47dad3af) ~ 16:21:38]# ip addr show lan | grep brd
  [172.16.109.124 (47dad3af) ~ 16:22:37]# ping -c 1 -b 10.205.109.255
  [172.16.109.124 (47dad3af) ~ 16:22:56]# ip neigh show
  10.205.109.101 dev lan lladdr a0:36:9f:1e:52:9a STALE
  10.205.108.7 dev lan lladdr 7c:4c:58:a8:c4:5b STALE
  10.205.108.8 dev lan lladdr 7c:4c:58:62:46:d3 STALE
  10.205.108.36 dev lan lladdr 7c:4c:58:ca:ea:eb STALE
  10.205.108.255 dev lan lladdr 7c:4c:58:59:95:5d STALE
  10.205.109.132 dev lan lladdr c8:1f:66:ba:34:49 STALE
  10.205.108.254 dev lan lladdr 7c:4c:58:0f:03:e7 STALE
  10.205.108.3 dev lan lladdr f0:f7:55:7c:99:c3 STALE
  10.205.108.2 dev lan lladdr 30:e4:db:12:5d:49 REACHABLE
  10.205.108.125 dev lan  FAILED
  10.205.109.128 dev lan lladdr 90:b1:1c:40:3b:de STALE
  10.205.108.124 dev lan  FAILED
  10.205.108.85 dev lan lladdr 7c:4c:58:b9:15:e7 STALE
  10.205.108.84 dev lan lladdr 7c:4c:58:42:fb:df STALE
  10.205.109.253 dev lan lladdr d0:67:e5:d0:e1:95 STALE
  10.205.108.128 dev lan lladdr da:ba:d5:03:2f:4a STALE
  10.205.109.36 dev lan lladdr 7c:d3:0a:5b:5e:d5 STALE
  10.205.109.252 dev lan lladdr 28:92:4a:7a:cd:c0 STALE
  fe80::7e4c:58ff:fe0f:b19a dev lan lladdr 7c:4c:58:0f:b1:9a STALE
  fe80::7e4c:58ff:fee9:2b89 dev lan  FAILED
  fe80::7e4c:58ff:fe02:d993 dev lan lladdr 7c:4c:58:02:d9:93 STALE
  fe80::7e4c:58ff:fefa:c9ed dev lan  FAILED
  fe80::7e4c:58ff:fe19:4750 dev lan lladdr 7c:4c:58:19:47:50 STALE
