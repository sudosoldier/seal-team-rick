========================
MOUSE TRACKING IS OFFSET
========================
Anything that is an old outdated linux is going to have this problem.

This was seen in a Gnome2 desktop, so any CentOS 6 or RHEL 6 will likely have this issue. The default resolution is 1280 x 768 which is a 17:9 aspect ratio. Changing to any 4:3 ratio, such as 1024 x 768, will resolve the issue


===================
QEMU BOOTING ISSUES
===================

Check the kernel version

https://stackoverflow.com/questions/37672417/getting-kernel-version-from-the-compressed-kernel-image

pacman -Sy binutils

strings <image> | grep 'Linux Version'

================
MONITOR COMMANDS
================

.... code-block:: console
  [172.16.109.124 (47dad3af) ~ 21:56:24]# virsh qemu-monitor-command --hmp --domain 018e5b83-7b42-46da-809b-06242feae83d --cmd info jit
  Translation buffer state:
  gen code size       0/0
  TB count            0/0
  TB avg target size  0 max=0 bytes
  TB avg host size    0 bytes (expansion ratio: 0.0)
  cross page TB count 0 (0%)
  direct jump count   0 (0%) (2 jumps=0 0%)
  Statistics:
  TB flush count      0
  TB invalidate count 0
  TLB flush count     12
  [TCG profiler not compiled]
  [172.16.109.124 (47dad3af) ~ 21:47:06]# virsh qemu-monitor-command --hmp --domain 018e5b83-7b42-46da-809b-06242feae83d --cmd info trace-events
  # virsh qemu-monitor-command --hmp --domain 018e5b83-7b42-46da-809b-06242feae83d --cmd info network
  # [172.16.109.124 (47dad3af) ~ 21:34:24]# virsh qemu-monitor-command --hmp --domain 018e5b83-7b42-46da-809b-06242feae83d --cmd info registers
  RAX=fffff8035080680c RBX=000000000000000c RCX=000000000000000c RDX=0000000000000070
  RSI=000000000000000c RDI=fffff80352a6bf40 RBP=000000000000000c RSP=fffff80352a6be78
  R8 =fffff80352a6bf01 R9 =0000000000000001 R10=fffff8035069d558 R11=0000000000000000
  R12=00000e930f985510 R13=fffff80350697180 R14=0000000000000000 R15=fffff80350856f90
  RIP=fffff80350806868 RFL=00000046 [---Z-P-] CPL=0 II=0 A20=1 SMM=0 HLT=0
  ES =002b 0000000000000000 ffffffff 00c0f300 DPL=3 DS   [-WA]
  CS =0010 0000000000000000 00000000 00209b00 DPL=0 CS64 [-RA]
  SS =0018 0000000000000000 00000000 00409300 DPL=0 DS   [-WA]
  DS =002b 0000000000000000 ffffffff 00c0f300 DPL=3 DS   [-WA]
  FS =0053 0000000000000000 00003c00 0040f300 DPL=3 DS   [-WA]
  GS =002b fffff80350697000 ffffffff 00c0f300 DPL=3 DS   [-WA]
  LDT=0000 0000000000000000 000fffff 00000000
  TR =0040 fffff80352a53000 00000067 00008b00 DPL=0 TSS64-busy
  GDT=     fffff80352a54fb0 00000057
  IDT=     fffff80352a52000 00000fff
  CR0=80050031 CR2=ffffc10873b23000 CR3=00000000001aa000 CR4=000006f8
  DR0=0000000000000000 DR1=0000000000000000 DR2=0000000000000000 DR3=0000000000000000
  DR6=00000000ffff4ff0 DR7=0000000000000400
  EFER=0000000000000d01
  FCW=027f FSW=0000 [ST=0] FTW=00 MXCSR=00001f80
  FPR0=0000000000000000 0000 FPR1=0000000000000000 0000
  FPR2=0000000000000000 0000 FPR3=0000000000000000 0000
  FPR4=0000000000000000 0000 FPR5=0000000000000000 0000
  FPR6=0000000000000000 0000 FPR7=0000000000000000 0000
  XMM00=00000000000000000000000000000000 XMM01=00000000000000000000000000000000
  XMM02=00000000000000000000000000000000 XMM03=00000000000000000000000000000000
  XMM04=00000000000000000000000000000000 XMM05=00000000000000000000000000000000
  XMM06=00000000000000000000000000000000 XMM07=00000000000000000000000000000000
  XMM08=00000000000000000000000000000000 XMM09=00000000000000000000000000000000
  XMM10=00000000000000000000000000000000 XMM11=00000000000000000000000000000000
  XMM12=00000000000000000000000000000000 XMM13=00000000000000000000000000000000
  XMM14=00000000000000000000000000000000 XMM15=00000000000000000000000000000000

  [172.16.109.124 (47dad3af) ~ 21:40:10]# virsh qemu-monitor-command --hmp --domain 018e5b83-7b42-46da-809b-06242feae83d --cmd info qtree
  bus: main-system-bus
  type System
  dev: kvm-ioapic, id ""
  gpio-in "" 24
  gsi_base = 0 (0x0)
  mmio 00000000fec00000/0000000000001000
  dev: i440FX-pcihost, id ""
  pci-hole64-size = 18446744073709551615 (16 EiB)
  short_root_bus = 0 (0x0)
  bus: pci.0
  type PCI
  dev: VGA, id "video0"
  vgamem_mb = 16 (0x10)
  mmio = true
  qemu-extended-regs = false
  addr = 02.0
  romfile = "vgabios-stdvga.bin"
  rombar = 1 (0x1)
  multifunction = false
  command_serr_enable = true
  class VGA controller, addr 00:02.0, pci id 1234:1111 (sub 1af4:1100)
  bar 0: mem at 0xfd000000 [0xfdffffff]
  bar 2: mem at 0xfebd0000 [0xfebd0fff]
  bar 6: mem at 0xffffffffffffffff [0xfffe]
  dev: virtio-net-pci, id "net0"
  ioeventfd = false
  vectors = 3 (0x3)
  virtio-pci-bus-master-bug-migration = true
  disable-legacy = false
  disable-modern = true
  addr = 03.0
  romfile = "pxe-virtio.rom"
  rombar = 1 (0x1)
  multifunction = false
  command_serr_enable = true
  class Ethernet controller, addr 00:03.0, pci id 1af4:1000 (sub 1af4:0001)
  bar 0: i/o at 0xc060 [0xc07f]
  bar 1: mem at 0xfebd1000 [0xfebd1fff]
  bar 6: mem at 0xffffffffffffffff [0x3fffe]
  bus: virtio-bus
  type virtio-pci-bus
  dev: virtio-net-device, id ""
  csum = true
  guest_csum = true
  gso = true
  guest_tso4 = true
  guest_tso6 = true
  guest_ecn = true
  guest_ufo = true
  guest_announce = false
  host_tso4 = true
  host_tso6 = true
  host_ecn = true
  host_ufo = true
  mrg_rxbuf = true
  status = true
  ctrl_vq = true
  ctrl_rx = true
  ctrl_vlan = true
  ctrl_rx_extra = true
  ctrl_mac_addr = true
  ctrl_guest_offloads = false
  mq = false
  mac = "7c:4c:58:54:85:3b"
  vlan = <null>
  netdev = "hostnet0"
  x-txtimer = 150000 (0x249f0)
  x-txburst = 256 (0x100)
  tx = ""
  rx_queue_size = 256 (0x100)
  indirect_desc = true
  event_idx = true
  notify_on_empty = true
  any_layout = true
  __com.redhat_rhel6_ctrl_guest_workaround = false
  dev: virtio-blk-pci, id "virtio-disk0"
  class = 0 (0x0)
  ioeventfd = true
  vectors = 2 (0x2)
  virtio-pci-bus-master-bug-migration = true
  disable-legacy = false
  disable-modern = true
  addr = 04.0
  romfile = ""
  rombar = 1 (0x1)
  multifunction = false
  command_serr_enable = true
  class SCSI controller, addr 00:04.0, pci id 1af4:1001 (sub 1af4:0002)
  bar 0: i/o at 0xc000 [0xc03f]
  bar 1: mem at 0xfebd2000 [0xfebd2fff]
  bus: virtio-bus
  type virtio-pci-bus
  dev: virtio-blk-device, id ""
  drive = "drive-virtio-disk0"
  logical_block_size = 512 (0x200)
  physical_block_size = 512 (0x200)
  min_io_size = 0 (0x0)
  opt_io_size = 0 (0x0)
  discard_granularity = 4294967295 (0xffffffff)
  cyls = 16383 (0x3fff)
  heads = 16 (0x10)
  secs = 63 (0x3f)
  serial = "1c0929c1"
  config-wce = true
  scsi = false
  request-merging = true
  x-data-plane = false
  indirect_desc = true
  event_idx = true
  notify_on_empty = true
  any_layout = false
  __com.redhat_rhel6_ctrl_guest_workaround = false
  dev: virtio-serial-pci, id "virtio-serial0"
  ioeventfd = true
  vectors = 2 (0x2)
  class = 1920 (0x780)
  virtio-pci-bus-master-bug-migration = true
  disable-legacy = false
  disable-modern = true
  addr = 1f.0
  romfile = ""
  rombar = 1 (0x1)
  multifunction = false
  command_serr_enable = true
  class Class 0780, addr 00:1f.0, pci id 1af4:1003 (sub 1af4:0003)
  bar 0: i/o at 0xc080 [0xc09f]
  bar 1: mem at 0xfebd3000 [0xfebd3fff]
  bus: virtio-bus
  type virtio-pci-bus
  dev: virtio-serial-device, id ""
  max_ports = 31 (0x1f)
  indirect_desc = true
  event_idx = true
  notify_on_empty = true
  any_layout = false
  __com.redhat_rhel6_ctrl_guest_workaround = false
  bus: virtio-serial0.0
  type virtio-serial-bus
  dev: virtserialport, id "channel0"
  chardev = "charchannel0"
  nr = 1 (0x1)
  name = "org.qemu.guest_agent.0"
  port 1, guest off, host on, throttle off
  dev: piix3-usb-uhci, id "usb"
  bandwidth = 1280 (0x500)
  maxframes = 128 (0x80)
  addr = 01.2
  romfile = ""
  rombar = 1 (0x1)
  multifunction = false
  command_serr_enable = true
  class USB controller, addr 00:01.2, pci id 8086:7020 (sub 1af4:1100)
  bar 4: i/o at 0xc040 [0xc05f]
  bus: usb.0
  type usb-bus
  dev: usb-tablet, id "input0"
  usb_version = 2 (0x2)
  display = ""
  head = 0 (0x0)
  port = ""
  serial = ""
  full-path = true
  msos-desc = true
  addr 0.0, port 1, speed 12, name QEMU USB Tablet, attached
  dev: PIIX4_PM, id ""
  smb_io_base = 1792 (0x700)
  disable_s3 = 1 (0x1)
  disable_s4 = 1 (0x1)
  s4_val = 2 (0x2)
  acpi-pci-hotplug-with-bridge-support = false
  memory-hotplug-support = false
  addr = 01.3
  romfile = ""
  rombar = 1 (0x1)
  multifunction = false
  command_serr_enable = true
  class Bridge, addr 00:01.3, pci id 8086:7113 (sub 1af4:1100)
  bus: i2c
  type i2c-bus
  dev: smbus-eeprom, id ""
  address = 87 (0x57)
  dev: smbus-eeprom, id ""
  address = 86 (0x56)
  dev: smbus-eeprom, id ""
  address = 85 (0x55)
  dev: smbus-eeprom, id ""
  address = 84 (0x54)
  dev: smbus-eeprom, id ""
  address = 83 (0x53)
  dev: smbus-eeprom, id ""
  address = 82 (0x52)
  dev: smbus-eeprom, id ""
  address = 81 (0x51)
  dev: smbus-eeprom, id ""
  address = 80 (0x50)
  dev: piix3-ide, id ""
  addr = 01.1
  romfile = ""
  rombar = 1 (0x1)
  multifunction = false
  command_serr_enable = true
  class IDE controller, addr 00:01.1, pci id 8086:7010 (sub 1af4:1100)
  bar 4: i/o at 0xc0a0 [0xc0af]
  bus: ide.1
  type IDE
  bus: ide.0
  type IDE
  dev: ide-cd, id "ide0-0-1"
  drive = "drive-ide0-0-1"
  logical_block_size = 512 (0x200)
  physical_block_size = 512 (0x200)
  min_io_size = 0 (0x0)
  opt_io_size = 0 (0x0)
  discard_granularity = 512 (0x200)
  ver = "2.3.0"
  wwn = 0 (0x0)
  serial = "QM00002"
  model = ""
  unit = 1 (0x1)
  dev: ide-cd, id "ide0-0-0"
  drive = "drive-ide0-0-0"
  logical_block_size = 512 (0x200)
  physical_block_size = 512 (0x200)
  min_io_size = 0 (0x0)
  opt_io_size = 0 (0x0)
  discard_granularity = 512 (0x200)
  ver = "2.3.0"
  wwn = 0 (0x0)
  serial = "QM00001"
  model = ""
  unit = 0 (0x0)
  dev: PIIX3, id ""
  addr = 01.0
  romfile = ""
  rombar = 1 (0x1)
  multifunction = true
  command_serr_enable = true
  class ISA bridge, addr 00:01.0, pci id 8086:7000 (sub 1af4:1100)
  bus: isa.0
  type ISA
  dev: isa-fdc, id ""
  iobase = 1008 (0x3f0)
  irq = 6 (0x6)
  dma = 2 (0x2)
  driveA = ""
  driveB = ""
  check_media_rate = true
  isa irq 6
  dev: port92, id ""
  dev: vmmouse, id ""
  dev: vmport, id ""
  dev: i8042, id ""
  isa irqs 1,12
  dev: isa-pcspk, id ""
  iobase = 97 (0x61)
  dev: kvm-pit, id ""
  gpio-in "" 1
  iobase = 64 (0x40)
  lost_tick_policy = "discard"
  dev: mc146818rtc, id ""
  base_year = 0 (0x0)
  lost_tick_policy = "slew"
  isa irq 8
  dev: kvm-i8259, id ""
  iobase = 160 (0xa0)
  elcr_addr = 1233 (0x4d1)
  elcr_mask = 222 (0xde)
  master = false
  dev: kvm-i8259, id ""
  iobase = 32 (0x20)
  elcr_addr = 1232 (0x4d0)
  elcr_mask = 248 (0xf8)
  master = true
  dev: i440FX, id ""
  addr = 00.0
  romfile = ""
  rombar = 1 (0x1)
  multifunction = false
  command_serr_enable = true
  class Host bridge, addr 00:00.0, pci id 8086:1237 (sub 1af4:1100)
  dev: fw_cfg_io, id ""
  iobase = 1296 (0x510)
  dev: kvmclock, id ""
  dev: kvmvapic, id ""
  dev: icc-bridge, id ""
  mmio 00000000fee00000/0000000000100000
  bus: icc
  type icc-bus
  dev: kvm-apic, id ""
  id = 3 (0x3)
  version = 20 (0x14)
  vapic = true
  dev: kvm64-x86_64-cpu, id ""
  pmu = false
  hv-spinlocks = 8191 (0x1fff)
  hv-relaxed = true
  hv-vapic = true
  hv-time = true
  check = false
  enforce = false
  kvm = true
  dev: kvm-apic, id ""
  id = 2 (0x2)
  version = 20 (0x14)
  vapic = true
  dev: kvm64-x86_64-cpu, id ""
  pmu = false
  hv-spinlocks = 8191 (0x1fff)
  hv-relaxed = true
  hv-vapic = true
  hv-time = true
  check = false
  enforce = false
  kvm = true
  dev: kvm-apic, id ""
  id = 1 (0x1)
  version = 20 (0x14)
  vapic = true
  dev: kvm64-x86_64-cpu, id ""
  pmu = false
  hv-spinlocks = 8191 (0x1fff)
  hv-relaxed = true
  hv-vapic = true
  hv-time = true
  check = false
  enforce = false
  kvm = true
  dev: kvm-apic, id ""
  id = 0 (0x0)
  version = 20 (0x14)
  vapic = true
  dev: kvm64-x86_64-cpu, id ""
  pmu = false
  hv-spinlocks = 8191 (0x1fff)
  hv-relaxed = true
  hv-vapic = true
  hv-time = true
  check = false
  enforce = false
  kvm = true
