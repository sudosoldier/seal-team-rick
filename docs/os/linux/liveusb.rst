Rescue
~~~~~~

.. warning:: It is assumed the customer you are connecting to is using a Windows workstation

- WebEx with the customer
- Go to https://getfedora.org/en/workstation/
- Download the application (usb writer)
- Install, run, follow the wizard
- Boot to the USB on the node (F11 to get a boot menu)
- Choose to "Try" Fedora Workstation DONT INSTALL


Open a terminal and setup ssh:

.. code-block:: console

  $ passwd
  (enter some password)
  $ sudo systemctl start sshd

Now ssh from one of the other nodes or from the customer's workstation using PuTTy.
