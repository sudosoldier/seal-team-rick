Misc
~~~~

.. code-block:: console

  # rsync -aAXv --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found"} / /path/to/backup/folder
  # sdparm -v -t sas -c RLM

xargs
~~~~~

.. code-block:: console

  # sc vm show | grep -i vsrv | cut -d'-' -f1 | xargs -I {} sh -c "sc vmreplication show | grep {}"

find
~~~~

.. code-block:: console

   # find . -name 'blahblah' -exec something \;



MAGICSYSRQ
~~~~~~~~~~



disown
~~~~~~

forgot to run command in a screen

.. code-block:: console

  Ctrl + Z
  # jobs
  # disown -h
  # bg
  # jobs
  <disconnect>

  Login with another shell and check that the process still exists

run script as other user
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

  # su -c 'COMMAND' - USER


follow symlinks
~~~~~~~~~~~~~~~

https://serverfault.com/questions/115856/how-to-list-symbolic-link-chains

.. code-block:: console

  # namei <target>
  # readlink -e <target>
