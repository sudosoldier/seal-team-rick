Kernel Networking
~~~~~~~~~~~~~~~~~~
https://blog.stackpath.com/glossary/cwnd-and-rwnd/

Mailing lists
--------------
https://marc.info/?l=linux-netdev


Testing
----------
https://doc.dpdk.org/dts/gsg/intro.html
https://doc.dpdk.org/dts/test_plans/

GRO/GSO/LRO
---------------
https://www.kernel.org/doc/html/latest/networking/index.html
https://doc.dpdk.org/guides/prog_guide/generic_receive_offload_lib.html
https://stackoverflow.com/questions/47332232/why-is-gro-more-efficient
https://events.static.linuxfound.org/images/stories/slides/jls09/jls09_xu.pdf


https://wiki.linuxfoundation.org/networking/gso

Softnet stats
-------------
From the 3.10 source:

.. code-block:: C

    static int softnet_seq_show(struct seq_file *seq, void *v)
    {
    	struct softnet_data *sd = v;

    	seq_printf(seq, "%08x %08x %08x %08x %08x %08x %08x %08x %08x %08x\n",
    		   sd->processed, sd->dropped, sd->time_squeeze, 0,
    		   0, 0, 0, 0, /* was fastroute */
    		   sd->cpu_collision, sd->received_rps);
    	return 0;
    }
