Logging
~~~~~~~

/var/log/libvirt/libvirtd.log
/var/log/libvirt/qemu/$(VM_UUID).log


Audit Logs
-----------

.. code-block:: console

  # grep -rnw /var/log/audit -e a4daa05a-2ebd-4e4e-b7ae-ee0c1c96a82a | ausearch -i | grep 08/23/2018
