hey.. yeah.. I’ll take a look real quick in case something pops out at me.
looks like there’s still some VSDs unrecovered and no recoveries currently running
and
```20  d117fc10-fb78-4bdd-896e-354307ac4ed7  192.168.0.17:11001  /dev/REDACTED/slot4data  VALID          1.17TB  85.2%  ONLINE  sas   1     IN     DIRTY      3   DISCONNECTED```
is that the same node/drive?
gosh that usb mouse is totally spamming logs
no purpose to a mouse on a server..

chris [09:56]
I'll have him pull it
and I'll double check in a few, sorry on the line atm
checking now

bchrisman [10:03]
other stuff attaching as well
```[   12.849559] scsi 7:0:0:0: Direct-Access     Kingston DataTraveler 3.0 PMAP PQ: 0 ANSI: 6
[   12.865709] sd 7:0:0:0: Attached scsi generic sg8 type 0
[   12.866276] sd 7:0:0:0: [sdi] 242417664 512-byte logical blocks: (124 GB/115 GiB)
[   12.867142] sd 7:0:0:0: [sdi] Write Protect is off
[   12.867148] sd 7:0:0:0: [sdi] Mode Sense: 45 00 00 00
[   12.867895] sd 7:0:0:0: [sdi] Write cache: disabled, read cache: enabled, doesn't support DPO or FUA
[   13.334911]  sdi: sdi1 sdi2
[   13.348026] sd 7:0:0:0: [sdi] Attached SCSI removable disk```
we ignore that
md booted with 8/8 drives..
will look at REDACTEDd logs to see if I can figure out why that rsd is disconnected
this is rsd 20
```10.20.0.17  35000c500b7d96f7b  4   sas   WFK0MTA1  IN   IN   20   yes      0                    0```
which node/drive was last replaced?
looks a little fishy
/proc/scsi/scsi suggests this drive is a different model than the others…. a replacement?
```Host: scsi0 Channel: 00 Id: 04 Lun: 00
  Vendor: SEAGATE  Model: ST1200MM0099     Rev: ST31
  Type:   Direct-Access                    ANSI  SCSI revision: 06```
```2019-02-12 20:55:01.593852 WARN[16498-16625-rsd-cm-syn] RSDLocal.cpp:579                   |verifyHeaders() exception ERSDBadMagic thrown (Invalid RSD Magic Number: /dev/REDACTED/slot4data)
2019-02-12 20:55:01.634525 ERR [16498-16625-rsd-cm-syn] RSDLocal.cpp:829                   |verify() RSDLocal::verify failedInvalid RSD Magic Number: /dev/REDACTED/slot4data```

chris [10:11]
you are on the correct node, .17

bchrisman [10:12]
.17

chris [10:12]
Untitled
Drive Replacement:

Drive: S3L0E1FG
was not readable on the bus, but was formerly known as `/dev/sde` which isnt very helpful

bchrisman [10:13]
ok.. so that was recently replaced?

chris [10:13]
yup

bchrisman [10:13]
the data partition is null

chris [10:13]
as of about a day ago

bchrisman [10:14]
```[root@REDACTED-192-168-0-17 ~ 09:14:34 ]# dd if=/dev/REDACTED/slot4data bs=8M count=100 | sum
100+0 records in
100+0 records out
00000 819200
838860800 bytes (839 MB) copied, 6.78215 s, 124 MB/s
[root@REDACTED-192-168-0-17 ~ 09:14:48 ]#```
ok.. so it was never in REDACTEDd.. REDACTEDd expected it to have RSD 20 on it.
rsd 20 must not have successfully evac’ed.
setting that drive to out
less /var/log/REDACTED/REDACTEDd.log /starting up shows that ERSDBadMagic
REDACTED expects a magic sequence to be at a fixed location on the data partition so it knows what’s there is “supposed to be an RSD”.
scrsdinfo /dev/REDACTED/slot4data confirms it’s not an RSD.

chris [10:18]
:+1:
tyvm sir

bchrisman [10:26]
four VSDs still point to RSD ID 20
```[root@REDACTED-192-168-0-17 ~ 09:26:01 ]# sc vsd show display rsdlist | grep -P '20(,|$)'
8dbba6df-7190-489d-a081-73a5a069f1a2  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
c3fa1620-8ad7-48ee-b21b-24a28eb0a04f  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
b8b8402b-55b8-46ee-8bfe-e06d8fa45aef  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
bf49d95a-c6da-4d73-81ab-25679ab7cf1c  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
[root@REDACTED-192-168-0-17 ~ 09:26:20 ]#```
sc vsd show display tasks <--- shows no recovery tasks
pausing replication
getting rid of slot4/sde on .17 for now.. the data partition is blank, and REDACTEDd seemed to be choking on it
```2019-02-13 15:29:29.164943 WARN[16498-16608]            CachedValue.h:64                   | {7df79cca-1673777} check() Failed to update frozen extent cache for RSD 20: RSD Task Aborted: No Connection```

bchrisman [10:38]
force-removing that rsd
recoveries are running
when complete, we’ll need to scan that drive back in
after conditions are all clear (except rsd space), can scan that drive back in
```echo '- - -'  > '/sys/devices/pci0000:00/0000:00:02.2/0000:03:00.0/host0/scsi_host/host0/scan'```
will probably just need to set it back to ‘in’ after that.
