Performance Tuning
==================

Recommendations
~~~~~~~~~~~~~~~
- update virtio drivers for Windows guests
- enable GRO features
- tune network queues
- set irqbalance to oneshot or manually pin REDACTED and network processes to cpus
- test specific nic driver tuning -> tg3, ixgbe, i40e
- change tuned profile to throughput-performance ?
- ensure we are using jemalloc as opposed to tcmalloc (for all flash only)
- tune sysctl
- ensure guest partition alignment
- consider disk read ahead
- tune libvirt templates for particular windows versions
- tune qemu settings
- add ECN to better monitor congestion during testing
- do we need the backplane to be subject to iptables lookups?
- increase MTU?

References
~~~~~~~~~~
- https://blog.packagecloud.io/eng/2016/06/22/monitoring-tuning-linux-networking-stack-receiving-data/#softirqs
- https://wiki.mikejung.biz/KVM_/_Xen
- https://engineering.linkedin.com/performance/optimizing-linux-memory-management-low-latency-high-throughput-databases
- http://tracker.ceph.com/projects/ceph/wiki/Tuning_for_All_Flash_Deployments
- https://wiki.mikejung.biz/KVM_/_Xen#KVM_Forum_2014_Slides_and_Videos
- https://github.com/virtio-win/kvm-guest-drivers-windows/wiki/netkvm-RSC-(receive-segment-coalescing)-feature
- https://blog.packagecloud.io/eng/2016/06/22/monitoring-tuning-linux-networking-stack-receiving-data
- https://events.linuxfoundation.org/events/open-source-summit-north-america-2018/program/slides/


Notes
~~~~~
- virtio-blk-pci on host, virtio-scsi in guest
- virtio-scsi in guest shows like...2006 date
- https://ovirt.org/develop/release-management/features/storage/virtio-scsi/ ??
- https://wiki.libvirt.org/page/Virtio
- https://bugzilla.redhat.com/show_bug.cgi?id=1610461




https://medium.com/@kkirsche/using-logstash-to-import-csv-files-into-elasticsearch-d9617fa03e35
