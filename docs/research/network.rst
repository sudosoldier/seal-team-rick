Testbed kzt15a
==============
** I noticed throughput was much faster with iptables rules flushed...is it doing a lookup on every interface? I.E. check rules on the lan0, lanBond, lan, then vnet interfaces?

Tools
~~~~~~
dropwatch
tcpdump
ethtool
iperf3
nuttcp
numastat
https://kukuruku.co/post/capturing-packets-in-linux-at-a-speed-of-millions-of-packets-per-second-without-using-third-party-libraries/
https://github.com/HewlettPackard/netperf
https://perf.wiki.kernel.org/index.php/Tutorial
http://man7.org/linux/man-pages/man1/perf-stat.1.html
http://oprofile.sourceforge.net/news/

References
~~~~~~~~~~~
https://www.monperrus.net/martin/performance+of+read-write+throughput+with+iscsi

https://unix.stackexchange.com/questions/144794/why-would-the-kernel-drop-packets

http://tracker.ceph.com/projects/ceph/wiki/Tuning_for_All_Flash_Deployments ***

https://www.researchgate.net/figure/Comparison-of-goodput-bit-s-up-and-normalized-throughput-down_fig3_220292372

https://www.juniper.net/documentation/en_US/junos/topics/concept/policer-mx-m120-m320-burstsize-determining.html

https://docs.citrix.com/en-us/netscaler/12/system/TCP_Congestion_Control_and_Optimization_General.html

https://engineering.linkedin.com/performance/optimizing-linux-memory-management-low-latency-high-throughput-databases

https://www.coverfire.com/articles/queueing-in-the-linux-network-stack/

https://severalnines.com/blog/optimizing-your-linux-environment-mongodb

https://support.cumulusnetworks.com/hc/en-us/articles/216509388-Throughput-Testing-and-Troubleshooting

https://docs.cumulusnetworks.com/display/DOCS/Buffer+and+Queue+Management

https://cumulusnetworks.com/blog/explicit-congestion-notification/

https://networkengineering.stackexchange.com/questions/28691/learning-of-mac-addresses-with-cut-through-switching

https://docs.cumulusnetworks.com/display/DOCS/Monitoring+and+Troubleshooting

https://www.cisco.com/c/en/us/products/collateral/switches/nexus-5020-switch/white_paper_c11-465436.html

https://forums.whirlpool.net.au/archive/1962488

https://docs.cumulusnetworks.com/display/DOCS/Monitoring+Interfaces+and+Transceivers+Using+ethtool

https://coreos.com/blog/intro-to-systemd-networkd.html

https://www.freedesktop.org/software/systemd/man/systemd.netdev.html

https://wiki.archlinux.org/index.php/systemd-networkd

http://man7.org/linux/man-pages/man5/systemd.network.5.html

http://ipset.netfilter.org/iptables-extensions.man.html

https://docs.cumulusnetworks.com/display/DOCS/Monitoring+Virtual+Device+Counters

https://www.linuxjournal.com/content/queueing-linux-network-stack

https://fasterdata.es.net/host-tuning/ **********

https://wiki.xen.org/wiki/Network_Throughput_and_Performance_Guide

https://blog.stackpath.com/glossary/cwnd-and-rwnd/

http://www.tldp.org/HOWTO/Adv-Routing-HOWTO/
^vvv old
http://linux-ip.net/articles/Traffic-Control-HOWTO/intro.html

http://linux-ip.net/articles/Traffic-Control-HOWTO/index.html

https://fasterdata.es.net/host-tuning/100g-tuning/interrupt-binding/ ***

https://github.com/hjr3/softnet-stat

https://blog.packagecloud.io/eng/2016/06/22/monitoring-tuning-linux-networking-stack-receiving-data/ ***

https://www.lmax.com/blog/staff-blogs/2016/05/06/navigating-linux-kernel-network-stack-receive-path/

Hardware Specs
~~~~~~~~~~~~~~

Test Results
~~~~~~~~~~~~~~


.. code-block:: console

  [root@REDACTED-192-168-7-15 ~ 14:59:28 ]# iperf3 -c 192.168.7.14 -d -V
  iperf 3.1.7
  Linux REDACTED-192-168-7-15 3.10.0-327.62.4.el7.scel.sc00.x86_64 #1 SMP Thu Jan 4 12:53:15 PST 2018 x86_64
  Control connection MSS 1448
  send_parameters:
  {
          "tcp":  true,
          "omit": 0,
          "time": 10,
          "parallel":     1,
          "len":  131072,
          "client_version":       "3.1.7"
  }
  Time: Wed, 26 Sep 2018 14:59:30 GMT
  Connecting to host 192.168.7.14, port 5201
        Cookie: REDACTED-192-168-7-15.1537973970.413049
        TCP MSS: 1448 (default)
  SNDBUF is 16384, expecting 0
  RCVBUF is 87380, expecting 0
  Congestion algorithm is cubic
  [  4] local 192.168.7.15 port 47844 connected to 192.168.7.14 port 5201
  Starting Test: protocol: TCP, 1 streams, 131072 byte blocks, omitting 0 seconds, 10 second test
  tcpi_snd_cwnd 511 tcpi_snd_mss 1448 tcpi_rtt 599
  [ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
  [  4]   0.00-1.00   sec   344 MBytes  2.88 Gbits/sec  759    723 KBytes
  tcpi_snd_cwnd 375 tcpi_snd_mss 1448 tcpi_rtt 689
  [  4]   1.00-2.00   sec   486 MBytes  4.08 Gbits/sec  850    530 KBytes
  tcpi_snd_cwnd 608 tcpi_snd_mss 1448 tcpi_rtt 459
  [  4]   2.00-3.00   sec   811 MBytes  6.81 Gbits/sec   22    860 KBytes
  tcpi_snd_cwnd 628 tcpi_snd_mss 1448 tcpi_rtt 481
  [  4]   3.00-4.00   sec   832 MBytes  6.98 Gbits/sec    0    888 KBytes
  tcpi_snd_cwnd 640 tcpi_snd_mss 1448 tcpi_rtt 603
  [  4]   4.00-5.00   sec   834 MBytes  6.99 Gbits/sec    0    905 KBytes
  tcpi_snd_cwnd 650 tcpi_snd_mss 1448 tcpi_rtt 522
  [  4]   5.00-6.00   sec   832 MBytes  6.98 Gbits/sec    0    919 KBytes
  tcpi_snd_cwnd 656 tcpi_snd_mss 1448 tcpi_rtt 556
  [  4]   6.00-7.00   sec   822 MBytes  6.90 Gbits/sec    0    928 KBytes
  tcpi_snd_cwnd 659 tcpi_snd_mss 1448 tcpi_rtt 467
  [  4]   7.00-8.00   sec   818 MBytes  6.86 Gbits/sec    0    932 KBytes
  tcpi_snd_cwnd 661 tcpi_snd_mss 1448 tcpi_rtt 436
  [  4]   8.00-9.00   sec   814 MBytes  6.83 Gbits/sec    0    935 KBytes
  tcpi_snd_cwnd 664 tcpi_snd_mss 1448 tcpi_rtt 528
  send_results
  {
          "cpu_util_total":       8.830530,
          "cpu_util_user":        0.236281,
          "cpu_util_system":      8.687376,
          "sender_has_retransmits":       1,
          "congestion_used":      "cubic",
          "streams":      [{
                          "id":   1,
                          "bytes":        7772583800,
                          "retransmits":  1631,
                          "jitter":       0,
                          "errors":       0,
                          "packets":      0
                  }]
  }
  get_results
  {
          "cpu_util_total":       61.559345,
          "cpu_util_user":        0.654305,
          "cpu_util_system":      60.980149,
          "sender_has_retransmits":       -1,
          "congestion_used":      "cubic",
          "streams":      [{
                          "id":   1,
                          "bytes":        7765702504,
                          "retransmits":  -1,
                          "jitter":       0,
                          "errors":       0,
                          "packets":      0
                  }]
  }
  [  4]   9.00-10.00  sec   819 MBytes  6.87 Gbits/sec    0    939 KBytes
  - - - - - - - - - - - - - - - - - - - - - - - - -
  Test Complete. Summary Results:
  [ ID] Interval           Transfer     Bandwidth       Retr
  [  4]   0.00-10.00  sec  7.24 GBytes  6.22 Gbits/sec  1631             sender
  [  4]   0.00-10.00  sec  7.23 GBytes  6.21 Gbits/sec                  receiver
  CPU Utilization: local/sender 8.8% (0.2%u/8.7%s), remote/receiver 61.6% (0.7%u/61.0%s)
  snd_tcp_congestion cubic
  rcv_tcp_congestion cubic

  iperf Done.
