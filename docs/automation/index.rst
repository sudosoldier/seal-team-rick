Automation
==============

.. toctree::
  :maxdepth: 1
  :caption: Infrastructure

  salt
  terraform
  elastic/index.rst
  ansible/index.rst
