Salt
====

https://docs.saltstack.com/en/latest/ref/index.html
https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.saltutil.html
https://docs.saltstack.com/en/latest/ref/cli/salt-run.html
https://docs.saltstack.com/en/latest/ref/cli/salt-api.html


Targeting
----------

.. code-block:: console
  
    # salt test.ping




Track down what happened on a previous job run 
----------------------------------------------

.. code-block:: console

    # salt-run
    # salt-run jobs.
    # salt-run jobs.list_jobs search_target=<target> | less
    # salt-run jobs.print_job <jid>

Grains
------

.. code-block:: console

    # salt <Targeting> grains.item

tr
---

.. code-block:: console

    # salt-key | grep <target> | tr -s '\n' ',' > targets.txt
    # for i in $(cat targets.txt); do echo $1; done

apply specific state instead of highstate
-----------------------------------------

.. code-block:: console

    # salt [-L/-G/-C] <targets> state.apply users

test runs
---------

.. code-block:: console

    # salt <Targeting> state.apply test=true

refresh cache without restart salt minions
------------------------------------------

this will need addtl research its not totally working

.. code-block:: console

    # saltutil.refresh_grains

user.info
---------

.. code-block:: console

    # salt <Targeting> user.info <user>