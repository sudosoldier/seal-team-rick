ELK
===

From the site_:

.. _site: https://www.elastic.co/elk-stack

  "ELK" is the acronym for three open source projects: Elasticsearch, Logstash, and Kibana. Elasticsearch is a search and analytics engine. Logstash is a server‑side data processing pipeline that ingests data from multiple sources simultaneously, transforms it, and then sends it to a "stash" like Elasticsearch. Kibana lets users visualize data with charts and graphs in Elasticsearch.


Beats are the guest agents that ship data to the ELK server.


Server Deployment
~~~~~~~~~~~~~~~~~

Create the VM with 8GB Memory and 4 vCPU threads. This may need to be increased if the environment is large, especially if monitoring network flows.

CentOS7 Minimal 1804 ISO:
http://mirror.trouble-free.net/centos/7.5.1804/isos/x86_64/CentOS-7-x86_64-Minimal-1804.iso

Disk Configuration
------------------
- Select "I will configure partitioning"
- Click "auto create partitions"
- Select the Home partition, click the minus to remove it
- Select the root partition and change the capacity to 100 GiB
- Click Update Settings
- Click done
- Accept changes



Change to root user ( I have not yet added handling for sudo users properly )

.. code-block:: console

  # yum install epel-release -y && yum install git ansible -y
  # git clone https://bitbucket.org/sudosoldier/REDACTED-monitoring.git
  # cd REDACTED-monitoring
  # ansible-playbook deploy.yml

Go get some coffee, this is going to take a minute.


Client Deployment
~~~~~~~~~~~~~~~~~
I am currently working on ansible automation to deploy the clients. I have a working prototype for Linux and will work on the Windows version once finished.

Linux
------

For RPM based machines:

Install Metricbeat:

.. code-block:: console

  $ curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-6.4.2-x86_64.rpm
  $ sudo rpm -vi metricbeat-6.4.2-x86_64.rpm

Configure Metricbeat:

Modify ``/etc/metricbeat/metricbeat.yml`` to set the connection information. You may paste the following block and overwrite the original config file contents:

.. important:: Replace the IP address with the IP of the ELK server VM

.. literalinclude:: metricbeat_linux.yml
  :language: yaml
  :emphasize-lines: 6,14


Enable the system module:

.. code-block:: console

  $ sudo metricbeat modules enable system

Overwrite the system module at ``/etc/metricbeat/modules.d/system.yml`` with the following configuration:

.. literalinclude:: metricbeat_system_linux.yml
  :language: yaml

Start Metricbeat:

.. code-block:: console

  $ sudo systemctl enable metricbeat
  $ sudo systemctl start metricbeat

  or

  $ sudo service metricbeat enable
  $ sudo service metricbeat start


Windows
-------

- Download: https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-6.4.2-windows-x86_64.zip
- Extract the contents of the zip file into C:\\Program Files.
- Rename the metricbeat-6.4.2-windows directory to Metricbeat.
- Open a PowerShell prompt as an Administrator (right-click the PowerShell icon and select Run As Administrator).
- From the PowerShell prompt, run the following commands to install Metricbeat as a Windows service.

.. code-block:: powershell

  PS > cd C:\Program Files\Metricbeat
  PS > C:\Program Files\Metricbeat> Set-ExecutionPolicy Bypass -Scope Process -Force; .\install-service-metricbeat.ps1

Modify ``C:\Program Files\Metricbeat\metricbeat.yml`` to set the connection information. You may paste the following block and overwrite the original config file contents:

.. important:: Replace the IP address with the IP of the ELK server VM

.. literalinclude:: metricbeat_windows.yml
  :language: yaml
  :emphasize-lines: 10,15

Uncomment the following from ``C:\Program Files\Metricbeat\modules.d\system.yml``

.. code-block:: yaml

  - module: system
    metricsets:
      - core
      - diskio

From the ``C:\Program Files\Metricbeat`` folder, run:

.. code-block:: powershell

  PS > C:\Program Files\Metricbeat> metricbeat.exe modules enable system
  PS > C:\Program Files\Metricbeat> metricbeat.exe modules enable windows

Start the Metricbeat service

.. code-block:: powershell

  PS > C:\Program Files\Metricbeat> Start-Service metricbeat

Troubleshooting
---------------

.. code-block:: console

  # metricbeat test config
  # metricbeat test output

Updating
~~~~~~~~
On the ELK Server run the following inside the ``REDACTED-monitoring`` directory:

.. code-block:: console

  # git pull
  # ansible-playbook deploy.yml


Recommended Kibana Settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~
Kibana is the web interface front-end of the ELK software stack. The following are recommended settings alterations which you can find under `Management -> Kibana -> Advanced Settings` Credit for these settings goes to the Elastiflow project.


- doc_table:highlight         -> false
- filters:pinnedByDefault     -> true
- state:storeInSessionStorage -> true

Also, when looking at a dashboard or data in the Discover tab, you may want to enable the auto complete feature. It will help you with the search bar queries and is especially useful if you are new to ELK.

To enable auto complete, click the ``Options`` link on the far right of the search bar and click the slider to enable the feature.


..
.. Docker
.. ------

Network Monitoring
~~~~~~~~~~~~~~~~~~

It supports Netflow v5/v9, sFlow and IPFIX flow types. Consult the documentation for your network equipment. As an example, there are instructions to enable sFlow for the Lenovo NE1032 on this site.

Usage
~~~~~~
You may access the web interface at http://ip_address_of_vm:5601

Username: elastic
Password: changeme


Go to Management > Kibana > Index Patterns and select metricbeat-* as the default index pattern by clicking the star icon.

Go to Dashboards > [Metricbeat] - System


Screenshot Examples
~~~~~~~~~~~~~~~~~~~

Network Monitoring
------------------

.. figure:: net_ingress_egress.png
   :scale: 50 %
   :alt: Ingress and egress interfaces.

   Ingress and egress interfaces.

.. figure:: net_geoip.png
  :scale: 50 %
  :alt: Geographic heat map of network traffic.

  Geographic heat map of network traffic.

.. figure:: net_traffic.png
  :scale: 50 %
  :alt: Visualization of clients and serer flows.

  Visualization of clients and serer flows.

.. figure:: net_client_server.png
  :scale: 50 %
  :alt: Breakdown of client and servers.

  Breakdown of client and servers.



Windows
-------

There is a dashboard for tracking Windows Services. To the right, the number of failed services will be recorded and can easily be identified via the Discover tab.

.. figure:: win_services.png
   :scale: 50 %
   :alt: Statistics for Windows logs.

   Statistics for Windows logs.

Performance
-----------

.. figure:: metrics_host.png
   :scale: 50 %
   :alt: Metrics from host node.

   Metrics from host node.

.. figure:: metrics_host_breakdown.png
   :scale: 50 %
   :alt: Breakdown of container metrics from host node.

   Breakdown of container metrics from host node.

.. Logs
.. ----


Machine Learning
----------------
Automatic anomaly detection. Basically, the code learns what normal patterns are and will alert when events occur outside the learned pattern. Alerting and forecasting is available as well.


.. figure:: ml_anomaly.png
   :scale: 50 %
   :alt: Logged events showing severity.

   Logged events showing severity.


.. figure:: ml_alert.png
  :scale: 50 %
  :alt: Email alerting based on anomalies.

  Email alerting based on anomalies.

.. figure:: ml_forecast.png
  :scale: 50 %
  :alt: Forecasting based on learned patterns.

  Forecasting based on learned patterns.

Thrift Performance
------------------

.. figure:: thrift.png
  :scale: 50 %
  :alt: Thrift performance.

  Thrift performance.
