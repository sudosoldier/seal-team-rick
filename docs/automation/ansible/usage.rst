
Installation
~~~~~~~~~~~~

Follow Ansible's installation_ documentation.


.. _installation: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-the-control-machine


OSX
---
.. warning:: You need to have python2 installed if it is not already present.

.. code-block:: console

  $ sudo easy_install pip # install pip if you don't have it already
  $ sudo pip install ansible

Download the playbooks repository
---------------------------------

Playbooks can be found on github_.

.. _github: https://github.com/christopherdraper/playbooks

To download the available playbooks

.. code-block:: console

  $ git clone https://github.com/christopherdraper/playbooks.git

.. warning:: All playbooks have sensitive paths and variables encrypted in a vault and require decryption before use. See Chris for the passcode. You may pass decryption passcode on the commandline with ``--ask-vault-pass`` or by persisting them via a static file per Ansible documentation. Refer to https://docs.ansible.com/ansible/latest/user_guide/vault.html#providing-vault-passwords for more information.

To configure a password file, create a file named ``.vault`` in the playbooks directory containing the password, and ensure the permissions of this file to ``0600`` as shown:

.. code-block:: console

  $ cd playbooks
  $ echo <PROVIDED PASSWORD> > .vault
  $ chmod 0600 .vault

.. note:: If you did not set the password file you need to add ``--vault-ask-pass`` to the the following examples.


Configure SSH
-------------


Add the following to your SSH configuration, replace the user with your username:

.. code-block:: console

  $ vi ~/.ssh/config

  Host ansible-bastion
    Hostname REDACTED.com
    IdentityFile ~/.ssh/id_rsa
    ServerAliveInterval 5
    IdentitiesOnly yes
    ForwardAgent yes
    User <USERNAME>

  Host ansible-entrynode
    ProxyJump ansible-bastion
    Hostname localhost
    ForwardAgent yes
    User root


.. warning:: The config requires your SSH agent to be running

Usage
~~~~~

The general pattern of using the playbooks:

- Clone the playbooks git repository.
- Run the playbook.
- Follow the prompts.

.. note:: Playbooks utilize your own SSH keys from your workstation to connect to nodes.

.. note:: You can add flags to ``ansible-playbook`` to see additional output, display changes made, or run in check mode (dry run for testing).

.. note:: To see what commands are being ran, run ``ansible-playbook`` with the ``-vv`` flag.

.. note:: To run in check mode, run ``ansible-playbook`` with the ``-C`` flag.


All playbooks must be run with ``--vault-ask-pass`` or have the passcode persisted_.

.. _persisted: https://docs.ansible.com/ansible/latest/user_guide/vault.html#providing-vault-passwords

.. code-block:: console

  $ ansible-playbook example-playbook.yml --vault-ask-pass

Examining playbooks
~~~~~~~~~~~~~~~~~~~

You can list tasks that the playbook is going to perform to get an overview of the work that will be performed:

.. code-block:: console

  $ ansible-playbook --list-tasks diagnostics.yml

  playbook: diagnostics.yml

    play #1 (localhost): localhost        TAGS: []
      tasks:
        Set target cluster        TAGS: []
        Gather facts from cluster TAGS: []
        Get cluster UUID  TAGS: []
        Get node UUID     TAGS: []
        Create temporary directory to dump archives       TAGS: []
        Check for pre-existing zookeeper dump     TAGS: []
        Dump zookeeper    TAGS: []
        Check for pre-existing logcabin dump      TAGS: []
        Dump logcabin     TAGS: []
        Check for pre-existing REDACTED logs dump    TAGS: []
        Dump REDACTED logs   TAGS: []
        Check for pre-existing system log dump    TAGS: []
        Dump System logs  TAGS: []
        Check for pre-existing journal dump       TAGS: []
        Dump current system journal       TAGS: []
        Check for pre-existing local_stor.db dump TAGS: []
        Dump local_stor.db        TAGS: []
        Dump udev info    TAGS: []
        Dump md array     TAGS: []
        Dump md devices   TAGS: []
        Dump devfs        TAGS: []
        Dump sc tool output       TAGS: []
        Dump kernel information   TAGS: []
        Dump historic RRD data    TAGS: []
        Dump megacli information  TAGS: []
        Dump PCI devices  TAGS: []
        Dump IPMI events  TAGS: []
        Dump BIOS (dmidecode)     TAGS: []
        Dump misc TAGS: []
        Dump registers    TAGS: []
        Create local directory to fetch diagnostic results into   TAGS: []
        Check for pre-existing diagnostic dump    TAGS: []
        Compress diagnostic results       TAGS: []
        Fetch diagnostic results  TAGS: []
        Cleanup temporary files   TAGS: []


If you would like, you can have ``ansible`` prompt for confirmation prior to running each task.

.. code-block:: console

  $ ansible-playbook example.yml --step

Output will prompt for each task that is ran:

.. code-block:: console

  Perform task: configure ssh (y/n/c):


Updating
~~~~~~~~

Simply run ``git pull`` from the playbook directory.

.. code-block:: console

  $ cd /path/to/playbooks
  $ git pull
