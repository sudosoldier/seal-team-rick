Prometheus
----------

.. code-block:: yaml

    ---
    - hosts:                                         localhost
    vars_prompt:
    - name:                                        tunnel
        prompt:                                      tunnel
        private:                                     no

    pre_tasks:
    - name:                                        init
        include:                                     "{{ playbook_dir }}/../pre_tasks/init.yml"

    - hosts:                                         ansible-nodes
    roles:
    - role:                                        undergreen.prometheus-node-exporter
        prometheus_node_exporter_version:            0.17.0
        prometheus_node_exporter_enabled_collectors:
        - conntrack
        - cpu
        - diskstats
        - entropy
        - filefd
        - filesystem
        - loadavg
        - mdadm
        - meminfo
        - netdev
        - netstat
        - stat
        - textfile
        - time
        - vmstat
        - buddyinfo
        - ksmd
        - logind
        - meminfo_numa
        - processes
        - qdisc
        - systemd
        - tcpstat
        - interrupts
        - ntp
        - sockstat
        - bonding
        - hwmon
        - bcache
        - arp
        - edac
        - uname
        prometheus_node_exporter_config_flags:
        'web.listen-address':                      '0.0.0.0:9100'
        'log.level':                               'info'
    tasks:
        # - name:                                      open prometheus exporter port
        #   register:                                  iptables_present
        #   command:                                   iptables -C INPUT -p tcp -m tcp --dport 9100 -m state --state NEW -j ACCEPT
        #   failed_when: false
        - name:                                      open prometheus exporter port
        # when:                                      iptables_present.rc == 1
        command:                                   iptables -I INPUT -p tcp -m tcp --dport 9100 -m state --state NEW -j ACCEPT

    - hosts:                                         monitoring
    roles:
    - role:                                        cloudalchemy.grafana
        grafana_security:
        admin_user:                                admin
        admin_password:                            REDACTED
    - role:                                        cloudalchemy.prometheus
        prometheus_targets:
        node:
        - targets:
            - localhost.localdomain:9100
            - 10.205.109.124:9100
            - 10.205.108.74:9182
            - 10.205.108.42:9182
            labels:
            env:                                   demosite
    - role:                                        undergreen.prometheus-node-exporter
        prometheus_node_exporter_version:            0.17.0
        prometheus_node_exporter_enabled_collectors:
        - conntrack
        - cpu
        - diskstats
        - entropy
        - filefd
        - filesystem
        - loadavg
        - mdadm
        - meminfo
        - netdev
        - netstat
        - stat
        - textfile
        - time
        - vmstat
        - buddyinfo
        - ksmd
        - logind
        - meminfo_numa
        - processes
        - qdisc
        - systemd
        - tcpstat
        - interrupts
        - ntp
        - sockstat
        - bonding
        - hwmon
        - bcache
        - arp
        - edac
        - uname
        prometheus_node_exporter_config_flags:
        'web.listen-address':                      '0.0.0.0:9100'
        'log.level':                               'info'

Build mainline kernel
---------------------

.. code-block:: yaml

    ---
    - hosts:             cl-cdraper
    tasks:
        - name:          create work dir for kernel build
        file:
            path:        /root/kernelbuild
            state:       directory

        - name:          download kernel source
        unarchive:
            src:         https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.4.tar.xz
            dest:        /root/kernelbuild
            remote_src:  yes
            creates:     /root/kernelbuild/linux-4.4
        - name:          ensure update server is resolvable
        lineinfile:
            path:        /etc/hosts
            insertafter: 'EOF'
            regexp:      '^10.100.45.153'
            line:        '10.100.45.153 irs.lab.local'
        - name:        open update tunnel
        shell:       /opt/REDACTED/bin/openupdatetunnel
        - name:        install development tools
        yum:
            name:      "@Development tools"
            enablerepo: '*'
            disablerepo: ["base", "REDACTED"]
        - name:        install development tools
        shell:       "yum -y groupinstall 'Development Tools' --enablerepo='*' --disablerepo=base --disablerepo=REDACTED && yum -y install openssl-devel --enablerepo='*' --disablerepo=base --disablerepo=REDACTED"
        - name:        close update tunnel
        shell:       /opt/REDACTED/bin/closeupdatetunnel
        - name:          clean build
        shell:         make clean && make mrproper
        args:
            chdir:       /root/kernelbuild/linux-4.4
            creates:     /root/kernelbuild/linux-4.4/.config
        - name:          build kernel
        shell:         make olddefconfig && make
        args:
            chdir:       /root/kernelbuild/linux-4.4
            creates:     /root/kernelbuild/linux-4.4/.config
        - name:          build kernel modules
        shell:         make modules_install
        args:
            chdir:       /root/kernelbuild/linux-4.4
            creates:     /usr/lib/modules/4.4.0
        - name:          copy kernel to boot directory
        shell:         cp -v arch/x86_64/boot/bzImage /boot/vmlinuz-4.4.0
        args:
            chdir:       /root/kernelbuild/linux-4.4
            creates:     /boot/vmlinuz-linux4.4
        - name:          gerneate initramfs
        shell:         mkinitrd -v /boot/initramfs-4.4.0.img 4.4.0
        args:
            creates:     /boot/initramfs-4.4.0.img
        - name:          build grub config
        shell:         /usr/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg


Test settings for gcloud role
-----------------------------

.. code-block:: yaml

    ---
    - hosts:                   localhost
    pre_tasks:
        - include:             "{{ playbook_dir }}/pre_tasks/init.yml"
        tags: always
    roles:
    - role:                  gcloud
    vars:
        tunnel:                9124
        lightbound:            yes
        teardown:              yes
        preemptible:           yes
        cloud_ip:              10.205.108.124
        customer_name:         cdraper-central
        sf_cluster_id:         cdraper-central
        cloud_gateway_vm_name: Cloud-Gateway-VM
        region:                us-central1
        zone:                  us-central1-b
        cloud_disk_size:       375GB
