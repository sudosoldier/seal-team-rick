Playbooks
~~~~~~~~~


Diagnostic Report
-----------------
The following information will be gathered:

- ansible_facts_
- MegaRAID
- udev
- processes
- zookeeper
- logcabin
- system journal
- system logs
- md devices
- IPMI
- BIOS
- sc tool output

.. _ansible_facts: https://docs.ansible.com/ansible/2.7/user_guide/playbooks_variables.html#information-discovered-from-systems-facts

.. code-block:: console

  $ ansible-playbook diagnostics.yml
  username: cdraper
  tunnel number: 9124
  confirm tunnel number: 9124
  salesforce case number: 12345

The resulting diagnostic will appear in a folder named ``results`` relative to where the playbook exists. Diagnostics are packaged in a gzipped tarball and designated by Salesforce case #.



Onboarding new employees
------------------------
The onboarding playbook will provision SSH keys and support customizations for new employees as we onboard them. By automating this process we can guarentee keys are up to date with security best practice, such as ensuring keys are 4096 bit.


Open your terminal and navigate to the directory the provided USB device is mounted to.

.. code-block:: console

  $ cd /path/to/usb
  $ ansible-playbook onboarding.yml
  username: cdraper
  create password for your ssh private key: *****
  confirm password for your ssh private key: *****
  console redirect port: 57600

Your bash configuration and keys will be automatically provisioned. After having your public key added to the list of allowed keys, test connectivity by typing ``RS`` in your terminal.

Scheduling updates
------------------

Choose a time to run the update. For reference:
https://www.computerhope.com/unix/uat.htm

Run the playbook and follow the prompts:


.. code-block:: console

  $ ansible-playbook update.yml
  username: cdraper
  tunnel number: 9124
  confirm tunnel number: 9124
  enter build to update to: 186391
  confirm build to update to: 186391
  time to run update: 4:05 pm

Example output:

.. code-block:: console

  ➜  playbooks git:(master) ✗ ansible-playbook update.yml
  Executing playbook update.yml
  REDACTED username: cdraper
  tunnel number: 9124
  confirm tunnel number: 9124
  build to update to: 187326
  time to run update: 3:58PM

  - localhost -
  set target node...
    localhost done
  open update tunnel...
    localhost -> localhost done
  preload firmware...
    localhost -> localhost done | stdout: Preparing to update to build ID 187326...
  Obtaining cluster IPs...
  Preparing update on 1 nodes...
  Prepare complete.
  Next steps:
  'scupdate detail 187326' will show details of the update.
  'scupdate update 187326' will RUN the update.
  generate scheduled update...
    localhost -> localhost ok
  schedule update...
    localhost -> localhost done | stderr: job 11 at Wed Nov  7 15:58:00 2018
  register scheduled update status...
    localhost -> localhost ok | stdout: 11        Wed Nov  7 15:58:00 2018 a root
  display update status...
    localhost ok: {
      "changed": false,
      "update_status.stdout_lines": [
          "11\tWed Nov  7 15:58:00 2018 a root"
      ]
  }

  - Play recap -
    localhost                  : ok=7    changed=4    unreachable=0    failed=0

SNMP configuration
------------------
A readonly SNMPv2c community with the name ``public`` will be provisioned.

.. code-block:: console

  $ ansible-playbook snmpd.yml
  username: cdraper
  tunnel number: 9124
  confirm tunnel number: 9124

Google cloud provisioning
-------------------------

.. warning:: This section is under construction and is incomplete.

.. warning:: This playbook is currently in alpha. There is not much in the way of error checking yet, if any of the stages fail you will need to connect and perform them manually.

Download and install the SDK_

.. _SDK: https://cloud.google.com/sdk/

For OSX: https://cloud.google.com/sdk/docs/quickstart-macos

Login to gcloud to set your credentials:

.. code-block:: console

  $ gcloud init
  $ gcloud auth login
  $ gcloud auth application-default login
  $ gcloud config set project high-codex-147117

Run the playbook and follow the prompts:

.. code-block:: console

  $ ansible-playbook gcloud.yml --ask-vault-pass


Chassis failure
---------------
Dump the information for ordering a chassis replacement.

.. code-block:: console

  $ ansible-playbook chassis-failure.yml
  username: cdraper
  tunnel number: 9124
  confirm tunnel number: 9124
  salesforce case number: 12345
  suspected issue: bad ram
  troubleshooting description: I performed some steps and yeah the ram is borked.

Output is saved to ``results/sf_<case number>_chassis.yml``

Example output:

.. code-block:: yaml

  Recommended by:  cdraper

  Node:
  Peer  UUID                                  Backplane       LAN
  ====  ====================================  ==============  ==============
  1     47dad3af-ace6-40e6-b8ab-f400c4984149  172.16.109.124  10.205.109.124

  Serial Number: 39978X1

                total        used        free      shared  buff/cache   available
  Mem:          64124        1130       46631         358       16362       60833
  Swap:             0           0           0

  model name	: Intel(R) Xeon(R) CPU E5-2420 0 @ 1.90GHz

  Suspected Issue:  bad ram


  Troubleshooting: I performed some steps and yeah the ram is borked.

Disk failure
------------
Dump the information for ordering a disk replacement.

.. warning:: Under construction, this playbook is not yet functional

.. code-block:: console

  $ ansible-playbook disk-failure.yml
  username: cdraper
  tunnel number: 9124
  confirm tunnel number: 9124


Collect sar data
-----------------
Collect and graph ``sar`` data into PDFs for performance cases.

.. warning:: Under construction, this playbook is not yet functional

.. code-block:: console

  $ ansible-playbook sar.yml
  username: cdraper
  tunnel number: 9124
  confirm tunnel number: 9124


Capture packets
---------------
Collect a packet capture for analysis with Wireshark_.

.. _Wireshark: https://www.wireshark.org/

.. warning:: Under construction, this playbook is not yet functional

.. code-block:: console

  $ ansible-playbook pcap.yml
  username: cdraper
  tunnel number: 9124
  confirm tunnel number: 9124

Custom ZSH shell
----------------

*placeholder

https://github.com/viasite-ansible/ansible-role-zsh
