Templates
=========

Performance Escalation
~~~~~~~~~~~~~~~~~~~~~~
- is it constant? sporadic?
- if it is sporadic, for how long? Does it last for an hour? An afternoon? Just for a moment?
- Does it happen at a certain time of day? Time of week? Does that coincide with anything else?
- Can the customer make the performance issue happen (by running a job, a query, etc.)?
- How does the customer measure the problem? User complaints? Benchmark results? Stopwatch?
- What is the customer's baseline? What are they comparing performance to? Details of the previous setup. (edited)

With hangs/freezing in particular:

- How is this observed? RDP? UI Console? What's the data path from eyeball to desktop (e.g. laptop -> VPN -> ui console)?
- Is the mouse responsive during the hang? Can you open the start menu during the hang?


Performance Escalation 2
~~~~~~~~~~~~~~~~~~~~~~~~

PERFORANCE QUESTIONS:

  Are all VMs affected, groups of VMs that work together, random VMs, or a single VM
  Can you deREDACTED the issue in as much detail as possible
  Did you use a tool to determine it?
  What tool?
  What type of test does this tool run?
  What are the expected results?
  Was the performance concerns made aware by your users?
  What do they deREDACTED happening?
  What is the expected functionality?
  Are you comparing it to your previous environment?
  What was your previous environments hardware?
  How did it perform in that environments?
  Can any metrics be provided?
  When does it occur?
  Does it happen all the time, at certain intervals, randomly?
  What is the interval?
  Any noticeable patterns?
  If random, what is an estimate of recurrence?
  Do any jobs/tasks run during this time?
  Is this the busy time of the day?
  If all the time, no further time questions needed?
  When did the issue first present itself?
  Did this occur after a migration, installing software, performing an update or other notable event?
  For all affected VMs can you provide the operating system, roles, and how the server is being used?
  Example : Server 2012 R2, running Microsoft SQL 2005 SP3, and users connection to it using the application <application>
  If the system has more than one drive then what are the functions for each drive


Case Notes
~~~~~~~~~~

.. code-block:: console

  Problem
  =======

  Impact
  ======

  Background Info
  =============

  Troubleshooting
  =============

  Suspected Causes
  ===============

  Next Steps
  =========

Root Cause Analysis
~~~~~~~~~~~~~~~~~~~

.. code-block:: console

  Problem Statement
  ================

  Impact
  ======

  Summary
  ========

  Cause & Effect
  ============

  Solution
  =======

Case Summary
~~~~~~~~~~~~

.. code-block:: console

  Problem
  =======

  Impact
  ======

  Troubleshooting
  =============

  Suspected Causes
  ===============

  Error Codes
  =========

  Resolution
  ========

PMI Case Note
~~~~~~~~~~~~~

.. code-block:: console

  Event
  =====

  Description
  ===========

  Next Steps
  ==========


Chasis Replacement
~~~~~~~~~~~~~~~~~~

.. code-block:: console

  Thanks! You should get a tracking number later today when it ships. It should arrive tomorrow. Let me know when you're ready and I can shutdown the ``###`` node for you to replace it. 

  When you swap it out, you'll want to uncable it, remove it from the rack and remove all of the physical drives. It's critical that these drives go back into the same slot position in the new chassis (slot0 to slot0, slot1 to slot1, etc.). Once the new drives are in, rack the replacement node, cable it and power it on. I'll bring it back into the cluster after that.

  Let me know if you have any questions between now and when it arrives!
