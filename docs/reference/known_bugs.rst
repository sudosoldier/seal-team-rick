Known Bugs
==========
- https://fedorapeople.org/groups/virt/virtio-win/CHANGELOG
- https://wiki.qemu.org/ChangeLog/
- https://bugs.launchpad.net/qemu/
- https://bugzilla.redhat.com/show_bug.cgi?id=1610461
