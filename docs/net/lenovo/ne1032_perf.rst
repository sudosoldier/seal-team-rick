Lenovo NE1032 Performance Test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Results
-------
Honestly, these tests don't show anything except the performance of an individual switch port. Since I only had two 10GbE interfaces to test with, the maximum we can hope for is 10Gbps between the client and server. These tests could be useful for CoS tuning, but otherwise only confirms that yes, in fact, these are 10Gbps switch ports. The published switch specifications state the NE1032 is capable of 640 Gbps at a maximum, which seems subpar for its price point. I would guess the ASIC is rated to 640 Gbps and tested separately as this switch cannot reach those speeds with 32x 10GbE interfaces. Perhaps this metric was determined from a loopback test.


Switch Specs
_____________
- Up to 476 Million packets per second (Mpps) (64-byte packets)
- Buffer size: 12 MB
- As low as 760 ns port-to-port switching latency
- Up to 640 Gbps switching throughput
- Up to 9,216-byte jumbo frames

Node Specs
__________
- Server: Intel Corporation Ethernet 10G 2P X520 Adapter [8086:154d] (rev 01)
- Client: Intel Corporation Ethernet Controller X710 for 10GbE SFP+ [8086:1572] (rev 01)


iperf
-----
A single threaded test caps out at about ~3Gbps. To achieve 10Gbps speeds I had to run at least 4 threads. Additionally, the server NIC seemed to perform slightly better during the bidirectional test, which seems to suggest issues with the X710 card.


Single thread
______________


.. code-block:: console

    ------------------------------------------------------------
    Client connecting to 10.205.13.11, TCP port 5001
    TCP window size: 1.06 MByte (default)
    ------------------------------------------------------------
    [  3] local 10.205.13.8 port 60798 connected with 10.205.13.11 port 5001
    [ ID] Interval       Transfer     Bandwidth
    [  3]  0.0-10.0 sec  3.54 GBytes  3.04 Gbits/sec




Bidirectional
_____________

.. code-block:: console

    Client connecting to 10.205.13.11, TCP port 5001
    TCP window size: 1.06 MByte (default)
    ------------------------------------------------------------
    [  5] local 10.205.13.8 port 34450 connected with 10.205.13.11 port 5001
    [  4] local 10.205.13.8 port 5001 connected with 10.205.13.11 port 38094
    [ ID] Interval       Transfer     Bandwidth
    [  5]  0.0-10.0 sec  3.58 GBytes  3.07 Gbits/sec
    [  4]  0.0-10.0 sec  4.31 GBytes  3.70 Gbits/sec


2 threads
_________

.. code-block:: console

    ------------------------------------------------------------
    Client connecting to 10.205.13.11, TCP port 5001
    TCP window size: 1.06 MByte (default)
    ------------------------------------------------------------
    [  3] local 10.205.13.8 port 34878 connected with 10.205.13.11 port 5001
    [  4] local 10.205.13.8 port 34876 connected with 10.205.13.11 port 5001
    [ ID] Interval       Transfer     Bandwidth
    [  3]  0.0-10.0 sec  3.11 GBytes  2.67 Gbits/sec
    [  4]  0.0-10.0 sec  3.31 GBytes  2.85 Gbits/sec
    [SUM]  0.0-10.0 sec  6.42 GBytes  5.51 Gbits/sec


3 threads
_________

.. code-block:: console

    ------------------------------------------------------------
    Client connecting to 10.205.13.11, TCP port 5001
    TCP window size: 1.06 MByte (default)
    ------------------------------------------------------------
    [  5] local 10.205.13.8 port 35150 connected with 10.205.13.11 port 5001
    [  3] local 10.205.13.8 port 35146 connected with 10.205.13.11 port 5001
    [  4] local 10.205.13.8 port 35148 connected with 10.205.13.11 port 5001
    [ ID] Interval       Transfer     Bandwidth
    [  5]  0.0-10.0 sec  2.88 GBytes  2.48 Gbits/sec
    [  3]  0.0-10.0 sec  2.91 GBytes  2.50 Gbits/sec
    [  4]  0.0-10.0 sec  2.78 GBytes  2.38 Gbits/sec
    [SUM]  0.0-10.0 sec  8.57 GBytes  7.36 Gbits/sec


4 threads
_________

.. code-block:: console

    ------------------------------------------------------------
    Client connecting to 10.205.13.11, TCP port 5001
    TCP window size: 1.06 MByte (default)
    ------------------------------------------------------------
    [  6] local 10.205.13.8 port 35586 connected with 10.205.13.11 port 5001
    [  4] local 10.205.13.8 port 35580 connected with 10.205.13.11 port 5001
    [  5] local 10.205.13.8 port 35582 connected with 10.205.13.11 port 5001
    [  3] local 10.205.13.8 port 35584 connected with 10.205.13.11 port 5001
    [ ID] Interval       Transfer     Bandwidth
    [  6]  0.0-10.0 sec  2.55 GBytes  2.19 Gbits/sec
    [  4]  0.0-10.0 sec  2.49 GBytes  2.13 Gbits/sec
    [  5]  0.0-10.0 sec  2.35 GBytes  2.02 Gbits/sec
    [  3]  0.0-10.0 sec  2.37 GBytes  2.04 Gbits/sec
    [SUM]  0.0-10.0 sec  9.76 GBytes  8.38 Gbits/sec

5 threads
_________

.. code-block:: console

    ------------------------------------------------------------
    Client connecting to 10.205.13.11, TCP port 5001
    TCP window size: 1.06 MByte (default)
    ------------------------------------------------------------
    [  7] local 10.205.13.8 port 35842 connected with 10.205.13.11 port 5001
    [  4] local 10.205.13.8 port 35834 connected with 10.205.13.11 port 5001
    [  3] local 10.205.13.8 port 35836 connected with 10.205.13.11 port 5001
    [  5] local 10.205.13.8 port 35838 connected with 10.205.13.11 port 5001
    [  6] local 10.205.13.8 port 35840 connected with 10.205.13.11 port 5001
    [ ID] Interval       Transfer     Bandwidth
    [  4]  0.0-10.0 sec  1.75 GBytes  1.50 Gbits/sec
    [  6]  0.0-10.0 sec  2.31 GBytes  1.99 Gbits/sec
    [  7]  0.0-10.0 sec  1.74 GBytes  1.50 Gbits/sec
    [  3]  0.0-10.0 sec  2.34 GBytes  2.01 Gbits/sec
    [  5]  0.0-10.0 sec  2.31 GBytes  1.99 Gbits/sec
    [SUM]  0.0-10.0 sec  10.5 GBytes  8.98 Gbits/sec

6 threads
_________

.. code-block:: console

    ------------------------------------------------------------
    Client connecting to 10.205.13.11, TCP port 5001
    TCP window size: 1.06 MByte (default)
    ------------------------------------------------------------
    [  8] local 10.205.13.8 port 36022 connected with 10.205.13.11 port 5001
    [  3] local 10.205.13.8 port 36014 connected with 10.205.13.11 port 5001
    [  4] local 10.205.13.8 port 36012 connected with 10.205.13.11 port 5001
    [  5] local 10.205.13.8 port 36016 connected with 10.205.13.11 port 5001
    [  6] local 10.205.13.8 port 36018 connected with 10.205.13.11 port 5001
    [  7] local 10.205.13.8 port 36020 connected with 10.205.13.11 port 5001
    [ ID] Interval       Transfer     Bandwidth
    [  3]  0.0-10.0 sec  1.60 GBytes  1.38 Gbits/sec
    [  4]  0.0-10.0 sec  1.49 GBytes  1.28 Gbits/sec
    [  5]  0.0-10.0 sec  1.94 GBytes  1.66 Gbits/sec
    [  6]  0.0-10.0 sec  2.14 GBytes  1.84 Gbits/sec
    [  7]  0.0-10.0 sec  1.71 GBytes  1.47 Gbits/sec
    [  8]  0.0-10.0 sec  1.46 GBytes  1.25 Gbits/sec
    [SUM]  0.0-10.0 sec  10.3 GBytes  8.87 Gbits/sec

10 threads
__________

.. code-block:: console

    ------------------------------------------------------------
    Client connecting to 10.205.13.11, TCP port 5001
    TCP window size: 1.06 MByte (default)
    ------------------------------------------------------------
    [ 12] local 10.205.13.8 port 36142 connected with 10.205.13.11 port 5001
    [  6] local 10.205.13.8 port 36128 connected with 10.205.13.11 port 5001
    [  4] local 10.205.13.8 port 36124 connected with 10.205.13.11 port 5001
    [  3] local 10.205.13.8 port 36126 connected with 10.205.13.11 port 5001
    [  7] local 10.205.13.8 port 36130 connected with 10.205.13.11 port 5001
    [  5] local 10.205.13.8 port 36132 connected with 10.205.13.11 port 5001
    [  9] local 10.205.13.8 port 36134 connected with 10.205.13.11 port 5001
    [  8] local 10.205.13.8 port 36136 connected with 10.205.13.11 port 5001
    [ 10] local 10.205.13.8 port 36138 connected with 10.205.13.11 port 5001
    [ 11] local 10.205.13.8 port 36140 connected with 10.205.13.11 port 5001
    [ ID] Interval       Transfer     Bandwidth
    [ 12]  0.0-10.0 sec   812 MBytes   681 Mbits/sec
    [  6]  0.0-10.0 sec  1.34 GBytes  1.15 Gbits/sec
    [  7]  0.0-10.0 sec   900 MBytes   755 Mbits/sec
    [ 10]  0.0-10.0 sec  1.48 GBytes  1.27 Gbits/sec
    [ 11]  0.0-10.0 sec   780 MBytes   655 Mbits/sec
    [  4]  0.0-10.0 sec   885 MBytes   742 Mbits/sec
    [  8]  0.0-10.0 sec  1.46 GBytes  1.25 Gbits/sec
    [  3]  0.0-10.1 sec   877 MBytes   725 Mbits/sec
    [  5]  0.0-10.1 sec   829 MBytes   686 Mbits/sec
    [  9]  0.0-10.1 sec  1.54 GBytes  1.31 Gbits/sec
    [SUM]  0.0-10.1 sec  10.8 GBytes  9.13 Gbits/sec


20 threads
__________

.. code-block:: console

    ------------------------------------------------------------
    Client connecting to 10.205.13.11, TCP port 5001
    TCP window size: 1.06 MByte (default)
    ------------------------------------------------------------
    [ 22] local 10.205.13.8 port 36472 connected with 10.205.13.11 port 5001
    [  4] local 10.205.13.8 port 36434 connected with 10.205.13.11 port 5001
    [  3] local 10.205.13.8 port 36436 connected with 10.205.13.11 port 5001
    [  6] local 10.205.13.8 port 36438 connected with 10.205.13.11 port 5001
    [  5] local 10.205.13.8 port 36440 connected with 10.205.13.11 port 5001
    [  7] local 10.205.13.8 port 36442 connected with 10.205.13.11 port 5001
    [  8] local 10.205.13.8 port 36444 connected with 10.205.13.11 port 5001
    [  9] local 10.205.13.8 port 36446 connected with 10.205.13.11 port 5001
    [ 11] local 10.205.13.8 port 36448 connected with 10.205.13.11 port 5001
    [ 13] local 10.205.13.8 port 36452 connected with 10.205.13.11 port 5001
    [ 10] local 10.205.13.8 port 36450 connected with 10.205.13.11 port 5001
    [ 12] local 10.205.13.8 port 36454 connected with 10.205.13.11 port 5001
    [ 14] local 10.205.13.8 port 36456 connected with 10.205.13.11 port 5001
    [ 15] local 10.205.13.8 port 36458 connected with 10.205.13.11 port 5001
    [ 17] local 10.205.13.8 port 36460 connected with 10.205.13.11 port 5001
    [ 16] local 10.205.13.8 port 36462 connected with 10.205.13.11 port 5001
    [ 18] local 10.205.13.8 port 36464 connected with 10.205.13.11 port 5001
    [ 20] local 10.205.13.8 port 36466 connected with 10.205.13.11 port 5001
    [ 19] local 10.205.13.8 port 36468 connected with 10.205.13.11 port 5001
    [ 21] local 10.205.13.8 port 36470 connected with 10.205.13.11 port 5001
    [ ID] Interval       Transfer     Bandwidth
    [  5]  0.0-10.0 sec   304 MBytes   255 Mbits/sec
    [ 22]  0.0-10.0 sec   919 MBytes   771 Mbits/sec
    [  4]  0.0-10.0 sec   489 MBytes   410 Mbits/sec
    [  3]  0.0-10.0 sec  1.00 GBytes   861 Mbits/sec
    [  6]  0.0-10.0 sec  1.00 GBytes   860 Mbits/sec
    [  7]  0.0-10.0 sec   299 MBytes   251 Mbits/sec
    [  9]  0.0-10.0 sec   310 MBytes   260 Mbits/sec
    [ 11]  0.0-10.0 sec  1004 MBytes   841 Mbits/sec
    [ 13]  0.0-10.0 sec  1.03 GBytes   883 Mbits/sec
    [ 12]  0.0-10.0 sec   876 MBytes   734 Mbits/sec
    [ 18]  0.0-10.0 sec   316 MBytes   265 Mbits/sec
    [ 19]  0.0-10.0 sec   520 MBytes   436 Mbits/sec
    [  8]  0.0-10.0 sec   589 MBytes   493 Mbits/sec
    [ 10]  0.0-10.0 sec   442 MBytes   370 Mbits/sec
    [ 15]  0.0-10.0 sec   274 MBytes   230 Mbits/sec
    [ 20]  0.0-10.0 sec   276 MBytes   232 Mbits/sec
    [ 14]  0.0-10.0 sec   354 MBytes   296 Mbits/sec
    [ 17]  0.0-10.0 sec   488 MBytes   408 Mbits/sec
    [ 21]  0.0-10.0 sec   259 MBytes   217 Mbits/sec
    [ 16]  0.0-10.0 sec   230 MBytes   193 Mbits/sec
    [SUM]  0.0-10.0 sec  10.8 GBytes  9.24 Gbits/sec


100 threads
___________

.. code-block:: console

    [ ID] Interval       Transfer     Bandwidth
    [ 21]  0.0- 7.0 sec  79.1 MBytes  95.0 Mbits/sec
    [ 30]  0.0- 7.0 sec  58.6 MBytes  70.4 Mbits/sec
    [ 32]  0.0- 7.0 sec  57.0 MBytes  68.4 Mbits/sec
    [  5]  0.0- 7.0 sec  42.0 MBytes  50.3 Mbits/sec
    [ 19]  0.0- 7.0 sec  37.1 MBytes  44.5 Mbits/sec
    [ 35]  0.0- 7.0 sec  48.0 MBytes  57.6 Mbits/sec
    [ 36]  0.0- 7.0 sec  53.5 MBytes  64.1 Mbits/sec
    [ 39]  0.0- 7.0 sec  61.6 MBytes  73.9 Mbits/sec
    [ 40]  0.0- 7.0 sec   114 MBytes   137 Mbits/sec
    [  7]  0.0- 7.0 sec   111 MBytes   132 Mbits/sec
    [ 10]  0.0- 7.0 sec   175 MBytes   209 Mbits/sec
    [ 28]  0.0- 7.0 sec   138 MBytes   165 Mbits/sec
    [ 33]  0.0- 7.0 sec  75.2 MBytes  90.0 Mbits/sec
    [ 34]  0.0- 7.0 sec  40.0 MBytes  47.9 Mbits/sec
    [ 38]  0.0- 7.0 sec  36.5 MBytes  43.7 Mbits/sec
    [ 44]  0.0- 7.0 sec  39.2 MBytes  46.9 Mbits/sec
    [ 42]  0.0- 7.0 sec  74.5 MBytes  89.2 Mbits/sec
    [ 12]  0.0- 7.0 sec  35.1 MBytes  42.0 Mbits/sec
    [ 37]  0.0- 7.0 sec  97.6 MBytes   117 Mbits/sec
    [ 45]  0.0- 7.0 sec  91.6 MBytes   109 Mbits/sec
    [ 50]  0.0- 7.0 sec  59.5 MBytes  71.1 Mbits/sec
    [ 22]  0.0- 7.0 sec   143 MBytes   171 Mbits/sec
    [ 23]  0.0- 7.0 sec   101 MBytes   121 Mbits/sec
    [ 31]  0.0- 7.0 sec  63.5 MBytes  75.8 Mbits/sec
    [ 41]  0.0- 7.0 sec   146 MBytes   174 Mbits/sec
    [ 48]  0.0- 7.0 sec   117 MBytes   140 Mbits/sec
    [ 13]  0.0- 7.0 sec  76.6 MBytes  91.2 Mbits/sec
    [ 26]  0.0- 7.0 sec   108 MBytes   129 Mbits/sec
    [101]  0.0- 7.0 sec  30.1 MBytes  35.9 Mbits/sec
    [  4]  0.0- 7.1 sec  75.0 MBytes  89.2 Mbits/sec
    [  9]  0.0- 7.1 sec  76.5 MBytes  90.6 Mbits/sec
    [ 43]  0.0- 7.1 sec  51.4 MBytes  60.9 Mbits/sec
    [ 25]  0.0- 7.1 sec  54.9 MBytes  64.9 Mbits/sec
    [ 20]  0.0- 7.1 sec   105 MBytes   124 Mbits/sec
    [ 29]  0.0- 7.1 sec   138 MBytes   163 Mbits/sec
    [ 11]  0.0- 7.2 sec  80.4 MBytes  94.2 Mbits/sec
    [ 16]  0.0- 7.2 sec   123 MBytes   144 Mbits/sec
    [  8]  0.0- 7.2 sec   102 MBytes   120 Mbits/sec
    [ 17]  0.0- 7.2 sec  69.9 MBytes  81.7 Mbits/sec
    [ 24]  0.0- 7.2 sec  37.5 MBytes  43.8 Mbits/sec
    [ 47]  0.0- 7.2 sec  45.4 MBytes  53.0 Mbits/sec
    [ 14]  0.0- 7.2 sec   153 MBytes   179 Mbits/sec
    [ 15]  0.0- 7.2 sec  43.5 MBytes  50.8 Mbits/sec
    [ 27]  0.0- 7.2 sec  54.8 MBytes  64.0 Mbits/sec
    [  6]  0.0- 7.2 sec  41.0 MBytes  47.8 Mbits/sec
    [ 18]  0.0- 7.2 sec   204 MBytes   238 Mbits/sec
    [ 46]  0.0- 7.2 sec  36.0 MBytes  41.9 Mbits/sec
    [ 49]  0.0- 7.2 sec   116 MBytes   135 Mbits/sec
    [  3]  0.0- 7.2 sec  92.8 MBytes   108 Mbits/sec
    [102]  0.0- 7.4 sec  60.5 MBytes  68.7 Mbits/sec
    [ 53]  0.0- 8.0 sec  55.1 MBytes  57.9 Mbits/sec
    [ 64]  0.0- 8.0 sec   176 MBytes   184 Mbits/sec
    [ 65]  0.0- 8.0 sec  99.8 MBytes   105 Mbits/sec
    [ 59]  0.0- 8.0 sec  45.8 MBytes  48.0 Mbits/sec
    [ 51]  0.0- 8.0 sec   172 MBytes   180 Mbits/sec
    [ 60]  0.0- 8.0 sec  53.2 MBytes  55.8 Mbits/sec
    [ 68]  0.0- 8.0 sec   103 MBytes   108 Mbits/sec
    [ 58]  0.0- 8.0 sec  87.0 MBytes  91.2 Mbits/sec
    [ 62]  0.0- 8.0 sec  86.8 MBytes  90.9 Mbits/sec
    [ 57]  0.0- 8.0 sec  77.0 MBytes  80.7 Mbits/sec
    [ 77]  0.0- 8.0 sec  76.8 MBytes  80.4 Mbits/sec
    [ 86]  0.0- 8.0 sec  73.9 MBytes  77.4 Mbits/sec
    [ 54]  0.0- 8.0 sec   192 MBytes   202 Mbits/sec
    [ 52]  0.0- 8.0 sec  82.9 MBytes  86.8 Mbits/sec
    [ 69]  0.0- 8.0 sec   137 MBytes   143 Mbits/sec
    [ 66]  0.0- 8.0 sec  65.6 MBytes  68.7 Mbits/sec
    [ 61]  0.0- 8.0 sec  97.9 MBytes   103 Mbits/sec
    [ 70]  0.0- 8.0 sec  45.5 MBytes  47.6 Mbits/sec
    [ 74]  0.0- 8.1 sec  77.9 MBytes  81.1 Mbits/sec
    [ 78]  0.0- 8.0 sec  32.5 MBytes  34.0 Mbits/sec
    [ 79]  0.0- 8.0 sec  67.6 MBytes  70.9 Mbits/sec
    [ 80]  0.0- 8.0 sec   102 MBytes   107 Mbits/sec
    [ 84]  0.0- 8.0 sec   126 MBytes   132 Mbits/sec
    [ 88]  0.0- 8.0 sec   150 MBytes   157 Mbits/sec
    [ 95]  0.0- 8.0 sec  52.5 MBytes  54.9 Mbits/sec
    [ 82]  0.0- 8.0 sec  79.8 MBytes  83.2 Mbits/sec
    [ 92]  0.0- 8.0 sec  94.2 MBytes  98.7 Mbits/sec
    [ 83]  0.0- 8.0 sec  60.1 MBytes  62.8 Mbits/sec
    [ 91]  0.0- 8.0 sec   201 MBytes   211 Mbits/sec
    [ 90]  0.0- 8.0 sec  46.5 MBytes  48.6 Mbits/sec
    [ 89]  0.0- 8.0 sec   116 MBytes   121 Mbits/sec
    [ 94]  0.0- 8.0 sec   176 MBytes   184 Mbits/sec
    [ 93]  0.0- 8.0 sec  58.2 MBytes  60.9 Mbits/sec
    [ 55]  0.0- 8.0 sec  66.5 MBytes  69.4 Mbits/sec
    [ 56]  0.0- 8.0 sec  60.9 MBytes  63.7 Mbits/sec
    [ 67]  0.0- 8.0 sec  75.8 MBytes  79.1 Mbits/sec
    [ 63]  0.0- 8.0 sec  47.0 MBytes  49.1 Mbits/sec
    [ 85]  0.0- 8.1 sec   108 MBytes   112 Mbits/sec
    [ 76]  0.0-10.0 sec   624 MBytes   523 Mbits/sec
    [ 71]  0.0-10.0 sec   282 MBytes   237 Mbits/sec
    [ 98]  0.0-10.0 sec   335 MBytes   281 Mbits/sec
    [ 97]  0.0-10.0 sec   219 MBytes   184 Mbits/sec
    [ 87]  0.0-10.0 sec   189 MBytes   158 Mbits/sec
    [ 81]  0.0-10.0 sec   254 MBytes   213 Mbits/sec
    [ 72]  0.0-10.0 sec   145 MBytes   121 Mbits/sec
    [ 73]  0.0-10.0 sec   331 MBytes   277 Mbits/sec
    [ 75]  0.0-10.0 sec   203 MBytes   170 Mbits/sec
    [100]  0.0-10.0 sec   145 MBytes   121 Mbits/sec
    [ 96]  0.0-10.0 sec   212 MBytes   177 Mbits/sec
    [ 99]  0.0-10.0 sec   218 MBytes   183 Mbits/sec
    [SUM]  0.0-10.0 sec  10.5 GBytes  9.00 Gbits/sec


UDP
___

TCP mode does not report loss, the UDP tests allow packet loss to be seen along a path. The X710 card had much worse packet loss.


.. code-block:: console

    ------------------------------------------------------------
    Client connecting to 10.205.13.11, UDP port 5001
    Sending 1470 byte datagrams
    UDP buffer size:  208 KByte (default)
    ------------------------------------------------------------
    [  4] local 10.205.13.8 port 54136 connected with 10.205.13.11 port 5001
    [ ID] Interval       Transfer     Bandwidth
    [  4]  0.0-10.0 sec   972 MBytes   815 Mbits/sec
    [  4] Sent 693118 datagrams
    [  4] Server Report:
    [  4]  0.0-10.0 sec   874 MBytes   733 Mbits/sec   0.007 ms 69654/693117 (10%)
    [  4]  0.0-10.0 sec  1 datagrams received out-of-order
    [  3] local 10.205.13.8 port 5001 connected with 10.205.13.11 port 47696
    [  3]  0.0-10.0 sec   965 MBytes   810 Mbits/sec   0.006 ms 2293/690825 (0.33%)
    [  3]  0.0-10.0 sec  1 datagrams received out-of-order


sockperf
--------

https://www.mankier.com/3/sockperf

Server
______

.. code-block:: console

    sockperf: +INFO:
    mode = 1
    with_sock_accl = 0
    msg_size = 65506
    msg_size_range = 0
    sec_test_duration = 1
    data_integrity = 0
    packetrate_stats_print_ratio = 0
    burst_size = 1
    packetrate_stats_print_details = 0
    fd_handler_type = 2
    mthread_server = 1
    sock_buff_size = 0
    threads_num = 4
    threads_affinity =
    is_blocked = 1
    is_nonblocked_send = 0
    do_warmup = 1
    pre_warmup_wait = 0
    is_vmarxfiltercb = 0
    is_vmazcopyread = 0
    mc_loop_disable = 1
    mc_ttl = 2
    tcp_nodelay = 1
    client_work_with_srv_num = 1
    b_server_reply_via_uc = 0
    b_server_dont_reply = 0
    b_server_detect_gaps = 0
    mps = 10000
    client_bind_info = 0.0.0.0:0
    reply_every = 100
    b_client_ping_pong = 0
    b_no_rdtsc = 0
    sender_affinity = <empty>
    receiver_affinity = <empty>
    b_stream = 0
    daemonize = 0
    feedfile_name = sockperf.conf
    tos = 0

    sockperf: == version #2.6 ==
    sockperf: +INFO: taking time, using the given settings, consumes 35.328 nsec
    sockperf: +INFO: taking rdtsc directly consumes 11.969 nsec
    sockperf: Running 4 threads to manage 4 sockets
    sockperf: thread 8611: fd_min: 5, fd_max : 5, fd_num: 1
    sockperf: thread 8610: fd_min: 4, fd_max : 4, fd_num: 1
    sockperf: [fd=4] Binding to: 0.0.0.0:11111...
    sockperf: [fd=5] Binding to: 0.0.0.0:11112...
    sockperf: thread 8612: fd_min: 6, fd_max : 6, fd_num: 1
    sockperf: [SERVER] listen on:sockperf: [SERVER] listen on:
    [ 0] IP = 0.0.0.0         PORT = 11111 # TCP
    sockperf: [fd=6] Binding to: 0.0.0.0:11113...

    sockperf: [SERVER] listen on:
    [ 0] IP = 0.0.0.0         PORT = 11113 # TCP
    sockperf: Warmup stage (sending a few dummy messages)...
    sockperf: [tid 8612] using select() to block on socket(s)
    sockperf: Warmup stage (sending a few dummy messages)...
    [ 0] IP = 0.0.0.0         PORT = 11112 # TCP
    sockperf: Warmup stage (sending a few dummy messages)...
    sockperf: [tid 8611] using select() to block on socket(s)
    sockperf: [tid 8610] using select() to block on socket(s)
    sockperf: thread 8614: fd_min: 7, fd_max : 7, fd_num: 1
    sockperf: [fd=7] Binding to: 0.0.0.0:11114...
    sockperf: [SERVER] listen on:
    [ 0] IP = 0.0.0.0         PORT = 11114 # TCP
    sockperf: Warmup stage (sending a few dummy messages)...
    sockperf: [tid 8614] using select() to block on socket(s)

Client
______
To reach 10Gbps speeds 4 threads were ran simultaneously, scheduled using `atq`.


.. code-block:: console

    sockperf: +INFO:
    mode = 0
    with_sock_accl = 0
    msg_size = 1472
    msg_size_range = 0
    sec_test_duration = 30
    data_integrity = 0
    packetrate_stats_print_ratio = 0
    burst_size = 1
    packetrate_stats_print_details = 0
    fd_handler_type = 0
    mthread_server = 0
    sock_buff_size = 0
    threads_num = 1
    threads_affinity =
    is_blocked = 1
    is_nonblocked_send = 0
    do_warmup = 1
    pre_warmup_wait = 0
    is_vmarxfiltercb = 0
    is_vmazcopyread = 0
    mc_loop_disable = 1
    mc_ttl = 2
    tcp_nodelay = 0
    client_work_with_srv_num = 1
    b_server_reply_via_uc = 0
    b_server_dont_reply = 0
    b_server_detect_gaps = 0
    mps = 10000000
    client_bind_info = 0.0.0.0:0
    reply_every = 1073741824
    b_client_ping_pong = 0
    b_no_rdtsc = 0
    sender_affinity = <empty>
    receiver_affinity = <empty>
    b_stream = 1
    daemonize = 0
    feedfile_name = <empty>
    tos = 0

    sockperf: == version #2.6 ==
    sockperf: +INFO: taking time, using the given settings, consumes 10.448 nsec
    sockperf: +INFO: taking rdtsc directly consumes 10.439 nsec
    sockperf: [fd=3] Binding to: 0.0.0.0:0...
    sockperf: [fd=3] Connecting to: 10.205.13.11:11111...
    sockperf[CLIENT] send on:
    [ 0] IP = 10.205.13.11    PORT = 11111 # TCP
    sockperf: Warmup stage (sending a few dummy messages)...
    sockperf: Starting test...
    sockperf: Test end (interrupted by timer)
    sockperf: Test ended
    sockperf: Total of 5556686 messages sent in 30.100 sec

    sockperf: Summary: Message Rate is 184608 [msg/sec]
    sockperf: Summary: BandWidth is 0.259 GBps (2.073 Gbps)


.. code-block:: console


    sockperf: +INFO:
    mode = 0
    with_sock_accl = 0
    msg_size = 1472
    msg_size_range = 0
    sec_test_duration = 30
    data_integrity = 0
    packetrate_stats_print_ratio = 0
    burst_size = 1
    packetrate_stats_print_details = 0
    fd_handler_type = 0
    mthread_server = 0
    sock_buff_size = 0
    threads_num = 1
    threads_affinity =
    is_blocked = 1
    is_nonblocked_send = 0
    do_warmup = 1
    pre_warmup_wait = 0
    is_vmarxfiltercb = 0
    is_vmazcopyread = 0
    mc_loop_disable = 1
    mc_ttl = 2
    tcp_nodelay = 0
    client_work_with_srv_num = 1
    b_server_reply_via_uc = 0
    b_server_dont_reply = 0
    b_server_detect_gaps = 0
    mps = 10000000
    client_bind_info = 0.0.0.0:0
    reply_every = 1073741824
    b_client_ping_pong = 0
    b_no_rdtsc = 0
    sender_affinity = <empty>
    receiver_affinity = <empty>
    b_stream = 1
    daemonize = 0
    feedfile_name = <empty>
    tos = 0

    sockperf: == version #2.6 ==
    sockperf: +INFO: taking time, using the given settings, consumes 19.754 nsec
    sockperf: +INFO: taking rdtsc directly consumes 11.571 nsec
    sockperf: [fd=3] Binding to: 0.0.0.0:0...
    sockperf: [fd=3] Connecting to: 10.205.13.11:11112...
    sockperf[CLIENT] send on:
    [ 0] IP = 10.205.13.11    PORT = 11112 # TCP
    sockperf: Warmup stage (sending a few dummy messages)...
    sockperf: Starting test...
    sockperf: Test end (interrupted by timer)
    sockperf: Test ended
    sockperf: Total of 5492150 messages sent in 30.100 sec

    sockperf: Summary: Message Rate is 182465 [msg/sec]
    sockperf: Summary: BandWidth is 0.256 GBps (2.049 Gbps)


.. code-block:: console


    sockperf: +INFO:
    mode = 0
    with_sock_accl = 0
    msg_size = 1472
    msg_size_range = 0
    sec_test_duration = 30
    data_integrity = 0
    packetrate_stats_print_ratio = 0
    burst_size = 1
    packetrate_stats_print_details = 0
    fd_handler_type = 0
    mthread_server = 0
    sock_buff_size = 0
    threads_num = 1
    threads_affinity =
    is_blocked = 1
    is_nonblocked_send = 0
    do_warmup = 1
    pre_warmup_wait = 0
    is_vmarxfiltercb = 0
    is_vmazcopyread = 0
    mc_loop_disable = 1
    mc_ttl = 2
    tcp_nodelay = 0
    client_work_with_srv_num = 1
    b_server_reply_via_uc = 0
    b_server_dont_reply = 0
    b_server_detect_gaps = 0
    mps = 10000000
    client_bind_info = 0.0.0.0:0
    reply_every = 1073741824
    b_client_ping_pong = 0
    b_no_rdtsc = 0
    sender_affinity = <empty>
    receiver_affinity = <empty>
    b_stream = 1
    daemonize = 0
    feedfile_name = <empty>
    tos = 0

    sockperf: == version #2.6 ==
    sockperf: +INFO: taking time, using the given settings, consumes 10.778 nsec
    sockperf: +INFO: taking rdtsc directly consumes 10.707 nsec
    sockperf: [fd=3] Binding to: 0.0.0.0:0...
    sockperf: [fd=3] Connecting to: 10.205.13.11:11113...
    sockperf[CLIENT] send on:
    [ 0] IP = 10.205.13.11    PORT = 11113 # TCP
    sockperf: Warmup stage (sending a few dummy messages)...
    sockperf: Starting test...
    sockperf: Test end (interrupted by timer)
    sockperf: Test ended
    sockperf: Total of 5484531 messages sent in 30.100 sec

    sockperf: Summary: Message Rate is 182209 [msg/sec]
    sockperf: Summary: BandWidth is 0.256 GBps (2.046 Gbps)

.. code-block:: console

    sockperf: +INFO:
    mode = 0
    with_sock_accl = 0
    msg_size = 1472
    msg_size_range = 0
    sec_test_duration = 30
    data_integrity = 0
    packetrate_stats_print_ratio = 0
    burst_size = 1
    packetrate_stats_print_details = 0
    fd_handler_type = 0
    mthread_server = 0
    sock_buff_size = 0
    threads_num = 1
    threads_affinity =
    is_blocked = 1
    is_nonblocked_send = 0
    do_warmup = 1
    pre_warmup_wait = 0
    is_vmarxfiltercb = 0
    is_vmazcopyread = 0
    mc_loop_disable = 1
    mc_ttl = 2
    tcp_nodelay = 0
    client_work_with_srv_num = 1
    b_server_reply_via_uc = 0
    b_server_dont_reply = 0
    b_server_detect_gaps = 0
    mps = 10000000
    client_bind_info = 0.0.0.0:0
    reply_every = 1073741824
    b_client_ping_pong = 0
    b_no_rdtsc = 0
    sender_affinity = <empty>
    receiver_affinity = <empty>
    b_stream = 1
    daemonize = 0
    feedfile_name = <empty>
    tos = 0

    sockperf: == version #2.6 ==
    sockperf: +INFO: taking time, using the given settings, consumes 10.454 nsec
    sockperf: +INFO: taking rdtsc directly consumes 11.193 nsec
    sockperf: [fd=3] Binding to: 0.0.0.0:0...
    sockperf: [fd=3] Connecting to: 10.205.13.11:11114...
    sockperf[CLIENT] send on:
    [ 0] IP = 10.205.13.11    PORT = 11114 # TCP
    sockperf: Warmup stage (sending a few dummy messages)...
    sockperf: Starting test...
    sockperf: Test end (interrupted by timer)
    sockperf: Test ended
    sockperf: Total of 5353395 messages sent in 30.100 sec

    sockperf: Summary: Message Rate is 177854 [msg/sec]
    sockperf: Summary: BandWidth is 0.250 GBps (1.997 Gbps)


Client single thread
____________________

.. code-block:: console


            sockperf: +INFO:
            mode = 0
            with_sock_accl = 0
            msg_size = 1472
            msg_size_range = 0
            sec_test_duration = 30
            data_integrity = 0
            packetrate_stats_print_ratio = 0
            burst_size = 1
            packetrate_stats_print_details = 0
            fd_handler_type = 0
            mthread_server = 0
            sock_buff_size = 0
            threads_num = 1
            threads_affinity =
            is_blocked = 1
            is_nonblocked_send = 0
            do_warmup = 1
            pre_warmup_wait = 0
            is_vmarxfiltercb = 0
            is_vmazcopyread = 0
            mc_loop_disable = 1
            mc_ttl = 2
            tcp_nodelay = 0
            client_work_with_srv_num = 1
            b_server_reply_via_uc = 0
            b_server_dont_reply = 0
            b_server_detect_gaps = 0
            mps = 10000000
            client_bind_info = 0.0.0.0:0
            reply_every = 1073741824
            b_client_ping_pong = 0
            b_no_rdtsc = 0
            sender_affinity = <empty>
            receiver_affinity = <empty>
            b_stream = 1
            daemonize = 0
            feedfile_name = <empty>
            tos = 0

    sockperf: == version #2.6 ==
    sockperf: +INFO: taking time, using the given settings, consumes 10.487 nsec
    sockperf: +INFO: taking rdtsc directly consumes 10.439 nsec
    sockperf: [fd=3] Binding to: 0.0.0.0:0...
    sockperf: [fd=3] Connecting to: 10.205.13.11:11111...
    sockperf[CLIENT] send on:
    [ 0] IP = 10.205.13.11    PORT = 11111 # TCP
    sockperf: Warmup stage (sending a few dummy messages)...
    sockperf: Starting test...
    sockperf: Test end (interrupted by timer)
    sockperf: Test ended
    sockperf: Total of 10665465 messages sent in 30.100 sec

    sockperf: Summary: Message Rate is 354337 [msg/sec]
    sockperf: Summary: BandWidth is 0.497 GBps (3.979 Gbps)
