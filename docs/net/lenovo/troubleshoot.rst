
Troubleshooting
~~~~~~~~~~~~~~~

Ping
----
Ping is very flexible in CNOS. There are many options that can be used to test specific ports, etc.

Issuing ping with no options will start a wizard:

.. code-block:: console

  # ping
  Vrf context to use [default]: management
  Protocol [ip]:
  Target IP address or hostname: 8.8.8.8
  Repeat count [5]:
  Datagram size [56]:
  Timeout in seconds [2]:
  Sending interval in seconds [1]:
  Extended commands [n]:
  PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
  64 bytes from 8.8.8.8: icmp_seq=1 ttl=120 time=15.2 ms
  64 bytes from 8.8.8.8: icmp_seq=2 ttl=120 time=15.4 ms
  64 bytes from 8.8.8.8: icmp_seq=3 ttl=120 time=16.2 ms
  64 bytes from 8.8.8.8: icmp_seq=4 ttl=120 time=15.1 ms
  64 bytes from 8.8.8.8: icmp_seq=5 ttl=120 time=16.8 ms

  --- 8.8.8.8 ping statistics ---
  5 packets transmitted, 5 received, 0% packet loss, time 4005ms
  rtt min/avg/max/mdev = 15.152/15.783/16.800/0.647 ms

.. note:: Use the default VRF if testing from a VLAN (SVI) interface


Diagnostics
-----------

.. code-block:: console

  # show tech-support


Debugging
---------

.. code-block:: console

  (config)# debug monitor
  (config)# debug telemetry all-messages
  (config)# no debug monitor
