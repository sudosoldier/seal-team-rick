General Concepts
=================

CLI Modes
---------

``>`` = unprivileged mode
``#`` = privileged mode
``(config)#`` = config mode

Topology
--------

The pipes need to get fatter as you move towards the core. If your pipes aren't fat, you need to bond your little pipes into big ol' pipes. Go check out LACP/LAG (most vendors) or PAgP/etherchannel (Cisco).

The two most common toplogies are your typical `mesh`, and a `spine and leaf` layout.

Spanning Tree
-------------
In 99% of scenarios you should enable spanning tree, and you must must always use a RSTP compliant version of spanning tree.

RSTP automatically detects `edge ports`, that is, ports on which an end device is attached. Ports will be determined to be edge ports if no BPDUs are received on that link for a pre-defined amount of time. RSTP allows edge ports to immediately transition to the forwarding state.

The following protocols are the only spanning tree versions you should use:

- RSTP
- Rapid-PVST+
- MSTP

Switched Virtual Interface
--------------------------
Fancy term for assigning an IP address to a VLAN.


Virtual Routing and Forwarding
------------------------------
Virtualization for your routing so you can have different routing for your management network and your data network.


Transceivers
------------
Not all SFP are created equally, there is no standard here really. Check with the switch vendor to make sure the tranceivers you are buying are compatible.


Routing
-------
- Reply packets do NOT automatically follow the reverse path of the one taken by the original request. All packets are routed according to the routing table of the host at each step of the way.

