SX1012
~~~~~~
This switch is going EOL, but if you have a case and need documentation to support it, you can find that here:

http://www.mellanox.com/related-docs/prod_management_software/MLNX-OS_ETH_v3_6_3508_UM.pdf
