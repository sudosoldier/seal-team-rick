Department of Diagnostic Technology
====================================

.. image:: images/house.jpg


The single most important link in the world:

https://landscape.cncf.io

.. toctree::
  :maxdepth: 1
  :caption: General Reference

  reference/templates
  reference/known_bugs
  reference/contrib

.. toctree::
  :maxdepth: 1
  :caption: Hardware

  hardware/index

.. toctree::
  :maxdepth: 1
  :caption: Networking

  net/index

.. toctree::
  :maxdepth: 1
  :caption: Virtualization

  hypervisor/index

.. toctree::
  :maxdepth: 1
  :caption: Operating Systems

  os/windows/index
  os/linux/index

.. toctree::
  :maxdepth: 1
  :caption: Containers

  containers/index

.. toctree::
  :maxdepth: 1
  :caption: Development

  dev/index
  automation/index

.. toctree::
  :maxdepth: 1
  :caption: Infrastructure

  infra/index

.. toctree::
  :maxdepth: 1
  :caption: Certs

  certs/networking

.. toctree::
  :maxdepth: 1
  :caption: Cloud

  cloud/index

.. toctree::
  :maxdepth: 1
  :caption: Misc

  misc/snippets
  misc/workarounds

.. toctree::
  :maxdepth: 1
  :caption: db

  db/index
