.. toctree::
  :maxdepth: 3

  git
  tools
  configs
  testing
  mailinglist
