Configuration
~~~~~~~~~~~~~


atom
----

Packages:
- file-types
- language-ansible

configuration

ctrl shift p
type in config
it will open the config.cson file
paste:

.. code-block:: yaml

    "*":
    "file-types":
        ".*.yml$": "source.ansible-advanced"
        ".*/group_vars/.*": "source.ansible-advanced"
        ".*/host_vars/.*": "source.ansible-advanced"
    "ansible-vault":
        path: "/bin/ansible-vault"
        vault_automatic_de_and_encrypt: true
        vault_password_file_flag: true
    "atom-beautify":
        yaml:
        beautify_on_save: true
    core:
        autoHideMenuBar: true
        disabledPackages: [
        "linter-lua"
        ]
        telemetryConsent: "no"
        customFileTypes:
        "source.ansible-advanced": [
            "yml"
        ]
    editor:
        fontSize: 24
    "exception-reporting":
        userId: "67de14f7-0021-4151-9c8a-710f26b70144"
    "file-types": {}
    "linter-ansible-linting":
        useProjectConfig: true
    "linter-ui-default":
        alwaysTakeMinimumSpace: true
        panelHeight: 196
        showPanel: true
        showProviderName: true
    "pretty-json":
        prettifyOnSaveJSON: true
    welcome:
        showOnStartup: false


pylint
------

pylint --generate-rcfile > .pylintrc
pylint --list-msgs
import foo # pylint: disable=W0611

gitlab
------

https://docs.gitlab.com/omnibus/docker/
https://docs.gitlab.com/runner/install/docker.html

.. code-block:: console

  $ sudo docker run --detach --hostname gitlab.cdraper.io --publish 443:443 --publish 80:80 --publish 22:22 --name gitlab --restart always --volume /srv/gitlab/config:/etc/gitlab --volume /srv/gitlab/logs:/var/log/gitlab --volume /srv/gitlab/data:/var/opt/gitlab gitlab/gitlab-ce:latest
  $ sudo docker exec -it gitlab editor /etc/gitlab/gitlab.rb
  $ sudo docker restart gitlab


gitlab
-------

.. code-block:: console

    2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 7c:4c:58:ca:6e:e3 brd ff:ff:ff:ff:ff:ff
    inet 10.205.108.45/23 brd 10.205.109.255 scope global dynamic ens3
    valid_lft 6637sec preferred_lft 6637sec
    inet6 fe80::7e4c:58ff:feca:6ee3/64 scope link
    valid_lft forever preferred_lft forever
