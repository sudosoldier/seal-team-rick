Tools
-----

Repository cleaner
~~~~~~~~~~~~~~~~~~

https://github.com/IBM/BluePic/wiki/Using-BFG-Repo-Cleaner-tool-to-remove-sensitive-files-from-your-git-repo


Argbash
~~~~~~~

https://argbash.io/


mtimproxy
~~~~~~~~~

https://docs.mitmproxy.org/stable/