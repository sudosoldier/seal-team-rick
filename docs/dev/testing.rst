Testing
=======


Kitchen CI
~~~~~~~~~~

Pass environment variables to docker containers

.. code-block:: yaml

  driver:
    name: whatever
    something: <%= ENV['asdf'] %>


Jenkins
~~~~~~~


Example `Jenkinsfile` - just checking syntax of ansible playbooks

.. code-block:: json

  pipeline {
    agent any
    stages {
      stage('config') {
        steps {
          sh 'ansible-pull -d ~/ansible -U http://support:gitlab+deploy-token-1@scansible.sprt.REDACTED.com/cdraper/ansible.git -e REDACTED_username=DUMMY'
        }
      }
      stage('syntax') {
        parallel {
          stage('demo') {
            steps {
              ansiblePlaybook(playbook: 'demo.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('diagnostics') {
            steps {
              ansiblePlaybook(playbook: 'diagnostics.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('chassis-failure') {
            steps {
              ansiblePlaybook(extras: '--syntax-check', playbook: 'chassis-failure.yml', colorized: true)
            }
          }
          stage('email') {
            steps {
              ansiblePlaybook(playbook: 'email.yml', extras: '--syntax-check', colorized: true)
            }
          }
          stage('exports') {
            steps {
              ansiblePlaybook(playbook: 'exports.yml', extras: '--syntax-check')
            }
          }
          stage('onecli') {
            steps {
              ansiblePlaybook(playbook: 'onecli.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('orphan-snapshots') {
            steps {
              ansiblePlaybook(playbook: 'orphan-snapshots.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('pcap') {
            steps {
              ansiblePlaybook(playbook: 'pcap.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('rebalance') {
            steps {
              ansiblePlaybook(playbook: 'rebalance.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('retier') {
            steps {
              ansiblePlaybook(playbook: 'retier.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('sar') {
            steps {
              ansiblePlaybook(playbook: 'sar.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('snmpd') {
            steps {
              ansiblePlaybook(playbook: 'snmpd.yml', extras: '--syntax-check', colorized: true)
            }
          }
          stage('vsd-lineage') {
            steps {
              ansiblePlaybook(playbook: 'vsd-lineage.yml', extras: '--syntax-check', colorized: true)
            }
          }
          stage('vsd-perf-collector') {
            steps {
              ansiblePlaybook(playbook: 'vsd-perf-collector.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('vsd-perf-parser') {
            steps {
              ansiblePlaybook(playbook: 'vsd-perf-parser.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('sched-update') {
            steps {
              ansiblePlaybook(playbook: 'sched-update.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('sched-service-restart') {
            steps {
              ansiblePlaybook(playbook: 'sched-service-restart.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('gcloud-setup-prompt') {
            steps {
              ansiblePlaybook(playbook: 'gcloud-setup-prompt.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('gcloud-setup-static') {
            steps {
              ansiblePlaybook(playbook: 'gcloud-setup-static.yml', colorized: true, extras: '--syntax-check')
            }
          }
          stage('local') {
            steps {
              ansiblePlaybook(playbook: 'local.yml', colorized: true, extras: '--syntax-check')
            }
          }
        }
      }
    }
  }


Messy test deployment via ansible

.. code-block:: yaml

  ---
  - hosts:                                  jenkinz
    vars:
      jenkins_reset:                        no
      jenkins_package_state:                latest
      jenkins_admin_username:               admin
      jenkins_admin_password:               !vault |
  $ANSIBLE_VAULT;1.1;AES256
  64316464303337363762336332653665653632646333633732666335303261656665646532333662
  3834666335623736656262653164346236626532653130350a633339336633346236303765396532
  32663166316161643733323161613164376630366463636238346638393164633864353635616464
  3139346261333064630a366634363031636165373432363633363030396136376131393934626237
  6164
      jenkins_plugins_state:                present
      jenkins_connection_delay:             5
      jenkins_connection_retries:           60
      jenkins_plugins_install_dependencies: true
      jenkins_plugin_timeout:               30
      jenkins_plugin_updates_expiration:    0
      jenkins_updates_url:                  "https://updates.jenkins.io"
      jenkins_plugins:
      - mailer
      - git
      - ssh
      - ssh-agent
      - ssh-steps
      - splunk-devops
      - logstash
      - elasticsearch-query
      - fluentd
      - prometheus
      - credentials-binding
      - github-branch-source
      - matrix-project
      - scm-api
      - ssh-credentials
      - ansible
      - ansible-tower
      - pipeline-github
      - pipeline-stage-view
      - pipeline-graph-analysis
      - workflow-aggregator
      - simple-travis-runner
      - pipeline-stage-step
      - pipeline-aws
      - workflow-basic-steps
      - pipeline-input-step
      - pipeline-build-step
      - workflow-api
      - github-organization-folder
      - blueocean
      - blueocean-pipeline-editor
      - blueocean-dashboard
      - blueocean-web
      - blueocean-github-pipeline
      - blueocean-bitbucket-pipeline
      - blueocean-config
      - blueocean-commons
      - display-url-api
      - pipeline-github-lib
      - workflow-multibranch
      - variant
      - blueocean-rest
      - credentials
      - plain-credentials
      - github
      - github-api
      - blueocean-pipeline-api-impl
      - apache-httpcomponents-client-4-api
      - lockable-resources
      - structs
      - junit
      - pipeline-rest-api
      - handlebars
      - jquery-detached
      - momentjs
      - workflow-job
      - git-client
      - workflow-step-api
      - workflow-scm-step
    roles:
    - role:                                 geerlingguy.jenkins
      become:                               yes
    pre_tasks:
    - name:                                 check if python3 is present
      changed_when:                         false
      failed_when:                          false
      register:                             python3_check
      raw:                                  which python3
    - name:                                 install python3
      become:                               yes
      when:                                 python3_check.rc == 1
      raw:                                  apt install python3 -y
    - name:                                 install java
      register:                             java_result
      become:                               yes
      apt:
        name:                               openjdk-8-jdk
    - name:                                 update alternatives
      alternatives:
        name:                               java
        path:                               /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
    - name:                                 remove jenkins
      when:                                 jenkins_reset
      become:                               yes
      apt:
        name:                               jenkins
        state:                              absent
        purge:                              yes


Playbook used as a hook to run and test playbooks, allows for definition of target hosts and what task set to run

.. code-block:: yaml

  ---
  - hosts:     "{{ test_hosts }}"
    pre_tasks:
    - import_tasks: "{{ playbook_dir }}/../pre_tasks/init.yml"
      when:    "test_hosts == 'ansible-entrynode' or test_hosts == 'ansible-nodes'"
    tasks:
    - import_tasks: "{{ playbook_dir }}/../include/{{ test_tasks }}"
  #  when:     not ansible_check_mode

Scratch example of trying to automate testing for gcloud nested virt issue

.. code-block:: yaml

  ---
  - hosts:          prod
    vars:
      vm_uuid_list:
      - b521d73a-4a0e-4c69-bdc6-2b3ce98f8beb
      - 2b310a5a-735a-4029-af12-371b524c7926
      - 33f38d06-791f-4870-90fe-f42114621cb0
      - a6678353-ae08-444e-a77a-a10451b393bb
      - c392f67f-208d-43a0-a29e-42e9b5f72375
    pre_tasks:
    - include:      "{{ playbook_dir }}/../../gcloud-active-mode.yml"
    post_tasks:
    - include:      "{{ playbook_dir }}/../../gcloud-passive-mode.yml"
    tasks:
    - name:         test
      debug:
        msg:        "{{ item }}"
      with_list:    "{{ vm_uuid_list }}"
    - name:         checksum
      shell:        find /etc -type f -exec md5sum {} \; | sort -k 2 | md5sum
    - name:         enable kvm tracing
      shell:        echo 1 > /sys/kernel/debug/tracing/events/kvm/enable
    - name:         write trace pipe to file
      shell:        cat /sys/kernel/debug/tracing/trace_pipe > /tmp/kvm_trace.log &
    - name:         start vms
      shell:        sc vm start uuid {{ item }}; strace -p `pgrep -f {{ item }}` -o /root/{{ item }}_strace.log &
      with_list:    "{{ vm_uuid_list }}"
      args:
        executable: /bin/bash
    - name:         get list of vm pids
      register:     vm_pid_list
      shell:        pgrep qemu-kvm
    - name:         debug
      debug:
        msg:        "{{ item }}"
      with_list:    "{{ vm_pid_list.stdout_lines }}"
    - name:         strace
      shell:        strace -p `pgrep -f {{ item }}` -o /root/{{ item }}_strace.log &
      with_list:    "{{ vm_pid_list.stdout_lines }}"
    - name:         wait for 5-10 minutes?
      pause:        minutes=1
    - name:         kill kvm trace pids
      shell:        kill $(pgrep -f /sys/kernel/debug/tracing/trace_pipe)
    - name:         stop tracing
      shell:        echo 0 > /sys/kernel/debug/tracing/events/kvm/enable
    - name:         kill straces
      shell:        killall strace
    - name:         shutdown vms
      shell:        sc vm shutdown uuid {{ item }}
      with_list:    "{{ vm_uuid_list }}"
  - hosts:          qe
    vars:
      vm_uuid_list:
      - 5fc6b2fc-6d5e-4429-b447-ca1b40ddc525
      - 2c9dc5e0-1441-429e-ad17-bdca90396e6d
      - 6b078b44-643a-4c3a-bff9-ebe890ab0a5b
      - 0b745194-1864-4b24-af94-bc11fc9ba11e
      - 69c0b488-896c-4e54-8560-8362c2ad2690
    tasks:
    - name:         test
      debug:
        msg:        "{{ item }}"
      with_list:    "{{ vm_uuid_list }}"
    - name:         enable kvm tracing
      shell:        echo 1 > /sys/kernel/debug/tracing/events/kvm/enable
    - name:         write trace pipe to file
      shell:        cat /sys/kernel/debug/tracing/trace_pipe > /tmp/kvm_trace.log &
    - name:         start vms
      shell:        sc vm start uuid {{ item }}; strace -p `pgrep -f {{ item }}` -o /root/{{ item }}_strace.log &
      with_list:    "{{ vm_uuid_list }}"
      args:
        executable: /bin/bash
    - name:         get list of vm pids
      register:     vm_pid_list
      shell:        pgrep qemu-kvm
    - name:         debug
      debug:
        msg:        "{{ item }}"
      with_list:    "{{ vm_pid_list.stdout_lines }}"
    - name:         strace
      shell:        strace -p `pgrep -f {{ item }}` -o /root/{{ item }}_strace.log &
      with_list:    "{{ vm_pid_list.stdout_lines }}"
    - name:         wait for 5-10 minutes?
      pause:        minutes=1
    - name:         kill kvm trace pids
      shell:        kill $(pgrep -f /sys/kernel/debug/tracing/trace_pipe)
    - name:         stop tracing
      shell:        echo 0 > /sys/kernel/debug/tracing/events/kvm/enable
    - name:         kill straces
      shell:        killall strace
    - name:         shutdown vms
      shell:        sc vm shutdown uuid {{ item }}
      with_list:    "{{ vm_uuid_list }}"


  #find /etc -type f -exec md5sum {} \; | sort -k 2 | md5sum

Travis CI
~~~~~~~~~


Circle CI
~~~~~~~~~



Python
~~~~~~

Mutation
--------

This is really cool, but wasn't for Python

https://stryker-mutator.io

So I found this stuff

https://mutation.readthedocs.io/en/latest/