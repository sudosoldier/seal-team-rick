Git
===

Reference
~~~~~~~~~
https://www.atlassian.com/git/tutorials/rewriting-history


Wipe all history
~~~~~~~~~~~~~~~~


https://stackoverflow.com/questions/13716658/how-to-delete-all-commit-history-in-github

.. code-block:: console

  $ git checkout --orphan latest_branch
  $ git add -A
  $ git commit -am "Wipe history"
  $ git branch -D master
  $ git branch -m master
  $ git push -f origin master

Wipe a single file from history
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

https://stackoverflow.com/questions/2047465/how-can-i-delete-a-file-from-git-repo

.. code-block:: console

  $ git filter-branch -f --index-filter 'git rm --cached --ignore-unmatch gcloud.yml'

Show changes between release builds
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

  # git log --pretty=oneline REDACTED-REDACTED-<VERSION>.<BUILD>...REDACTED-REDACTED-<VERSION>.<BUILD>

Duplicate a repository
~~~~~~~~~~~~~~~~~~~~~~

For example, duplicating a repository that was on bitbucket to github.

https://help.github.com/articles/duplicating-a-repository/


Tracking dotfiles in git
~~~~~~~~~~~~~~~~~~~~~~~~
The majority of linux workstation configuration is in package management and dotfile configuration i.e. `.vimrc` `.zshrc` `.backrc` etc etc.
Tracking them in git is an option for backup/tracking changes to your configuration.

https://wiki.archlinux.org/index.php/Dotfiles#Tracking_dotfiles_directly_with_Git

https://dotfiles.github.io/

https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/

http://www.saintsjd.com/2011/01/what-is-a-bare-git-repository/

.. code-block:: console

  ~/git/ansible >>> git config --get remote.origin.url                                                                                                                                    ±[master]
  git@10.205.108.45:cdraper/ansible.git
  ~/git/ansible >>> git remote show origin                                                                                                                                                ±[master]
  * remote origin
  Fetch URL: git@10.205.108.45:cdraper/ansible.git
  Push  URL: git@10.205.108.45:cdraper/ansible.git
  HEAD branch: master
  Remote branches:
  master                           tracked
  refs/remotes/origin/ansible-pull stale (use 'git remote prune' to remove)
  refs/remotes/origin/beta         stale (use 'git remote prune' to remove)
  refs/remotes/origin/onecli       stale (use 'git remote prune' to remove)
  refs/remotes/origin/stable       stale (use 'git remote prune' to remove)
  refs/remotes/origin/testing      stale (use 'git remote prune' to remove)
  Local branch configured for 'git pull':
  master merges with remote master
  Local ref configured for 'git push':
  master pushes to master (up to date)


gitflow
~~~~~~~

eval if this is worthwile

negative opinion:
https://hackernoon.com/gitflow-is-a-poor-branching-model-hack-d46567a156e7

how to
https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
https://datasift.github.io/gitflow/IntroducingGitFlow.html
https://danielkummer.github.io/git-flow-cheatsheet/
https://github.com/nvie/gitflow


learn git
~~~~~~~~~
https://www.codecademy.com/courses/learn-git/lessons/git-workflow/exercises/git-init


search for releases containing a specific commit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

  # git tag -l --contains=<SHA-1>

search for commits that introduced code
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

  # git log -S <search term>

determine remote url
~~~~~~~~~~~~~~~~~~~~
https://stackoverflow.com/questions/4089430/how-can-i-determine-the-url-that-a-local-git-repository-was-originally-cloned-fr


accidentally working on wrong branch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

  # git stash
  # git checkout branch123
  # git stash apply

what did i branch my branch from again?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell

  git show-branch -a \
  | grep '\*' \
  | grep -v `git rev-parse --abbrev-ref HEAD` \
  | head -n1 \
  | sed 's/.*\[\(.*\)\].*/\1/' \
  | sed 's/[\^~].*//'


store dotfiles
~~~~~~~~~~~~~~

https://www.anand-iyer.com/blog/2018/a-simpler-way-to-manage-your-dotfiles.html

store credentials for https
~~~~~~~~~~~~~~~~~~~~~~~~~~~

https://stackoverflow.com/questions/35942754/how-to-save-username-and-password-in-git