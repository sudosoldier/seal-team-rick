terraform
=========

.. code-block:: console

    1045  ~/go/bin/govc -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=false -vm.ip=10.10.20.14
    1046  ~/go/bin/govc vm.info -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=false -vm.ip=10.10.20.14
    1047  ~/go/bin/govc vm.info -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -vm.ip=10.10.20.14
    1048  vim .zshrc
    1049  source .zshrc
    1050  govc vm.info -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -vm.ip=10.10.20.14
    1051  govc ls -l -i /IvyTechDC01/InfoSec\ -\ Backup/Linux -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true
    1052  govc ls -l -i /IvyTechDC01/ -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true
    1053  govc ls -l -i /IvyTechDC01/
    1054  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/
    1055  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/Infosec\ -\ Backup/
    1056  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/vm
    1057  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/vm/Infosec - backup
    1058  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/vm/Infosec\ -\ backup
    1059  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/vm/Infosec \- \backup
    1060  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i '/IvyTechDC01/vm/Infosec - backup'
    1061  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i "/IvyTechDC01/vm/Infosec - backup"
    1062  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/vm/'Infosec - Backup'
    1063  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/vm/'Infosec - Backup'/Linux
    1064  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/vm/'Infosec - Backup'/Linux/col-nessusscp0
    1065  govc find -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true /IvyTechDC01 -type p
    1066  govc vm.info -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -vm.ip=10.10.20.15
    1067  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/vm/Infosec - Backup/Linux/col-nessusscnp0
    1068  govc ls -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true -l -i /IvyTechDC01/vm/'Infosec - Backup'/Linux/col-nessusscnp0
    1069  govc find -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true /IvyTechDC01 -type s
    1070  govc find -u='cdraper11@ivytech.edu':'gT72&DgnwzNTtq'@co-vcenterp0.ivytech.local -k=true /IvyTechDC01 -type c
