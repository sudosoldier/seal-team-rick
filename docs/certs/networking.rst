Study Tools
===========

virl
~~~~

gns3
~~~~
https://community.gns3.com/software


packettracer
~~~~~~~~~~~~



RFC Reference
==============

-  RFC 768: UDP
-  RFC 783: TFTP
-  RFC 791: IP
-  RFC 792: ICMP
-  RFC 793: TCP
-  RFC 826: ARP
-  RFC 854: Telnet
-  RFC 951: Bootstrap Protocol
-  RFC 1542: BOOTP Extensions
-  RFC 959: FTP
-  RFC 1058: RIP Routing
-  RFC 1112: IP Multicast and IGMP
-  RFC 1157: SNMPv1
-  RFC 1166: IP Addresses
-  RFC 1253: OSPF Routing
-  RFC 1256: ICMP Router Discovery
-  RFC 1305: NTP
-  RFC 1492: TACACS+
-  RFC 1493: Bridge MIB
-  RFC 1542: Bootstrap Protocol
-  RFC 1583: OSPFv2
-  RFC 1643: Ethernet Interface MIB
-  RFC 1723: RIPv2 Routing
-  RFC 1757: RMON
-  RFC 1812: IP Routing
-  RFC 1901: SNMPv2C
-  RFC 1902-1907: SNMPv2
-  RFC 1981: MTU Path Discovery IPv6
-  FRC 2068: HTTP
-  RFC 2080: RIP for IPv6
-  RFC 2131: DHCP
-  RFC 2138: RADIUS
-  RFC 2233: IF MIB
-  RFC 2236: IP Multicast
-  RFC 2328: OSPFv2
-  RFC 2273-2275: SNMPv3
-  RFC 2373: IPv6 Aggregatable Addrs
-  RFC 2453: RIPv2 Routing
-  RFC 2460: IPv6 protocol
-  RFC 2461: IPv6 Neighbor Discovery
-  RFC 2462: IPv6 Autoconfiguration
-  RFC 2463: ICMP IPv6
-  RFC 2474: DiffServ Precedence
-  RFC 2597: Assured Forwarding
-  RFC 2598: Expedited Forwarding
-  RFC 2571: SNMP Management
-  RFC 2740: OSPF for IPv6
-  RFC 3046: DHCP Relay Agent Information Option
-  RFC 3101, 1587: NSSAs
-  RFC 3376: IGMPv3
-  RFC 3580: 802.1x RADIUS

IEEE Reference
================

-  IEEE 802.1D Spanning Tree Protocol
-  IEEE 802.1p CoS Prioritization
-  IEEE 802.1Q VLAN
-  IEEE 802.1s
-  IEEE 802.1w
-  IEEE 802.1x
-  IEEE 802.1AB (LLDP)
-  IEEE 802.3ad
-  IEEE 802.3af

IEEE SFP Reference
===================
-  IEEE 802.3ah (100BASE-X single/multimode fiber only)
-  IEEE 802.3x full duplex on 10BASE-T, 100BASE-TX, and 1000BASE-T ports
-  IEEE 802.3 10BASE-T specification
-  IEEE 802.3u 100BASE-TX specification
-  IEEE 802.3ab 1000BASE-T specification
-  IEEE 802.3z 1000BASE-X specification
-  100BASE-BX (SFP)
-  100BASE-FX (SFP)
-  100BASE-LX (SFP)
-  1000BASE-BX (SFP)
-  1000BASE-SX (SFP)
-  1000BASE-LX/LH (SFP)
-  1000BASE-ZX (SFP)
-  1000BASE-CWDM SFP 1470 nm
-  1000BASE-CWDM SFP 1490 nm
-  1000BASE-CWDM SFP 1510 nm
-  1000BASE-CWDM SFP 1530 nm
-  1000BASE-CWDM SFP 1550 nm
-  1000BASE-CWDM SFP 1570 nm
-  1000BASE-CWDM SFP 1590 nm
-  1000BASE-CWDM SFP 1610 nm



CCENT
~~~~~

Trivia
------
These are worth memorizing, they are "trivia" questions and could easily be the difference between a pass and a fail.

RFC1918 => Address Allocation for Private Internets

  This outlines private addresses, I.E. 192.168, etc.

RFC1519 => CIDR


MTU => Baby giant frames refer to Ethernet frame size up to 1600 bytes, and jumbo frame refers to Ethernet frame size up to 9216 bytes So basically beyond 1600 bytes it should be considered Jumbo.

.. important::
  Note the language above says "up to", it does not include 1600 bytes!!
  Cisco is evil and will use this technicality to trip you up. They've got to weed out the weak ones I guess.





Practice Questions
~~~~~~~~~~~~~~~~~~~

Question 6
What are three characteristics of the TCP protocol? (Choose three)
A. The connection is established before data is transmitted.
B. It uses a single SYN-ACK message to establish a connection.
C. It ensures that all data is transmitted and received by the remote device.
D. It uses separate SYN and ACK messages to establish a connection.
E. It supports significantly higher transmission speeds than UDP.
F. It requires applications to determine when data packets must be retransmitted.

  Answer: A C D


Question 9

Which forwarding technology stores destination addresses in the cache?

A. MPLS
B. Cisco express forwarding
C. Process switching
D. Fast switching

 Answer: B


Question 12

Which route is the most secure?

  Answer: connected route


Question 13

Which syslog severity level logs informational messages?

A. 2
B. 6
C. 4
D. 0

Answer: B

..
..
.. CCNA
.. =====
..
..
.. CCNP
.. =====
..
..
..
.. CCIE
.. =====
.. To be continued...
