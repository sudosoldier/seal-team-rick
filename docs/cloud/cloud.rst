gcloud cmd notes
~~~~~~~~~~~~~~~~

.. code-block:: console

  # gcloud compute connect-to-serial-port cl-7790 --port 2 --zone=us-east1-b --extra-args replay-lines=0 --extra-args on-dtr-low=disconnect
  # stty rows 60 cols 120

  ## CLOUD SNAPSHOT STUFF
  # gcloud compute --project "high-codex-147117" disks create "cl-7799-central" --size "8000" --zone "us-central1-b" --source-snapshot "rhr8p9msl7uo" --type "pd-standard"

  # gcloud compute --project=high-codex-147117 instances create cl-7799-central --zone=us-central1-b --machine-type=n1-standard-2 --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=103962587030-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --tags=http-server,https-server --disk=name=cl-7799-central,device-name=cl-7799-central,mode=rw,boot=yes,auto-delete=yes


  # gcloud compute --project "high-codex-147117" disks create "cl-7799-central" --size "8000" --zone "us-central1-b" --source-snapshot "rhr8p9msl7uo" --type "pd-ssd"

  # gcloud compute --project=high-codex-147117 instances create cl-7799-central --zone=us-central1-b --machine-type=n1-standard-2 --subnet=peachstate-test --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=103962587030-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --tags=http-server,https-server --disk=name=cl-7799-central,device-name=cl-7799-central,mode=rw,boot=yes,auto-delete=yes

